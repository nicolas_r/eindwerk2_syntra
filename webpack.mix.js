const { mix } = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

// Before you call mix to process files in webpack.mix.js
// mix.webpackConfig({
//     resolve: {
//         alias: {
//             'bootstrap-toggle': 'bootstrap-toggle/js/bootstrap-toggle.js'  // relative to node_modules
//         }
//     }
// });

// Default css-packages + plugins
mix.js('resources/assets/js/app.js', 'public/js')
    .sass('resources/assets/sass/app.scss', 'public/css');
    // .js(
    //     [
    //         'node_modules/bootstrap-toggle/js/bootstrap-toggle.js'
    //     ],
    //     'public/js/vendor.js' //-- Compileer alle plugins naar vendor.js
    // );

// Default js packages
mix.copy('node_modules/angular/angular.js', 'public/js');
mix.copy('resources/assets/js/angular_components/app.js', 'public/js/app');
mix.copy('resources/assets/js/angular_components/controllers/VerpakkingenController.js', 'public/js/app/controllers');

mix.copy('node_modules/bootstrap-toggle/css/bootstrap-toggle.css', 'public/css');

mix.copy('node_modules/font-awesome/css/font-awesome.css', 'public/css');
mix.copy('node_modules/font-awesome/fonts/fontawesome-webfont.ttf', 'public/fonts');
mix.copy('node_modules/font-awesome/fonts/fontawesome-webfont.woff', 'public/fonts');
mix.copy('node_modules/font-awesome/fonts/fontawesome-webfont.woff2', 'public/fonts');

// mix.script to combine not working, using mix.combine (for later?) instead...
// mix.combine([
//     'node_modules/angular/angular.js',
//     'resources/assets/js/angular_components/app.js',
//     'resources/assets/js/angular_components/controllers/VerpakkingenController.js'
// ], 'public/js/angularApp.js');

// Enable JS versioning when setting app in production ('npm run production' <-> 'npm run dev')
if (mix.config.inProduction) {
    mix.version();
}
