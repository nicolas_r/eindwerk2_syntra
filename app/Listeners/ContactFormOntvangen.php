<?php

namespace App\Listeners;

use App\Events\ContactFormEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Mail\ContactForm;
use App;

class ContactFormOntvangen
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  ContactFormEvent  $event
     * @return void
     */
    public function handle(ContactFormEvent $event)
    {
        \Mail::to($event->mailData['ontvanger_email'])->send(new ContactForm($event->mailData));
    }
}
