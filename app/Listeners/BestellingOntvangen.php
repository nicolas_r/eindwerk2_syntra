<?php

namespace App\Listeners;

use App\Events\BestellingOntvangenEvent;
use App\LeveringsAdressenModel;
use App\Mail\BestelbonBevestiging;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Log;
use App\Custom\CalculateOrderInvoice;

class BestellingOntvangen
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  BestellingOntvangenEvent  $event
     * @return void
     */
    public function handle(BestellingOntvangenEvent $event)
    {
        //Log::info($event->bestelbonData);
        Log::info(($event->bestelbonData->id));
        Log::info(($event->bestelbonData->klanten_id));
        Log::info(($event->restaurantData->naam_restaurant));

        $adresData = LeveringsAdressenModel::find($event->bestelbonData->leveringsadressen_id);

        $userData = \App\User::find($event->bestelbonData->klanten_id);

        $invoiceCalculation = new CalculateOrderInvoice($event->bestelbonData);
        $invoice = $invoiceCalculation->getOrderInvoice();

        \Mail::to($userData->email)->send(new BestelbonBevestiging($event->bestelbonData, $invoice, $userData, $event->restaurantData->naam_restaurant, $adresData));
    }
}
