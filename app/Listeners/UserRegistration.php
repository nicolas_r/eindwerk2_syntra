<?php

namespace App\Listeners;

use App\Events\UserRegistrationEvent;
use App\Mail\UserCreated;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Log;

class UserRegistration
{

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  UserRegistrationEvent  $event
     * @return void
     */
    public function handle(UserRegistrationEvent $event)
    {
        //Log::info($event->email);
        Log::info(($event->user->name));
        Log::info(($event->user->email));

        \Mail::to($event->user->email)->send(new UserCreated($event->user));
    }
}
