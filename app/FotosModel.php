<?php

namespace App;

use ClassesWithParents\G;
use Illuminate\Database\Eloquent\Model;
use App\Lib\GlideHandler;
use League\Glide\Server;
use League\Glide\ServerFactory;

class FotosModel extends Model
{
    protected $table = 'fotos';

    protected $fillable = [
        'path',
        'filename',
        'extensie',
        'temporary',
        'uploaded_by',
        'gerechten_id',
        'restaurants_id'
    ];

    public function gerecht()
    {
        return $this->belongsTo('App\GerechtModel','fotos','id');
    }

    public function restaurant()
    {
        return $this->belongsTo('App\RestaurantModel','foto','id');
    }

    // kon je rechstreeks van hieruit de glide handler retourneren, was dit leuk geweest... werkte nl niet
    public function getFotoDataAttribute($data = null)
    {
        return $this->path . '/' . $this->filename;
        //$result = $handler->createImageUrl($this->path . '/' . $this->filename, ['w' => 300, 'h' => 300, 'fit' => 'crop']);

        //$data = "data binnen accessor" . $data;
        //$glideHandler = new GlideHandler(\League\Glide\Server $server);

        //return $data;
//        //dump($handler);
//
////        if(!is_null($this)){
////            dump($handler);
////            $result = $handler->createImageUrl($this->path . '/' . $this->filename, ['w' => 300, 'h' => 300, 'fit' => 'crop']);
////            dump($result);
//////            return $result;
////        }
//
////        return $handler . " is the result";
    }

//    public function setFotoDataAttribute($url){
//        $this->attributes['url'] = $url;
//    }

}
