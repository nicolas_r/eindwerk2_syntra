<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;

class CheckRole
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $role)
    {
        if (Auth::guest() or ! $request->user()->hasRole($role))
        {
            echo "You have no access to this page!";
            App::abort(403);

        }
        // echo 'boe';
        // return redirect('/'); // doet het wel, als alles 'klopt' - onlogisch...
        return $next($request);
    }
}
