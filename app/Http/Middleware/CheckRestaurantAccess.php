<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;

class CheckRestaurantAccess
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $restaurantId = $request->route('restaurantId');

        if (Auth::guest() or ! $request->user()->canManageRestaurant($restaurantId))
        {
            //if(!$this->authenticationOk)
                abort(404, 'The middleware I created, decided you have no access to this Restaurant! :)'); // No restaurant found
        }

        return $next($request);
    }
}
