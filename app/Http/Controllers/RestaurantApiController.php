<?php

namespace App\Http\Controllers;

use App\Modules\ApiResponse;
use Illuminate\Http\Request;
use App\RestaurantModel;
use Illuminate\Support\Facades\Auth;

class RestaurantApiController extends Controller
{
    private $restaurantModel = null;
    private $apiResponse = null;

    public function __construct(RestaurantModel $restaurantModel, ApiResponse $apiResponse){
        $this->restaurantModel = $restaurantModel;
        $this->apiResponse = $apiResponse;
    }

    /**
     * Toggles the data 'actief' for a specified record in table restaurants to TRUE or FALSE (1 / 0)
     * @param Request $request
     * @param $id
     */
    public function update(Request $request, $id)
    {
        $result = $this->restaurantModel->where('id', '=', $id)->update($request->all());
        $response = $this->restaurantModel->where('id','=', $id)->first();

        if($result)
            return $this->apiResponse->create($response);

        return $this->apiResponse->create($response, [ "Update database failed!" ]);
    }

    /**
     * @param $id
     */
    public function show($id)
    {

    }
}
