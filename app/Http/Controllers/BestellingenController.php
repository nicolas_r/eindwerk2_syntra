<?php

namespace App\Http\Controllers;

use App\BestelbonModel;
use App\BestellingStatusModel;
use App\GerechtModel;
use App\LeveringsAdressenModel;
use App\RestaurantModel;
use App\User;
use Illuminate\Http\Request;
use Cart;
use App\Custom\CalculateOrderInvoice;
use App\Modules\ApiResponse;

class BestellingenController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(BestelbonModel $bestelbonModel, RestaurantModel $restaurantModel, BestellingStatusModel $bestellingStatusModel, Request $request, $restaurantId)
    {
        $filterId = $request->get('filterstatus');

        if(is_null($filterId))
            $filterId = 0;

        if($filterId == 0)
        {
            $result = $bestelbonModel->with('user')->where('restaurant_id', $restaurantId)->orderBy('bestellingstatusen_id')->paginate(15);
        }
        else
        {
            $result = $bestelbonModel->with('user')->where('restaurant_id', $restaurantId)->where('bestellingstatusen_id', $filterId)->paginate(15);
        }

        $naam = $restaurantModel->find($restaurantId)->naam_restaurant;
        $statuses = $bestellingStatusModel->all();

//        $test = $bestelbonModel->first();
//        $invoice = new CalculateOrderInvoice($test);
//        dd($invoice);

        return view('pages.restaurant.bestellingen.bestellingen', [
            'bestelbondata' => $result,
            'restaurant_naam' => $naam,
            'statuses' => $statuses,
            'currentRestaurant' => $restaurantId,
            'filterId' => $filterId
        ]);
    }

    public function detail(RestaurantModel $restaurantModel, BestelbonModel $bestelbonModel, User $user, LeveringsAdressenModel $adressenModel, BestellingStatusModel $bestellingStatusModel, $restaurantId, $bestellingId)
    {

        $restaurant = $restaurantModel->find($restaurantId);

        $bestelbon = $bestelbonModel->find($bestellingId);

        $statuses = $bestellingStatusModel->all();
        $leveringsadres = $adressenModel->find($bestelbon->leveringsadressen_id);

        $invoice = new CalculateOrderInvoice($bestelbon);
        $invoiceData = $invoice->getOrderInvoice();

        $gebruikersdata = $user->where('id',$bestelbon->klanten_id)->first();

        //dd($invoiceData);

        return view('pages.restaurant.bestellingen.bestelling_beheer', [
            'statuses' => $statuses,
            'currentRestaurant' => $restaurant,
            'invoiceData' => $invoiceData,
            'leveringsadres' => $leveringsadres,
            'gebruikersData' => $gebruikersdata,
            'bestellingStatus' => $bestelbon->bestellingstatusen_id,
            'bestellingTijdstip' => $bestelbon->bestellingtijdstip,
            'bestellingId' => $bestellingId
        ]);
    }

    public function changeBestellingStatus(BestelbonModel $bestelbonModel, Request $request, $restaurantId, $bestellingId = null)
    {
        $newStatus = $request->get('status');

        $bestelbon = $bestelbonModel->find($bestellingId);
        $bestelbon->bestellingstatusen_id = $newStatus;
        $bestelbon->save();

        // Dummy value in overzicht, bepaalt welke redirect wordt gebruikt
        if($request->get('overzicht')) {
            return redirect('/restaurant/bestellingen/' . $restaurantId);
        } else {
            //indien route met 2 url parameters
            return redirect('/restaurant/bestellingen/' . $restaurantId . '/' . $bestellingId);
        }
    }

    public function annuleerBestelling($restaurantId, $bestellingId)
    {
        // kan je doen door de status op 'cancelled' te zetten bijvb?
    }

    public function annuleerBestellingItem($restaurantId, $bestellingId, $gerechtId)
    {
        // Je zou dit kunnen doen, door "aantal" op 0 te zetten.  0 betekent geannuleerd.
        // Want probleem is als je ze fysiek verwijdert, kan je niet meer opvolgen...
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function followup($id, $url_hash, BestelbonModel $bestelbonModel, BestellingStatusModel $bestellingStatusModel, GerechtModel $gerechtModel){

        $data = $bestelbonModel->with('gerechten')->findOrFail($id);

        if($data->followup_url_hash != $url_hash){
            abort (404, 'The resource you are looking for could not be found');
        }

        $status = $bestellingStatusModel->find($data->bestellingstatusen_id)->status;

        $invoiceCalculation = new CalculateOrderInvoice($data);

        //dd($invoiceCalculation->getOrderInvoice());

        return view('pages.public.followup', [
            'bestelbondata' => $invoiceCalculation->getOrderInvoice(),
            'status' => $status
        ]);
    }

    /* API */
    public function checkStatus(Request $request, BestelbonModel $bestelbonModel, BestellingStatusModel $bestellingStatusModel, ApiResponse $apiResponse){
        $errors = [];
        $response = new ApiResponse;

        $bestelbon = $bestelbonModel->find($request->get('id'));

        if(is_null($bestelbon)){
            array_push($errors, "Bestelbon niet gevonden");
            return $apiResponse->create($response, $errors);
        }

        if($bestelbon->followup_url_hash != $request->get('urlHash'))
        {
            array_push($errors, "Incorrect hash");
        } else {
            $response = [
                'bestelbon_id' => $request->get('id'),
                'hash' => $request->get('urlHash'),
                'status' => $bestellingStatusModel->find($bestelbon->bestellingstatusen_id)->status,
                'leveringstijd' => $bestelbon->geschatteleveringtijdstip
            ];

            // De laatste 50 % van de levertijd zou hier moeten kunnen worden berekend, afhankelijk van traject-tijd leverancier
            // deze tijd zou vanaf de leveranciers app naar de server moeten kunnen worden verstuurd
            // Op basis van deze cijvers bereken een percentage-offset van 50 eenheden (TODO)

            if(!is_null($bestelbon->travel_time) && !is_null($bestelbon->remaining_time))
            {
                $correctieFactor = 50 - ($bestelbon->remaining_time / $bestelbon->travel_time * 50);
                if($correctieFactor > 50)
                    $correctieFactor = 50;

                if($correctieFactor < 0)
                    $correctieFactor = 0;

            } else {
                $correctieFactor = 0;
            }

            // Formule correctie-factor = remaining_time / travel_time * 50
            $response['percent_ready'] = $bestellingStatusModel->find($bestelbon->bestellingstatusen_id)->progress_procent + $correctieFactor;
        }

        return $apiResponse->create($response, $errors);
    }
}
