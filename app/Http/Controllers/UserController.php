<?php

namespace App\Http\Controllers;

use App\LeveringsAdressenModel;
use App\Role;
use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use App;

class UserController extends Controller
{
    private $rules = [
        //'name' => 'required|min:4',
        //'email' => 'required|email',
        //'password' => 'required'
        'firstname' => 'required|min:2',
        'lastname' => 'required|min:2'
    ];

    /**
     * Index users tabel in pagina's...
     *
     * @param User $user
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(\App\User $user){
        // Filteren zodat enkel mesen met deze role, toegang hebben

        // Haal alle users uit database
        $result = $user->paginate(15);

        return view('pages.beheer.users', ['users' => $result]);
    }

    public function create(){

    }

    // POST
    public function store(Request $request){

    }

    /**
     * Individuele record van een user + zijn privileges...
     *
     * @param User $user
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show(User $user, LeveringsAdressenModel $adressenModel, $id) {

        //-- Kijk wie ingelogd is, is die admin of niet?
        $activeUser = $user->with('roles')->where('id', Auth::user()->id)->first();

        if($activeUser->hasRole('admin')){
            //-- Kan enkel eigen ID profielgegevens beheren!
            //$result = $user->with('roles')->where('id', $id)->first();
            //$leveringsAdressen = $adressenModel->where('user_id', $id)->get();
            $result = $user->with('roles', 'leveringsadressen')->where('id', $id)->first();
            //dump($result->toArray());

        } else {
            //-- Kan enkel eigen ID profielgegevens beheren!
            $result = $user->with('roles', 'leveringsadressen')->where('id', Auth::user()->id)->first();
        }

        //dd($result->toArray());

        return view('pages.beheer.userprofile', [
            'userData' => $result
        ]);
    }

    /**
     * Ajax call voor de delete-modal (todo: kan eventueel weg, modal formdata eventueel via Javascript?)
     *
     * @param User $user
     * @param $id
     * @return mixed
     */
    public function showUserAjax(User $user, $id){
        $result = $user->with('roles')->find($id);
        return $result;
    }

    // GET - zie show() ?
    public function edit(User $user, $id){

        return "Edit user!" . $id;
    }

    /**
     * POST : Update user data + roles
     *
     * @param Request $request
     * @param User $user
     * @param Role $roles
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, User $user, Role $roles, $id){

        //-- Kijk wie ingelogd is, is die admin of niet?
        $activeUser = $user->with('roles')->where('id', Auth::user()->id)->first();
        $data = $request->except('_token');

        // Validatie gegevens - todo
        $validator = Validator::make($data, $this->rules);

        if ($validator->fails()) {
            //Doe hier dd's vr diagnose)
            //dump($data);
            //dump("validator failed!");
            if($activeUser->hasRole('admin')){
                return redirect('/admin/user/profile/' . $id )
                    ->withErrors($validator)
                    ->withInput(); // Zorgt ervoor dat we in detail, de 'old' kan gebruiken in de view
            }
            return redirect('/user/profile/' . $id )
                ->withErrors($validator)
                ->withInput();     // Zorgt ervoor dat we in detail, de 'old' kan gebruiken in de view
        }

        // Admin zal role checkbox data binnen gePOST krijgen
        // Niet-Admin kan géén role checkbox data binnen krijgen, dus niet updaten...
        if($activeUser->hasRole('admin')){
            $userRoles = [];

            // Haal bestaande roles op uit form input:
            $existingRoles = $roles->get();
            foreach($existingRoles->toArray() as $key){
                // Check if $key['name'] exists in $data array...
                if($request->has($key['name'])){
                    array_push($userRoles, $key['id']); // Collect role keys for pivot table
                    array_forget($data, $key['name']);  // Forget parsed role keys
                }
            }

            //-- Update user role for user $id
            $result = $user->find($id)->roles()->sync($userRoles);

            // Update user database
            $result = $user->find($id);
            $result->update($data);

            return redirect('/admin/users')->with('status', 'User updated!');
        } else {
            //-- Update user role enkel en alleen voor ingelogde user
            $result = $user->find(Auth::user()->id);

            // Update user database
            $result = $user->find(Auth::user()->id);
            $result->update($data);

            return redirect('/')->with('status', 'User updated!');
        }
    }

    /**
     * DELETE : Je kan elk user account verwijderen ... BEHALVE je EIGEN account waarmee je bent ingelogd... (see Blade-code)
     * @param Request $request
     * @param User $user
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function delete(Request $request, User $user){
        // Retrieve POST data
        $id = $request->get('id');

        // Delete user
        $result = $user->find($id)->delete();



        // Herlaad tabel
        return redirect('/admin/users');
    }

    // momenteel niet gebruikt...
    // Toggles activation / deactivation of individual users via a fancy toggle-switch
    public function toggleActivationAjax(Request $request, User $user){
        $id = $request->get('id');
        $result = $user->find($id);

        $result->id = ($result->id) ? 0 : 1;

        // Niet vergeten te saven in database

        // Resultaat teruggeven met extra data: success true of false (op basis van succes can AJAX via JS view aanpassen zonder reload?)
        Return "Output has been inverted, current value: " . $result->id;
    }

    /**
     * Verwijdert leveringsadressen in profielbeheer, via ajax call. Gewoon omdat het kan...
     * @param Request $request
     * @param LeveringsAdressenModel $adressenModel
     * @param null $userId
     * @param null $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function deleteAddressAjax(Request $request, LeveringsAdressenModel $adressenModel, App\BestelbonModel $bestelbonModel, $userId = null, $id = null){
        $data = $adressenModel->where('id', '=', $id)->first();

        // als een leveringsadres op een actief order zit, kan deze niet worden verwijderd -> fout!
        $bestelbon = $bestelbonModel->where('leveringsadressen_id',$data->id)->where('bestellingstatusen_id', '<' ,6)->get();

        if(!$data){
            return response()
                ->json([
                    'data' => [
                        'error' => 'Delete failed, record not found!',
                        'id' => $id
                    ]
                ], 410);
        }

        if($bestelbon->count()){
            return response()
                ->json([
                    'data' => [
                        'error' => 'Delete failed, actieve bestellingen momenteel bezig op dit adres!',
                        'id' => $id
                    ]
                ], 405);
        }

        $result = $data->delete();
        if($result) {
            return response()
                ->json([
                    'data' => [
                        'message' => 'Succesfully deleted!',
                        'id' => $id
                    ]
                ], 200);
        }
    }
}

//test
//Test2
//Test3
//Test4