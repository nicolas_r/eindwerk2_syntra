<?php

namespace App\Http\Controllers;

use App\BestelbonModel;
use App\BestellingStatusModel;
use App\GerechtModel;
use App\RestaurantModel;
use Illuminate\Http\Request;
use App\Modules\ApiResponse;
use Carbon\Carbon;

class LeveranciersAppController extends Controller
{

    private $apiResponse = null;

    public function __construct(ApiResponse $apiResponse){
        $this->apiResponse = $apiResponse;
    }

    // Haal alle niet-geclaimde (= null) orders op
    public function index(BestelbonModel $bestelbonModel, RestaurantModel $restaurantModel){
        $errors = [];

        $result = $bestelbonModel->where('leveranciers_id',null)
            ->with('leveringsadres', 'user', 'bestellingstatus', 'restaurant')
            ->get();

        $data = [];

        foreach($result as $key => $content){
            $data[$key] = [];
            $data[$key] = $content->toArray();
            $data[$key]['totaal'] = $content->totaal_bedrag['totaal'];
        }

        return $this->apiResponse->create($data, $errors);
    }

    public function claim(Request $request, BestelbonModel $bestelbonModel, $orderId){
        $errors = [];

        $bestelbon = $bestelbonModel->find($orderId);

        if($bestelbon->leveranciers_id == 1){
            $errors[0] = "Dit order werd mogelijks reeds door iemand anders geclaimd!";
        } else {
            $bestelbon->leveranciers_id = 1;
        }

        // Frontend kan status veranderen bij claimen, indien nodig...
        if(!is_null($request->get('status')))
            $bestelbon->bestellingstatusen_id = $request->get('status');

        $bestelbon->save();

        return $this->apiResponse->create($bestelbon, $errors);
    }

    // niet gebruikt
    public function bestellingGereed(BestelbonModel $bestelbonModel, $orderId){
        $errors = [];

        $bestelbon = $bestelbonModel->find($orderId);

        // todo : error-afhandeling

        return $this->apiResponse->create($bestelbon, $errors);
    }

    public function changeStatus(Request $request, BestelbonModel $bestelbonModel, $orderId){
        $errors = [];

        if(!($request->get('status') > 3) && ($request->get('status') < 7)){
            $errors[0] = "Ongeldige order-status ontvangen!";
            $result = null;
            return $this->apiResponse->create($result, $errors);
        }

        $result = $bestelbonModel->with('bestellingstatus','restaurant')->find($orderId);

        if(!is_null($request->get('pickup_time')))
            $result->pickuptijdstip = Carbon::createFromTimestamp($request->pickup_time)->toDateTimeString();

        if(!is_null($request->get('delivery_time')))
            $result->effectieveleveringtijdstip = Carbon::createFromTimestamp($request->get('delivery_time'))->toDateTimeString();

        $result->bestellingstatusen_id = $request->get('status');
        $result->save();

        // Nieuwe call doen, omdat na het saven, het juiste resultaat niet wordt meegegeven
        $result = $bestelbonModel->with('bestellingstatus','restaurant')->find($orderId);

        return $this->apiResponse->create($result, $errors);
    }

    public function travelTime(Request $request, BestelbonModel $bestelbonModel, $orderId){
        $errors = [];
        $travel_time = $request->get('travel_time');

        // Voor geval API een absurde waarde geeft, anders loopt opslaan in DB mogelijks in de soep...
        if($travel_time > 32760){
            $errors[0] = "Ongeldige reistijd-waarde ontvangen!";
            $result = null;
            return $this->apiResponse->create($result, $errors);
        }

        $bestelbon = $bestelbonModel->find($orderId);
        $bestelbon->geschatteleveringtijdstip = Carbon::now()->addSeconds($travel_time);

        // Traveltime wordt 1 malig bewaard.  Remaining time, steeds opnieuw
        // Ze bepalen de "reistijd" in seconden vd leverancier naar de klant (dit vormt de basis voor die 50% balk met "remaining_time")
        if(!$bestelbon->travel_time)
            $bestelbon->travel_time = $travel_time;

        $bestelbon->remaining_time = $travel_time;

        $bestelbon->save();

        // haal tijd van pick-up uit de database, maak som met travel_time (in seconden) via Carbon

        // op 1 of andere manier moet deze tijds-info in het opvolgingsvenster geraken...
        // spelen in bestelbonmodel met "travel_time" (in seconden) en "pickuptijdstip" om geschatte leveringstijdstip te bepalen in "geschatteleveringtijdstip"
        return $this->apiResponse->create($bestelbon, $errors);
    }
}
