<?php

namespace App\Http\Controllers;

use App\RestaurantModel;
use Illuminate\Http\Request;
use Spatie\OpeningHours\OpeningHours;
use Validator;

class OpeningsUrenController extends Controller
{
    public function openingsuren(Request $request, RestaurantModel $restaurantModel, $id) {
        $restaurant = $restaurantModel->find($id);
        if($restaurant)
            $huidigetijden = json_decode($restaurant->openingsuren);

        return view('pages.restaurant.openingsuren', [
            'huidigeopeningstijden' => $huidigetijden,
            'currentRestaurant' => $restaurant->id
        ]);
    }

    public function addTime(Request $request, $id){
        $rules = [
            'weekdag' => 'required',
            'openingsuur' => 'required:date_format:H:i',
            'sluitingsuur' => 'required:date_format:H:i',
        ];

        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return redirect('/restaurant/' . $id . '/openingsuren')
              ->withErrors($validator)
              ->withInput();             // Zorgt ervoor dat we in detail, de 'old' kan gebruiken in de view
        }

        $openingsuren = $request->session()->get('input_openingsuren');

        // create sub-array
        $openingsuren[$request->get('weekdag')][] = $request->get('openingsuur') . '-' . $request->get('sluitingsuur');

        if(array_has($openingsuren, $request->get('weekdag'))){
            try{
                $openingHours = OpeningHours::create($openingsuren);
            } catch (\Exception $except) {
                $errors[0] = $except->getMessage();
                    return redirect('/restaurant/' . $id . '/openingsuren')
                        ->withErrors($errors)
                        ->withInput();             // Zorgt ervoor dat we in detail, de 'old' kan gebruiken in de view
            }

//            // loop alle arrays af
//            for($i = 0; $i < count($openingsuren[$request->get('weekdag')]); $i++){
//                $overlap = false;
//
//                // vergelijk elke set openings- sluitingsuur in de array
//                $times = explode('-', $openingsuren[$request->get('weekdag')][$i]);
//
//                // ingegeven openingsuur
//                $current_time = $request->get('openingsuur');
//
//                // indien ingegeven openingsuur < sluitingsuur && ingegeven openingsuur > openingsuur
//                if($request->get('openingsuur') < $times[1] && $request->get('openingsuur') > $times[0])
//                {
//                    //Indien overlap openingstijd = $overlap = true;
//                    $overlap = true;
//                }
//
//                // indien ingegeven sluitingsuur > openingsuur && ingegeven openingsuur < sluitingsuur
//                if($request->get('sluitingsuur') > $times[0] && $request->get('sluitingsuur') < $times[1]){
//                    //Indien overlap sluitingstijd = $overlap = true;
//                    $overlap = true;
//                }
//
//                if($overlap)
//                {
//                    $errors[0] = "Bepaalde uren overlappen op éénzelfde dag, kan niet bewaren!";
//                    return redirect('/restaurant/' . $id . '/openingsuren')
//                        ->withErrors($errors)
//                        ->withInput();             // Zorgt ervoor dat we in detail, de 'old' kan gebruiken in de view
//                }
//            }
        }

        // sorteer keys array
        $week_order = array_flip(array('monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday', 'sunday', 'exceptions'));

        uksort($openingsuren, function($a, $b) use ($week_order) { return $week_order[$a] - $week_order[$b]; });

        // update session
        $request->session()->put('input_openingsuren', $openingsuren);

        return redirect('/restaurant/' . $id . '/openingsuren');
    }

    public function reset($id){
        session()->forget('input_openingsuren');
        return redirect('/restaurant/' . $id . '/openingsuren');
    }

    public function createTime(RestaurantModel $restaurantModel, $id){
        dump(session());
        if(session()->get('input_openingsuren')){
//            dump('save data in restaurant met id: ' . $id);
//            dump(session()->get('input_openingsuren'));

            $data = json_encode(session()->get('input_openingsuren'));

//            try{
//                $openingHours = OpeningHours::create(session()->get('input_openingsuren'));
//            } catch (\Exception $except) {
//                dump('exception occreed!');
//                dd($except);
//            }
//            $openingHours = OpeningHours::create([
//                'monday' => ['09:00-12:00', '13:00-18:00'],
//                'tuesday' => ['09:00-12:00', '13:00-18:00'],
//                'wednesday' => ['09:00-12:00'],
//                'thursday' => ['09:00-12:00', '13:00-18:00'],
//                'friday' => ['09:00-12:00', '13:00-20:00'],
//                'saturday' => ['09:00-12:00', '13:00-16:00'],
//                'sunday' => [],
//                'exceptions' => [
//                    '2016-11-11' => ['09:00-12:00'],
//                    '2016-12-25' => [],
//                ],
//            ]);

            // Update database
            $restaurant = $restaurantModel->find($id);
            $restaurant->openingsuren = $data;

            // clear session  input_openingsuren *after* succesful save
            if($restaurant->save()){
                session()->forget('input_openingsuren');
                return redirect('/restaurant/' . $id . '/beheer');
            }
        }
    }
}
