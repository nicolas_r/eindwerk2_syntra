<?php

namespace App\Http\Controllers;

use App\GerechtModel;
use Illuminate\Http\Request;
use Illuminate\Routing\Route;
use Illuminate\Support\Facades\Auth;
use Spatie\OpeningHours\OpeningHours;
use Carbon\Carbon;
use Validator;

class RestaurantBeheerController extends Controller
{
    private $restaurantId;

    protected $authenticationOk = null;

    public function __construct(Request $request){
        $this->restaurantId = $request->route('restaurantId');

//-- VERHUISD NAAR MIDDLEWARE 'CheckRestaurantAccess'
//        //-- Check if logged-in user may access the "restaurant management"...
//        $this->middleware(function ($request, $next) {
//            $this->authenticationOk = Auth::user()->canManageRestaurant($this->restaurantId);
//
//            if(!$this->authenticationOk)
//                abort(404, 'You have no access to this Restaurant!'); // No restaurant found
//
//            return $next($request);
//        });

    }

    public function home() {
            return view('pages.beheer.restaurant_beheer', [ 'restaurantId' => $this->restaurantId ]);

    }

    public function restaurantDetails() {
        dump("Restaurant Details");
    }

    public function gerechten(GerechtModel $gerechten) {

            return view('pages.restaurant.gerechten.gerechten', ['gerechten' => $result]);

    }
}
