<?php

namespace App\Http\Controllers;

use App\VerpakkingenModel;
use Illuminate\Http\Request;

class VerpakkingenController extends Controller
{
    public function index(VerpakkingenModel $verpakkingen, $id = null){
        if($id == null){
            return $verpakkingen->orderBy('id', 'asc')->get();
        } else {
            return $this->show($verpakkingen, $id);
        }
    }

    public function store(VerpakkingenModel $verpakkingen, Request $request){
        //dd($request);
        $verpakkingen->naam = $request->input('naam');
        $verpakkingen->lengte = $request->input('lengte');
        $verpakkingen->breedte = $request->input('breedte');
        $verpakkingen->hoogte = $request->input('hoogte');
        $verpakkingen->save();
        return 'Verpakking succesvol bewaard met id ' . $verpakkingen->id;
    }

    public function show(VerpakkingenModel $verpakkingen, $id) {
        return $verpakkingen->find($id);
    }

    public function update(VerpakkingenModel $verpakkingen, Request $request, $id) {
        $result = $verpakkingen->find($id);
        $result->naam = $request->input('naam');
        $result->lengte = $request->input('lengte');
        $result->breedte = $request->input('breedte');
        $result->hoogte = $request->input('hoogte');
        $result->update();
        return 'Verpakking succesvol upgedate met id ' . $result->id;
    }

    public function destroy(VerpakkingenModel $verpakkingen, $id){
        $result = $verpakkingen->find($id)->delete();
        return 'Verpakking succesvol verwijderd';
    }
}

