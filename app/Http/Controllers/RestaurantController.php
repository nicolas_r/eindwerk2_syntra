<?php

namespace App\Http\Controllers;

use App\RestaurantModel;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use App\FotosModel;
use App\Modules\ApiResponse;
use Log;
use Illuminate\Support\Str;


class RestaurantController extends Controller
{
    private $restaurantModel = null;

    // Validation rules
    private $rules = [
        'naam_restaurant' => 'required|min:3',
        'postcode' => 'required|min:1000|numeric',
        'gemeente' => 'required',
        'email_restaurant' => 'email|required',
    ];

    public function __construct(RestaurantModel $restaurantModel){
        $this->restaurantModel = $restaurantModel;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(User $user)
    {
        //-- Site-Admin kan ALLE restaurants beheren...
        //-- Todo: pagination... (indien heel veel restaurants, vooral als admin relevant...)


        if(Auth::user()->hasRole('admin')){
            $result = $this->restaurantModel->paginate(10);
        } else {
            $result = $this->restaurantModel->where('users_id', '=', Auth::user()->id )->paginate(10);
        }

        //dump($result->toArray());
        return view('pages.beheer.restaurants', ['restaurants' => $result]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(User $user)
    {
        if(!Auth::user()->hasRole('restaurant')) {
            abort(404, 'Not allowed to create restaurant!');
        }

        $owners = $user->orderBy('lastname', 'asc')->get();

        return view('pages.beheer.restaurant',['owners' => $owners]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // Validation
        $data = $request->except(['_token', '_method']);
        $validator = Validator::make($data, $this->rules);

        if ($validator->fails()) {
            return redirect('/restaurant/create')
                ->withErrors($validator)
                ->withInput(); // Zorgt ervoor dat we in detail, de 'old' kan gebruiken in de view
        }

        //dd($data);

        $restaurant = new RestaurantModel($data);
        $result = $restaurant->save();

        return redirect('/restaurant');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        dd("show restaurant");
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($restaurantId, User $user)
    {
        //-- Admin can edit any restaurant, non-admin can only edit own restaurants
//        if(Auth::user()->hasRole('admin')){
//            $result = $this->restaurantModel->where('id','=', $id)->first();
//        } else {
//            $result = $this->restaurantModel->where('id','=', $id)->where('users_id', Auth::user()->id)->first();
//        }

        if(Auth::user()->canManageRestaurant($restaurantId)) {
            $restaurant = $this->restaurantModel->where('id','=', $restaurantId)->first();
            //dd($restaurant);
            $owner = $user->where('id', $restaurant->users_id)->first()->name; // Find owner of restaurant
        } else {
            abort(404, 'Restaurant not found'); // No restaurant found
        }

        return view('pages.beheer.restaurant', ['restaurant' => $restaurant, 'owner' => $owner]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // Validation
        $data = $request->except(['_token', '_method']);
        //dump($data);
        $validator = Validator::make($data, $this->rules);

        if ($validator->fails()) {
                return redirect('/restaurant/' . $id . '/edit')
                    ->withErrors($validator)
                    ->withInput(); // Zorgt ervoor dat we in detail, de 'old' kan gebruiken in de view
        }

        $this->restaurantModel->where('id', $id)->update($data);
        return redirect('/restaurant/' . $id . '/beheer');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function fotos_index($restaurantId, RestaurantModel $restaurantModel, FotosModel $fotosModel, \App\Lib\GlideHandler $glideHandler)
    {
        if(Auth::user()->canManageRestaurant($restaurantId));

        $restaurant = $restaurantModel->find($restaurantId);

        $fotos = $fotosModel->where('restaurants_id',$restaurantId)->where('gerechten_id',null)->get();
        //dd($fotos);

        $fotoData = [];

        if(!$fotos->isEmpty()){
            foreach($fotos as $foto){
                $id = $foto->id;

                Log::info('0 - Initiële trigger: glideHandler->createImageUrl vanuit controller:');
                Log::info('    Samen met origineel pad uit database: "' . $foto->path . '/' . $foto->filename . '"');

                $path = $glideHandler->createImageUrl($foto->path . '/' . $foto->filename, ['w' => 300, 'h' => 300, 'fit' => 'crop']);
                // test opties: , 'mark' => '/copyright.png', 'markw' => 300, 'markh' => 300, 'markfit' => 'max', 'markalpha' => 30, 'filt' => 'sepia'
                //dd($glideHandler);

                Log::info('    urlBuilder geeft een aanroepbare URL terug, met de nodige parameters:');
                Log::info('    "' . $path . '"' );

                $fotoData[] = ['id' => $id, 'path' => $path];
            }
        }

        //dd($fotoData);

        return view('pages.beheer.restaurant_fotos', [
            'restaurantId' => $restaurantId,
            'restaurantNaam' => $restaurant->naam_restaurant,
            'fotoData' => $fotoData,
        ]);
    }

    /**
     * API call, Bewaart foto's in een property
     *
     * @param Request $request
     * @param FotosModel $fotosModel
     * @param ApiResponse $apiResponse
     * @return \Illuminate\Http\JsonResponse
     */
    public function fotos_upload(Request $request, FotosModel $fotosModel, ApiResponse $apiResponse, GerechtController $gerechtController)
    {
//        Log::info('uploadAction');

        // Probleem - parallel uploads (ajax) zorgt ervoor dat de session variabelen niet allen worden ingevuld...
        // Gelijkaardig aan: https://github.com/laravel/framework/issues/7549
        // parallelUploads: 2 -> 1 lost het probleem op, maar toch...
        // Oplossing: bewaar foto's rechtstreeks in fotos database, maar met een extra vlag temporary = 1 ofzo...

        $temp = true;
        $restaurantId = $request->get('restaurantId');
        //dd($restaurantId);

        // Check of geposte restaurantId geen gefakete is... (anders vlieg je d'r uit...)
        if(Auth::user()->canManageRestaurant($restaurantId));

        // Een restaurant bestaat altijd alle uploads zijn steeds definitief
        $originalPath = 'images/' . $restaurantId .'/resto_img';
        $dbPath = $restaurantId . '/resto_img';

        $file = $request->file('file');
        $fileName = $gerechtController->validateFileName($fotosModel, $originalPath, $file); // Duplicaten checken

        $path = $file->storeAs($originalPath, $fileName); // Opslaan
        $extensie = Str::lower($file->getClientOriginalExtension());

        // Value of null, dus geen if nodig...
        $data = [
            'path' => $dbPath,
            'filename' => $fileName,
            'extensie' => $extensie,
            'restaurants_id' => $restaurantId,
            'temporary' => 0,
            'uploaded_by' => Auth::user()->id
        ];

        if($path)
        {
            $result = $fotosModel->create($data); // Opslaan foto-database
        }
        else
            return Response::json([
                'data' => $data['filename'],
                'error' => 'Unable to copy file!'
            ], 400);

//        ob_start();
//           var_dump($request->get('fotoUploadData'));
//           $ses = ob_get_contents();
//           ob_get_clean();
//        Log::info($ses);
//        Log::info($file->getClientOriginalName());

        if($result)
            return $apiResponse->create($result);
    }

    public function fotos_bewaar_keuze($restaurantId, Request $request, RestaurantModel $restaurantModel)
    {
        if(Auth::user()->canManageRestaurant($restaurantId));

        $selectedFoto = $request->get('fotos');
        $restaurant = $restaurantModel->find($restaurantId);
        $restaurant->fotos = $selectedFoto;
        $restaurant->save();

        return redirect('/restaurant/' . $restaurantId . '/beheer');
    }

    public function fotos_delete($restaurantId, $fotoId, FotosModel $fotosModel){
        // Check of geposte restaurantId geen gefakete is... (anders vlieg je d'r uit...)
        if(Auth::user()->canManageRestaurant($restaurantId));

        $foto = $fotosModel->find($fotoId);

        // Remove from disk
        $path = 'images/' . $restaurantId .'/resto_img';
        $filename = $foto->filename;

        // Indien geen file, zal er geen fout zijn, dus safe:
        Storage::delete($path . '/' . $filename);

        // Remove from Database
        if(!is_null($foto))
            $foto->delete();

        return redirect('/restaurant/'. $restaurantId .'/fotos');
    }
}
