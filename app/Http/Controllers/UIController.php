<?php

namespace App\Http\Controllers;

use App\BestelbonModel;
use Cart;
Use Illuminate\Http\Request;
Use Spatie\OpeningHours\OpeningHours;
Use Carbon\Carbon;
Use App\RestaurantModel;
Use App\GerechtModel;
Use App\FotosModel;
Use Validator;
Use Symfony\Component\HttpFoundation\Response;
use Auth;
use Event;

class UIController extends Controller
{
    public function showRestaurants(RestaurantModel $restaurantModel, Request $request, \App\Lib\GlideHandler $glideHandler){
        $restaurants = $restaurantModel->with('foto')->where('actief', '1')->get();

        // Gewoon omdat het kan: toevoegen to restaurant : is ze momenteel open of gesloten?
        for($i = 0; $i < count($restaurants); $i++){
            $openingsuren = (array)json_decode($restaurants[$i]['openingsuren']);

            $time = $request->server('REQUEST_TIME'); // Lokale tijd uit browser halen... (server tijd niet betrouwbaar)

            $openingHours = OpeningHours::create($openingsuren)->isOpen(Carbon::createFromTimestamp($time)->toDateTimeString());
            if($openingHours){
                $restaurants[$i]['status'] = 'Open';
            } else {
                $restaurants[$i]['status'] = 'Gesloten';
            }
        }



        foreach($restaurants as $key => $restaurant){
            if($restaurant['foto'])
            {
                $restaurants[$key]['logo'] = $glideHandler->createImageUrl($restaurant['foto']->path . '/' . $restaurant['foto']->filename, ['w' => 300, 'h' => 300, 'fit' => 'crop']);
            } else
            {
                $restaurants[$key]['logo'] = '/assets/images/fork_spoon_default.png';  // Default foto (geen foto beschikbaar
            }
        }

        //dd($restaurants->toArray());

        return view('pages.public.portal', [ 'restaurants' => $restaurants]);
    }

    public function showGerechten(FotosModel $fotosModel, GerechtModel $gerechtModel, \App\Lib\GlideHandler $glideHandler, $restoId){


        $menus  = $gerechtModel->with('foto')->where('restaurants_id', $restoId)->get();

        $gerechten = [];

        //dd($menus->toArray());

        foreach($menus as $key => $menu){
            $gerechten[$key] = [
                'id' => $menu['id'],
                'naam' => $menu['naam'],
                'omschrijving' => $menu['omschrijving'],
                'prijs' => $menu['prijs'],
                'btw' => $menu['btw'],
                'bereidingstijd' => $menu['bereidingstijd'],
                'restaurants_id' => $menu['restaurants_id'],
                'verpakkingen_id' => $menu['verpakkingen_id'],
                'foto_id' => $menu['fotos'],
                'foto' => '/assets/images/fork_spoon_default.png',  // Default foto (geen foto beschikbaar
            ];

            //dd($menu['foto']);

            if($menu['foto']){
                $gerechten[$key]['foto'] = $glideHandler->createImageUrl($menu['foto']->path . '/' . $menu['foto']->filename, ['w' => 300, 'h' => 300, 'fit' => 'crop']);
            }
        }

        return view('pages.public.menu', [ 'menus' => $gerechten]);
    }

    // functie misschien niet nodig...
    public function showGerechtFoto($id = null){
        // optioneel als id 'null' is (kwestie om geen fout te geven), toon gewoon de default foto...
    }

    public function addGerechtToBasket(Request $request, GerechtModel $gerechtModel, RestaurantModel $restaurantModel, $restoId){
        $errors = [];

        // Honeypot
        if($request->get('vliegenvanger'))
            return response('Forbidden.', 403)
                            ->header('Content-Type', 'text/plain');

        $validator = Validator::make($request->all(), [
            'aantal' => 'required|numeric|min:1|max:100|regex:/^\d+$/',
            'gerecht_id' => 'required|numeric',
        ]);

        if ($validator->fails()) {
            $errors[$request->gerecht_id] = "Fout!";
                        return redirect($restoId . '/menu')
                             ->withErrors($errors);
        }

        // Validation succesvol - Proceed:
        $gerecht = $gerechtModel->find($request->get('gerecht_id'));

        // Indien het bestelde gerecht om 1 of andere reden, niet bestaat
        if(!$gerecht)
            return "error 404";

        // Check of het toegevoegde gerecht van hetzelfde restaurant is, of niet
        //$restaurantId = $gerechtModel->where('restaurants_id', $request->gerecht_id)->first();

        if(Cart::count() > 0) {
            $eersteBestellingRestaurantId =  $gerechtModel->where('id', Cart::content()->first()->id)->first()->restaurants_id;
            //dump($eersteBestellingRestaurantId);
            $eersteBestellingRestaurantNaam = $restaurantModel->find($eersteBestellingRestaurantId)->naam_restaurant;
            //dump($eersteBestellingRestaurantNaam);
            $volgendeBestellingRestaurantId = $gerecht->restaurants_id;

            if($volgendeBestellingRestaurantId != $eersteBestellingRestaurantId ){
                $errors[0] = 'U probeerde reeds te gerechten te bestellen vanuit een ander restaurant; "' . strtoupper($eersteBestellingRestaurantNaam) . '".';
                $errors[1] = "U kunt enkel gerechten uit éénzelfde restaurant bestellen.  Maak anders het winkelkarretje leeg en probeer opnieuw!";
                return redirect()->back()
                    ->withErrors($errors);
            }
        }

        Cart::add($request->gerecht_id,$gerecht->naam,$request->aantal, $gerecht->prijs)->setTaxRate($gerecht->btw);

//        // Checken of sessie bestaat, zoja sessie ophalen
//        $winkelwagen = $request->session()->get('winkelwagen');
//
//        if($winkelwagen){
//            dump(session()->get('winkelwagen'));
//            dump('nieuwe sessie ok!');
//
//            // Checken of toegevoegd item uit hetzelde restaurant komt...
//        }
//
//        else {
//            dump(session()->get('winkelwagen'));
//            dump('maak nieuwe sessie');
//        }
//
//        // Toevoegen aan winkelwagen-sessie + sessie bewaren!
//        $winkelwagen[] = $request->except('_token', 'vliegenvanger');
//
//        dump($winkelwagen);
//        $request->session()->put('winkelwagen', $winkelwagen);
//
//        var_dump($request->session()->get('winkelwagen'));

        return redirect()->back();
    }

    public $contactFormQuestions = array(
        1 => 'Ik heb een algemene vraag',
        2 => 'Ik heb een vraag omtrent mijn bestelling', // Deze sleutel nr niet veranderen, wegens JS in view
        3 => 'Ik heb een klacht',                        // Deze sleutel nr niet veranderen, wegens JS in view
        4 => 'Ik wil restauranthouder worden',
        5 => 'Ik wil leverancier worden'
    );

    public function contactForm(BestelbonModel $bestelbonModel){
        $bestellingen = $bestelbonModel
            ->with('restaurant')
            ->where('klanten_id', Auth::getUser()->id)
            ->orderBy('bestellingtijdstip')
            ->get();

        return view('pages.public.contactform', [
            'bestellingen' => $bestellingen,
            'keuze_vragen' => $this->contactFormQuestions
        ]);
    }

    public function submitContactForm(Request $request, BestelbonModel $bestelbonModel, RestaurantModel $restaurantModel){
        // check of naam1 bestaat in request, zoja mogelijke spam issue => abort 404)

        if(!is_null($request->get('naam1')))
            abort(404, 'Not allowed!');

        $rules = [
            'message' => 'required',
            'question_type' => 'required'
        ];

        $questionNr = $request->get('question_type');

        if(!is_null($questionNr)){
            if($questionNr == 2 || $questionNr == 3 )
            {
                $rules['bestelbon_nr'] = 'required';
            }
        }

        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return redirect('/contact')
                ->withErrors($validator)
                ->withInput();             // Zorgt ervoor dat we in detail, de 'old' kan gebruiken in de view
        }

        if(!is_null($request->get('bestelbon_nr')))
        {
            $bestelbon = $bestelbonModel->find($request->get('bestelbon_nr'));
            $restaurant = $restaurantModel->find($bestelbon->restaurant_id);
            $data['ontvanger_email'] = $restaurant->email_restaurant;
            $data['restaurant_id'] = $restaurant->id;

        } else {
            $data['ontvanger_email'] = 'nicolas.ramoudt@gmail.com';
        }

        $vraag['question'] = $this->contactFormQuestions[$questionNr];
        $data = array_merge($data, $vraag);

        // e-mail verzenden naar restaurant, indien restaurant id mee is verstuurd
        $data = array_merge($data, $request->except(['_token']));

        //dd($data);

        Event::fire(new \App\Events\ContactFormEvent($data)); // Verdere synchrone of asynchrone afhandeling, bevestigingen via e-mail...

        return redirect('/');
    }
}
