<?php

namespace App\Http\Controllers;

use App\FotosModel;
use App\RestaurantModel;
use App\User;
use App\VerpakkingenModel;
use Illuminate\Http\Request;
use App\GerechtModel;
use Illuminate\Session\Store;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use File;
use App\Modules\ApiResponse;
use Response;
use Storage;
//use Log;

class GerechtController extends Controller
{
    private $restaurantId;
    private $gerechtModel;

    //private $uploadedFotos;

    public function __construct(Request $request, GerechtModel $gerechtModel){
        //dd("$request->route('restaurantId')");
        $this->restaurantId = $request->route('restaurantId');
        $this->gerechtModel = $gerechtModel;
    }

    /**
     * Geef een lijst van alle gerechten met specifieke restaurant id (die is geselecteerd)
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(FotosModel $fotosModel)
    {
        // Garbage collection - Check alle of er temporary gerecht-foto's zijn van de ingelogde gebruiker, die 'orphan' zijn...
        $fotos = $fotosModel->where('temporary',true)->where('uploaded_by',Auth::user()->id)->get();
        if(!$fotos->isEmpty()){
            $deletedRows = $fotosModel->where('temporary',true)->where('uploaded_by',Auth::user()->id)->delete();
            foreach($fotos->toArray() as $foto){
                $toDelete = $foto['path'] . '/' . $foto['filename'];
                if(Storage::exists($toDelete))
                    Storage::delete($toDelete);
            }
        }

        // Haal overzicht op
        $output = $this->gerechtModel->where('restaurants_id', $this->restaurantId)->get();
        return view('pages.restaurant.gerechten.gerechten', ['gerechten' => $output, 'restaurantId' => $this->restaurantId]);
    }

    /**
     * @param RestaurantModel $restaurantModel
     * @param VerpakkingenModel $verpakkingenModel
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create(RestaurantModel $restaurantModel, VerpakkingenModel $verpakkingenModel)
    {
        $restaurantData = $restaurantModel->where('id',$this->restaurantId)->first();
        $verpakkingen = $verpakkingenModel->where('restaurant_id',$this->restaurantId)->get();

        $fotoData = array();

        return view('pages.restaurant.gerechten.gerechten_create', [
            'restaurantNaam' => $restaurantData->naam_restaurant,
            'restaurantId' => $restaurantData->id,
            'verpakkingen' => $verpakkingen,
            'fotoData' => $fotoData
        ]);
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request, FotosModel $fotosModel)
    {
        $regex = '/^\d*(\.\d{2})?$/'; // getallen tot 2 cijfers na de komma..

        $rules = [
            'naam' => 'required',
            'prijs' => 'required|regex:'.$regex,
            'btw' => 'required|numeric|min:0|max:100',
            'bereidingstijd' => 'required|numeric|min:0|max:1320'
        ];

        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return redirect('/restaurant/' . $this->restaurantId . '/gerechten/create')
                ->withErrors($validator)
                ->withInput();             // Zorgt ervoor dat we in detail, de 'old' kan gebruiken in de view
        }

        $data = $request->except('_token');

        // restaurant Id mee bewaren in gerecht data
        $data['restaurants_id'] = $this->restaurantId;
        $result = $this->gerechtModel->create($data);

        //-- Check temporary foto's, indien 'create' successfull, move fotos onder final 'gerecht' location
        if($result){
            // Check alle nieuwe temporary foto's van de ingelogde gebruiker...
            $fotos = $fotosModel->where('temporary',true)->where('uploaded_by',Auth::user()->id)->get();
            if($fotos){
                $uploadedFotos = $fotos->toArray();
                foreach($uploadedFotos as $foto){
                    $temporaryLocation = $foto['path'] . '/' . $foto['filename'];
                    $newFotoPath = str_replace('/tmp', '/' . $result->id, $foto['path']);
                    $finalLocation = $newFotoPath . '/' . $foto['filename'];

                    if(Storage::exists($temporaryLocation)){
                        Storage::move($temporaryLocation, $finalLocation);
                        $test = $fotosModel->where('filename', $foto['filename'])->where('temporary',true)->where('uploaded_by',Auth::user()->id)->first();
                        $test->update([
                            'temporary' => false,
                            'gerechten_id' => $result->id,
                            'path' => $newFotoPath
                        ]);
                    }
                }
            }
        }

        return redirect ('restaurant/' . $this->restaurantId . '/gerechten');
    }

    /**
     * Laad en toon 1 specifiek gerecht
     * Bijvb: http://eindwerk.syntra/restaurant/1/gerechten/50
     * @param User $userModel
     * @param RestaurantModel $restaurantModel
     * @param VerpakkingenModel $verpakkingenModel
     * @param $restaurantId
     * @param $gerechtId
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show(User $userModel, RestaurantModel $restaurantModel, VerpakkingenModel $verpakkingenModel, $restaurantId, $gerechtId, FotosModel $fotosModel, \App\Lib\GlideHandler $glideHandler)
    {
        if($this->bevatGerecht($restaurantId, $gerechtId)){
            $gerecht = $this->gerechtModel->where('id',$gerechtId)->first();
            $restaurantData = $restaurantModel->where('id',$this->restaurantId)->first();
            $verpakkingen = $verpakkingenModel->where('restaurant_id',$this->restaurantId)->get();
            $fotos = $fotosModel->where('gerechten_id',$gerechtId)->get();

            $fotoData = [];

            if($fotos){
                foreach($fotos as $foto){
                    $id = $foto->id;

                    Log::info('0 - Initiële trigger: glideHandler->createImageUrl vanuit controller:');
                    Log::info('    Samen met origineel pad uit database: "' . $foto->path . '/' . $foto->filename . '"');

                    $path = $glideHandler->createImageUrl($foto->path . '/' . $foto->filename, ['w' => 300, 'h' => 300, 'fit' => 'crop']);
                    // test opties: , 'mark' => '/copyright.png', 'markw' => 300, 'markh' => 300, 'markfit' => 'max', 'markalpha' => 30, 'filt' => 'sepia'
                    //dd($glideHandler);

                    Log::info('    urlBuilder geeft een aanroepbare URL terug, met de nodige parameters:');
                    Log::info('    "' . $path . '"' );

                    $fotoData[] = ['id' => $id, 'path' => $path];
                }
            }

            dump($fotoData);

            return view('pages.restaurant.gerechten.gerechten_create', [
                'restaurantNaam' => $restaurantData->naam_restaurant,
                'restaurantId' => $restaurantData->id,
                'verpakkingen' => $verpakkingen,
                'gerechtData' => $gerecht,
                'fotoData' => $fotoData
            ]);
        }
    }

    /**
     * Laad specifiek gerecht om te editeren...
     * Bijvb: http://eindwerk.syntra/restaurant/1/gerechten/50/edit
     * @param VerpakkingenModel $verpakkingenModel
     * @param RestaurantModel $restaurantModel
     * @param $restaurantId
     * @param $gerechtId
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(VerpakkingenModel $verpakkingenModel, RestaurantModel $restaurantModel, $restaurantId, $gerechtId, FotosModel $fotosModel, \App\Lib\GlideHandler $glideHandler)
    {
        // Check eerst of het te editeren gerecht ID wel bij het juiste restaurant behoort...
        // Geef het restaurant voor 1 bepaald gerecht -- todo : werkt niet, geeft geen restaurant terug?!
        $gerecht = $this->gerechtModel->where('id', $gerechtId)->where('restaurants_id', $restaurantId)->first();

        if($this->bevatGerecht($restaurantId, $gerechtId)) {
            $restaurantData = $restaurantModel->where('id', $this->restaurantId)->first();
            $verpakkingen = $verpakkingenModel->where('restaurant_id', $this->restaurantId)->get();
            $fotos = $fotosModel->where('gerechten_id',$gerechtId)->get();

            $fotoData = [];
            if($fotos){
                foreach($fotos as $foto){
                    $id = $foto->id;

                    Log::info('0 - Initiële trigger: glideHandler->createImageUrl vanuit controller:');
                    Log::info('    Samen met origineel pad uit database: "' . $foto->path . '/' . $foto->filename . '"');

                    $path = $glideHandler->createImageUrl($foto->path . '/' . $foto->filename, ['w' => 300, 'h' => 300, 'fit' => 'crop']);
                    // test opties: , 'mark' => '/copyright.png', 'markw' => 300, 'markh' => 300, 'markfit' => 'max', 'markalpha' => 30, 'filt' => 'sepia'
                    //dd($glideHandler);

                    Log::info('    urlBuilder geeft een aanroepbare URL terug, met de nodige parameters:');
                    Log::info('    "' . $path . '"' );

                    $fotoData[] = ['id' => $id, 'path' => $path];
                }
            }

            //dd($fotoData);

            return view('pages.restaurant.gerechten.gerechten_create', [
                'restaurantNaam' => $restaurantData->naam_restaurant,
                'restaurantId' => $restaurantData->id,
                'verpakkingen' => $verpakkingen,
                'gerechtData' => $gerecht,
                'fotoData' => $fotoData
            ]);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $restaurantId, $gerechtId, FotosModel $fotosModel)
    {
        $regex = '/^\d*(\.\d{2})?$/'; // getallen tot 2 cijfers na de komma..

        $rules = [
            'naam' => 'required',
            'prijs' => 'required|regex:'.$regex,
            'btw' => 'required|numeric|min:0|max:100',
            'bereidingstijd' => 'required|numeric|min:0|max:1320'
        ];

        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return redirect('/restaurant/' . $this->restaurantId . '/gerechten/' . $gerechtId . '/edit')
                ->withErrors($validator)
                ->withInput();             // Zorgt ervoor dat we in detail, de 'old' kan gebruiken in de view
        }

        //dd($request->toArray());

        if($this->bevatGerecht($restaurantId, $gerechtId)){
            $data = $request->except(['_token', '_method']);

            //dd($data);

            $gerecht = $this->gerechtModel->find($gerechtId);
            $result = $gerecht->update($data);

            //-- Check temporary foto's, indien 'update' successfull, mogen ze als finaal worden gemarkeerd!
            if($result){
                // Check alle nieuwe temporary foto's van de ingelogde gebruiker...
                $fotos = $fotosModel->where('temporary',true)->where('uploaded_by',Auth::user()->id)->get();
                if($fotos){
                    $uploadedFotos = $fotos->toArray();
                    foreach($uploadedFotos as $foto){
                        $test = $fotosModel->where('filename', $foto['filename'])->where('temporary',true)->where('uploaded_by',Auth::user()->id)->first();
                        $test->update([
                            'temporary' => false,
                        ]);
                    }
                }
            }
            return redirect('/restaurant/' . $restaurantId . '/gerechten/');
        }
    }

    /**
     * Verwijder gerechten...
     *
     * @param $restaurantId
     * @param $gerechtId
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function destroy($restaurantId, $gerechtId)
    {
        if($this->bevatGerecht($restaurantId, $gerechtId)){
            $this->gerechtModel->where('id',$gerechtId)->delete();

            // Geef een lijst van alle gerechten met specifieke restaurant id (die is geselecteerd)
            $output = $this->gerechtModel->where('restaurants_id', $this->restaurantId)->get();
            return view('pages.restaurant.gerechten.gerechten', [
                'gerechten' => $output,
                'restaurantId' => $this->restaurantId
            ]);
        }
    }

    public function bevatGerecht($restaurantId, $gerechtId){

        $gerecht = $this->gerechtModel->where('id', $gerechtId)->where('restaurants_id', $restaurantId)->first();

        if($gerecht)
            return true;
        else {
            abort(404, 'Dit gerecht bestaat niet in dit restaurant!');
        }
    }

    /**
     * API call, Bewaart foto's in een property
     *
     * @param Request $request
     * @param FotosModel $fotosModel
     * @param ApiResponse $apiResponse
     * @return \Illuminate\Http\JsonResponse
     */
    public function uploadAction(Request $request, FotosModel $fotosModel, ApiResponse $apiResponse){
        Log::info('uploadAction');

        // Probleem - parallel uploads (ajax) zorgt ervoor dat de session variabelen niet allen worden ingevuld...
        // Gelijkaardig aan: https://github.com/laravel/framework/issues/7549
        // parallelUploads: 2 -> 1 lost het probleem op, maar toch...
        // Oplossing: bewaar foto's rechtstreeks in fotos database, maar met een extra vlag temporary = 1 ofzo...

        $temp = true;
        $restaurantId = $request->get('restaurantId');
        $gerechtId = $request->get('gerechtId');

        // Check of geposte restaurantId geen gefakete is... (anders vlieg je d'r uit...)
        if(Auth::user()->canManageRestaurant($restaurantId));

        // 2 situaties : create new gerecht => gerechtId bestààt niet
        //               edit bestaand gerecht => gerechtId = value

        if($gerechtId){ // gerechtId bestaat, alle uploads zijn *bijna* definitief
            $originalPath = 'images/' . $restaurantId . '/' . $gerechtId;
            $dbPath = $restaurantId . '/' . $gerechtId;
            //$temp = false; // disable reason: altijd temporary, tot ze is gesaved...
        } else {        // gerechtId bestaat NIET, uploads zijn tijdelijk
            $originalPath = 'images/' . $restaurantId . '/tmp';
            $dbPath = $restaurantId . '/tmp';
        }

        $file = $request->file('file');
        $fileName = $this->validateFileName($fotosModel, $originalPath, $file); // Duplicaten checken
        $path = $file->storeAs($originalPath, $fileName); // Opslaan
        $extensie = Str::lower($file->getClientOriginalExtension());

        // Value of null, dus geen if nodig...
        $data = [
            'path' => $dbPath,
            'filename' => $fileName,
            'extensie' => $extensie,
            'gerechten_id' => $gerechtId,
            'restaurants_id' => $restaurantId,
            'temporary' => $temp,
            'uploaded_by' => Auth::user()->id
        ];

        if($path)
            $result = $fotosModel->create($data); // Opslaan foto-database
        else
            return Response::json([
                'data' => $data['filename'],
                'error' => 'Unable to copy file!'
            ], 400);

//        ob_start();
//           var_dump($request->get('fotoUploadData'));
//           $ses = ob_get_contents();
//           ob_get_clean();
//        Log::info($ses);
//        Log::info($file->getClientOriginalName());

        if($result)
            return $apiResponse->create($result);
    }

    /**
     * Valideert / checkt of file bestaat. Zo ja, rename file naam met préfix _1, _2...
     *
     * @param $fotoModel
     * @param $path
     * @param $file
     * @return mixed|string
     */
    public function validateFileName($fotoModel, $path, $file)
    {
        $name = Str::lower($file->getClientOriginalName());

        // We checken WEL op duplicaten in de database...
        $original = $name;
        $counter = 1;
        while ($fotoModel->where('filename', $name)->count() || File::exists(storage_path('app/' . $path . '/'. $name))) {
            $ext = "." . Str::lower($file->getClientOriginalExtension());
            $name = str_replace($ext, "_" . $counter . $ext, $original);
            $counter++;
        }
        return $name; //endif
    }

    public function fotos_delete($restaurantId, $gerechtId, $fotoId, FotosModel $fotosModel){
        // Check of geposte restaurantId geen gefakete is... (anders vlieg je d'r uit...)
        if(Auth::user()->canManageRestaurant($restaurantId));

        $foto = $fotosModel->find($fotoId);

        // Remove from disk
        $path = 'images/' . $restaurantId .'/' . $gerechtId;
        $filename = $foto->filename;

        // Indien geen file, zal er geen fout zijn, dus safe:
        Storage::delete($path . '/' . $filename);

        // Remove from Database
        if(!is_null($foto))
            $foto->delete();

        return redirect('/restaurant/'. $restaurantId .'/gerechten/' . $gerechtId . '/edit');
    }
}
