<?php

namespace App\Http\Controllers;

use App\BestelbonModel;
use App\RestaurantModel;
use App\GerechtModel;
use App\LeveringsAdressenModel;
use Illuminate\Http\Request;
use Auth;
use Cart;
use Carbon\Carbon;
use Event;
use App;
use Geocoder;
use Validator;
use App\Custom\CalculateOrderInvoice;

class WinkelwagenController extends Controller
{
    public function inhoud(LeveringsAdressenModel $leveringsAdressenModel, Request $request){

        // Moest ik die data nu eens in een session kunnen stoppen, dan weet ik welke de laatste 'route' was
        // vooraleer ik in de winkewagen ben gegaan waar ik vanalles heb uitgestoken...

        // Die kan ik gebruiken om terugNaarGerechten() hieronder naar de vorige route terug te leiden...
        //dd($request->headers->get('referer'));

        $userId = Auth::getUser()->id;
        $leveringsadressen = $leveringsAdressenModel->where('user_id', $userId)->get();

        //dd($leveringsAdressenModel->where('user_id', Auth::getUser()->id)->first());

        $defaultAddress = [
            'straat' => '',
            'nr' => '',
            'extension' => '',
            'postcode' => '',
            'stad' => ''
        ];

        //dump($leveringsadressen);
        //dump(count($leveringsadressen));

        // Als er geen leveringsadressen zijn gedefiniëerd, gebruik default user account adres...
        if(!count($leveringsadressen)){
            //dump("collection is empty");
            $defaultAddress = [
                'straat' => isset(Auth::getUser()->straat) ? Auth::getUser()->straat : '',
                'nr' => isset(Auth::getUser()->nr) ? Auth::getUser()->nr : '',
                'extension' => isset(Auth::getUser()->extension) ? Auth::getUser()->extension : '',
                'postcode' => isset(Auth::getUser()->postcode) ? Auth::getUser()->postcode : '',
                'stad' => isset(Auth::getUser()->gemeente) ? Auth::getUser()->gemeente : '',
            ];
        }
//        // Retrieve "default address" from logged-in user account
//            dump("found user id");
//        if(isset(Auth::getUser()->straat))
//            dump(Auth::getUser()->straat);
//            $defaultAddress[] = [ 'straat' => Auth::getUser()->straat ];
//            dump($defaultAddress);
//        if(isset(Auth::getUser()->nr))
//            $defaultAddress[] = [ 'nr' => Auth::getUser()->nr ];
//        if(isset(Auth::getUser()->extension))
//            $defaultAddress[] = [ 'extension' => Auth::getUser()->extension ];
//        if(isset(Auth::getUser()->postcode))
//            $defaultAddress[] = [ 'postcode' => Auth::getUser()->postcode ];
//        if(isset(Auth::getUser()->gemeente))
//            $defaultAddress[] = [ 'stad' => Auth::getUser()->gemeente ];

        //dump($defaultAddress);

        return view('pages.public.winkelwagen', [
                'leveringsadressen' => $leveringsadressen,
                'defaultLeveringsAdres' => $defaultAddress
            ]);
    }

    // Ik weet niet of ik het in 1 of 2 stappen ging doen -> beslist, alles hierboven. Later zien we wel...
    // Bij checkout doe je eigenlijk niets, enkel een stukje JS om de onderste blok weer te geven...
    public function checkout()
    {

    }

    public function remove($rowId){
        Cart::remove($rowId);
        return redirect()->back();
    }

    public function update(Request $request){
        Cart::update($request->rowId, $request->aantal);
        return redirect()->back();
    }

    // Volledige winkelwagen leeg maken
    public function resetCart(){
        Cart::destroy();
        return redirect()->back();
    }

    public function plaatsOrder(Request $request, RestaurantModel $restaurantModel, GerechtModel $gerechtModel){
        $errors = [];

        // Simpele validator die checkt of er wel iets in het winkelwagentje zit?
        if(Cart::count() == 0){
            $errors[0] = "Het winkelwagentje is momenteel leeg.  Er kan geen bestelling worden geplaatst!";
            return redirect('/winkelwagen')
                ->withErrors($errors);
        }

//        // Check uit welk restaurant de content van het bestelmandje wordt besteld
//        $gerechtId = Cart::content()->first()->id;
//        $restaurantId = $gerechtModel->find($gerechtId)->restaurants_id;
//        $restaurant = $restaurantModel->find($restaurantId);
//
//        // Stap 1: creëer een nieuwe bestelbon (velden: bestellingstatusen_id, leveranciers_id, klanten_id, extrawachttijd,
//        $bestelbon = new BestelbonModel;
//        $bestelbon->bestellingstatusen_id = 1;
//        $bestelbon->klanten_id = Auth::getUser()->id;
//        $bestelbon->bestellingtijdstip = Carbon::now();
//        $bestelbon->extrawachttijd = 0;
//        $bestelbon->save();
//
//
//        // Instance bestelbon bevat id. Dus deze instance gebruiken, om de items in de bon te kunnen bewaren...
//        // Cart aflopen en alle items attachen in de pivot table
//        foreach(Cart::content() as $content){
//            $bestelbon->gerechten()->attach($content->id, [
//                'aantal' => $content->qty,
//                'eenheidsprijs' => $gerechtModel->find($content->id)->prijs
//            ]);
//        }
//
//        dd($request);

        // Test result
        // dump(\App\BestelbonModel::with('gerechten')->find($bestelbon->id));

        // bevatten van het winkelmandje (cart data in een variabele doorgeven voor de e-mail, en cart-data resetten)
        // als test doen we een call uit de database...
        //$data = \App\BestelbonModel::with('gerechten')->find($bestelbon->id);

//        $data2 = \App\GerechtModel::with('bestelbons')->find(1); // zoek je gerecht-id
//        $data2 = \App\GerechtModel::where('id', 1)->with('bestelbons')->first(); // zoek je gerecht-id
//
//        // specifiek gerecht uit bestelbon halen
//        $data3 = \App\BestelbonModel::where('id', 12)->with(['gerechten' => function($query){
//            $query->where('gerechten.id', 1);  // bon nummer 12
//        }])->first(); // zoek je gerecht-id
//
//        // TESTEN
//        // specifieke bestelbon uit gerecht halen
//        $data2 = \App\GerechtModel::where('id', 1)->with(['bestelbons' => function($query){
//            $query->where('bestelbons.id', 12);  // bon nummer 12
//        }])->first(); // zoek je gerecht-id
//
//        dump($data2->prijs); // prijs individueel gerecht
//
//
//        // aantal zoeken uit pivot van bestelbonModel (1 bestelling zal altijd uniek gerecht id hebben voor aantal...)
//        dump($data3->gerechten->each(function($gerecht){
//            return $gerecht->pivot->aantal;
//        }));

        // If no "address_option" containing "leveringsadres ID", create new...
        if(!isset($request->address_option)){
            //dump("Other addres selected - Retrieve address from User database.  Validate input of Other address.");

            //dump($request->toArray());

            $data = $request->except('_token','other_address');

            //dump($data);

            // Vallidatie ingevoerde / gewijzigde leveringsadres...
            $validator = Validator::make($data, [
                'straat' => 'required|min:3',
                'nr' => 'required|numeric',
                'postcode' => 'required|digits:4',
                'stad' => 'required'
            ]);

            if ($validator->fails()) {
                return redirect('/winkelwagen/')
                    ->withErrors($validator)
                    ->withInput(); // Zorgt ervoor dat we in detail, de 'old' kan gebruiken in de view
            }

            //if($request->adresbewaren){  // Laten we ervan uitgaan: we bewaren ALTIJD het adres...
                dump("Save to leveringsadressen if ok...");
                $leveringsAdressenModel = new LeveringsAdressenModel;
                $leveringsAdressenModel->straat = $request->straat;
                $leveringsAdressenModel->nr = $request->nr;
                $leveringsAdressenModel->extension = $request->extension;
                $leveringsAdressenModel->postcode = $request->postcode;
                $leveringsAdressenModel->stad = $request->stad;
                $leveringsAdressenModel->land = $request->land;
                $leveringsAdressenModel->user_id = Auth::getUser()->id;

                $coordinates = ['lat' => 0, 'lng' => 0, 'accuracy' => null, 'formatted_address' => null];

                // Mogelijke bug - stinkaard moeilijk te vinden: als er geen enkel adres bestaat, wordt de fout getriggerd...

                try {

                $coordinates = Geocoder::getCoordinatesForQuery($request->straat . ' ' .
                                                                      $request->nr . ',' .
                                                                      $request->postcode . ' ' .
                                                                      $request->stad . ',' .
                                                                      $request->land);

                } catch (\Exception $e) {
                    echo 'Caught exception: ',  $e->getMessage(), "\n";
                    
                }

                // dd($coordinates);

                $leveringsAdressenModel->lat = $coordinates['lat'];
                $leveringsAdressenModel->lng = $coordinates['lng'];
                $leveringsAdressenModel->accuracy = $coordinates['accuracy'];
                $leveringsAdressenModel->formatted_address = $coordinates['formatted_address'];

                $leveringsAdressenModel->save();

                // todo : *check fillable fields* $leveringsAdressenModel + (!!!) zoek gps coordinaten op tijdens bestelling (in view) om mee op te slaan, als het kàn...
                // velden te kort: lat, lng

            //}

            //$bestelbon->leveringsadressen_id = $leveringsAdressenModel->id; // in het geval, nieuw adres...
        } else {
            //$bestelbon->leveringsadressen_id = $request->address_option;
        }

        // Check uit welk restaurant de content van het bestelmandje wordt besteld
        $gerechtId = Cart::content()->first()->id;
        $restaurantId = $gerechtModel->find($gerechtId)->restaurants_id;
        $restaurant = $restaurantModel->find($restaurantId);

        // Creëer een nieuwe bestelbon (velden: bestellingstatusen_id, leveranciers_id, klanten_id, extrawachttijd,
        $bestelbon = new BestelbonModel;
        $bestelbon->bestellingstatusen_id = 1;
        $bestelbon->klanten_id = Auth::getUser()->id;
        $bestelbon->restaurant_id = $restaurantId;
        $bestelbon->bestellingtijdstip = Carbon::now();
        $bestelbon->extrawachttijd = 0;   // later te implementeren door in UI "gewenste leveringstijd" in bestelling te geven

        if(!isset($request->address_option)){
            $bestelbon->leveringsadressen_id = $leveringsAdressenModel->id; // in het geval, nieuw adres...
        } else {
            $bestelbon->leveringsadressen_id = $request->address_option;
        }

        $bestelbon->save();

        // Id bestelbon krijg je pas na saven string ID - concatenate met underscore + timestamp in string-vorm

        $bestelbon->followup_url_hash = hash('sha1', (string)$bestelbon->id . '_' .(string)time());
        $bestelbon->save();

        // Instance bestelbon bevat id. Dus deze instance gebruiken, om de items in de bon te kunnen bewaren...
        // Cart aflopen en alle items attachen in de pivot table
        $maxBereidingsTijd = 0;
        foreach(Cart::content() as $content){
            $bestelbon->gerechten()->attach($content->id, [
                'aantal' => $content->qty,
                'eenheidsprijs' => $gerechtModel->find($content->id)->prijs
            ]);

            // alle gerechten uit bestelbon overlopen, zoek de 'langste' bereidingstijd
            if($gerechtModel->find($content->id)->bereidingstijd > $maxBereidingsTijd)
                $maxBereidingsTijd = $gerechtModel->find($content->id)->bereidingstijd;
        }

        // voeg som 'langste' bereidingstijd + extrawachttijd samen met Carbon::now() om de geschatte ready4pickup te bepalen
        $result = Carbon::parse($bestelbon->bestellingtijdstip)->addMinutes($maxBereidingsTijd);
        $bestelbon->ready4pickuptijdstip = $result;
        $bestelbon->save();

        // bevatten van het winkelmandje (cart data in een variabele doorgeven voor de e-mail, en cart-data resetten)
        // als test doen we een call uit de database...
        $maildata = \App\BestelbonModel::with('gerechten')->find($bestelbon->id);

        // e-mail zenden via event met cartdata + een link naar de bestelstatusen?
        Event::fire(new App\Events\BestellingOntvangenEvent($maildata, $restaurant)); // Verdere synchrone of asynchrone afhandeling, bevestigingen via e-mail...

        //dump("Bestelling werd geplaatst!");

        Cart::destroy();

        //Redirecten naar window met opvolging en bestelstatusen?
        return redirect('/followup/' . $bestelbon->id . '/' . $bestelbon->followup_url_hash);
    }

    public function terugNaarGerechten(GerechtModel $gerechtModel){

        // zie bovenaan index, eventueel via session (maar doen we niet, geen tijd)
        if(Cart::count() === 0){
            return redirect('/');
        }

        $gerechtId = Cart::content()->first()->id;
        $restaurantId = $gerechtModel->find($gerechtId)->restaurants_id;

        return redirect('/' . $restaurantId . '/menu');
    }
}
