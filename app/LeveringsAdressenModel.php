<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LeveringsAdressenModel extends Model
{
    protected $table = 'leveringsadressen';

    protected $hidden = [
        'user_id'
    ];

    public function user()
    {
        return $this->belongsTo('App\LeveringsAdressenModel');
    }


    public function bestelbons()
    {
        return $this->hasMany(BestelbonModel::class);
    }
}
