<?php
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 18/04/17
 * Time: 21:21
 */

namespace App\Modules;

use Response;

class ApiResponse
{
    /**
     * Creates a 'standard, uniform response output' for all API calls
     * @param $result
     * @param array $errors
     * @return \Illuminate\Http\JsonResponse
     */
    public function create($result, array $errors = []) {

        $response = [
            'result' => $result,
            'status' => count($errors) ? 'error' : 'success',
            'errors' => $errors
        ];

        if (is_null($result))
            return Response::json($response, 200);

        return Response::json($response, 200);
    }
}