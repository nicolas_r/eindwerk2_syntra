<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'firstname', 'lastname', 'tel', 'straat', 'nr', 'extension', 'postcode', 'gemeente'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function roles()
    {
        return $this->belongsToMany(Role::class)->withTimestamps();
    }

    public function bestelbons()
    {
        return $this->hasMany(BestelbonModel::class);
    }

    // Helper functie - Checkt of gebruiker de juiste rol / privilege heeft
    public function hasRole($name)
    {
        foreach ($this->roles as $role)
        {
            if ($role->name == $name) return true;
        }

        return false;
    }

    // Helper functie - Voeg role toe aan gebruiker
    public function assignRole($role)
    {
        $this->roles()->attach($role);
    }

    // Helper functie - Verwijder role van gebruiker
    public function removeRole($role)
    {
        $this->roles()->detach($role);
    }

    public function leveringsadressen()
    {
        return $this->hasMany('App\LeveringsAdressenModel');
    }

    public function restaurants()
    {
        return $this->hasMany('App\RestaurantModel', 'users_id', 'id');
    }

    /******************************************************************************************************************
     * Helper functie die checkt of een non-admin user, wel het desbetreffende restaurant (id) mag beheren
     *
     * @param $restaurantId
     * @return bool
     *****************************************************************************************************************/
    public function canManageRestaurant($restaurantId){

        $result = null;

        if($this->hasRole('admin')){

            //-- Admins mogen *alle* restaurants ophalen en raadplegen
            if($this->with('restaurants')->first())
                return true;

        } elseif($this->hasRole('restaurant')) {

            //-- Rest mag enkel restaurants raadplegen, toegekend aan hun *eigen account* user_id
            $result = $this->where('id', \Illuminate\Support\Facades\Auth::user()->id)->with('restaurants')->first();
            if($result->restaurants->contains('id', $restaurantId))
                return true;

        } else {

            //-- Geen role als 'admin' of 'restaurant'? Geen toegang!
            return false;
        }
    }
}
