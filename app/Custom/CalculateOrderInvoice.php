<?php

namespace App\Custom;
use App\GerechtModel;

/**
 * Created by PhpStorm.
 * User: nicol
 * Date: 17/06/2017
 * Time: 2:48
 */
class CalculateOrderInvoice
{
    private $bestelbon_data = [];
    private $order;
    private $aantal_items = 0;

    public function __construct($order)
    {
        $this->order = $order;
    }

    public function getOrderInvoice(){
        $this->bestelbon_data['order_nr'] = $this->order->id;
        $this->bestelbon_data['order_items'] = $this->calculateInvoiceSubtotals();
        $this->bestelbon_data['totaal'] = $this->calculateTotals();
        $this->bestelbon_data['totaal']['aantal_items'] = $this->aantal_items;
        return $this->bestelbon_data;
    }

    public function getOrderItems(){
        return $this->bestelbon_data['order_items'] = $this->calculateInvoiceSubtotals();
    }

    public function setNewOrderData($order){
        $this->order = $order;
    }

    private function calculateInvoiceSubtotals(){
        $orderdata = [];
        $i = 0;

        $gerechtModel = new GerechtModel;

        foreach($this->order->gerechten as $gerecht){
            $orderdata[$i] = [];
            $gerechtData = $gerechtModel->find($gerecht->pivot->gerechten_id);
            $orderdata[$i]['artikelnr'] = $gerechtData->id;
            $orderdata[$i]['gerechtnaam'] = $gerechtData->naam;
            $orderdata[$i]['eenheidsprijs'] = $gerechtData->prijs;
            $orderdata[$i]['aantal'] = $gerecht->pivot->aantal;
            $orderdata[$i]['btw_basis'] = $gerechtData->btw;
            $orderdata[$i]['bereidingstijd'] = $gerechtData->bereidingstijd;
            $orderdata[$i]['verpakking'] = $gerechtData->verpakkingen_id;
            $orderdata[$i]['subtotaal'] = $gerecht->pivot->aantal * $gerecht->pivot->eenheidsprijs;
            $orderdata[$i]['subtotaal_btw'] = $this->calculateBtw($gerecht, $orderdata[$i]['subtotaal']);
            $this->aantal_items += $gerecht->pivot->aantal;
            $i++;
        }

        return $orderdata;
    }

    private function calculateBtw($orderItem, $netPrice){
        $gerechtModel = new GerechtModel;
        return $netPrice * $gerechtModel->find($orderItem->pivot->gerechten_id)->btw / 100;
    }

    private function calculateTotals(){
        $totals = [
            'excl_btw' => 0,
            'btw' => 0,
            'aantal_artikelsoorten' => 0,
            'algemeen' => 0
        ];

        foreach($this->bestelbon_data['order_items'] as $index => $orderItem){
            $totals['excl_btw'] += $orderItem['subtotaal'];
            $totals['btw'] += $orderItem['subtotaal_btw'];
            $totals['algemeen'] = $totals['btw'] + $totals['excl_btw'];
            $totals['aantal_artikelsoorten'] = $index + 1;
        }

        return $totals;
    }
}
