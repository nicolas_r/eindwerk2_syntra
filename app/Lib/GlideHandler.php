<?php
/**
 * Created by PhpStorm.
 * User: BigfishDim
 * Date: 23/02/17
 * Time: 20:19
 */

namespace App\Lib;
use League\Glide\Server;
use League\Glide\Signatures\SignatureFactory;
use League\Glide\Urls\UrlBuilderFactory;
use Log;

class GlideHandler
{
    private $key = "syntra";
    private $server = null;


    public function __construct(Server $server)
    {
        $this->server = $server;
    }

    /**
     * Wordt getriggerd (en dus aangeroepen) uit controller, als een nieuw beeld moet worden aangemaakt...
     * @param $name
     * @param array $options
     * @param string $path
     * @return string
     */
    public function createImageUrl($name, $options = [], $path = '/img'){

        Log::info('1 - UrlBuilderFactoryBuilding in GlideHandler.php creëert een URL:');

        $urlBuilder = UrlBuilderFactory::create($path , $this->key);

        Log::info('    urlBuilder object aangemaakt, pad URL uitgehaald & aan controller teruggegeven...');

        return $urlBuilder->getUrl($name, $options);
    }

    /**
     * Wordt getriggerd wanneer de route wordt aangeroepen, bouwt het effectieve beeld / eindfoto op de pagina
     * $path is het originele pad
     * $data is de getriggerde Request object uit de router (in deze functie hier worden 'magisch' de url parameters
     * eruit gefilterd, waarop wordt gevalideerd (size, key, opties...)
     * @param $path
     * @param $data
     */
    public function createImage($path, $data){

        // dd($data);

        Log::info('3 - createImage: bij creatie foto, werd een validatie sleutel mee gegenereerd, dus gechecked');

        SignatureFactory::create($this->key)->validateRequest('/img/' . $path, $data);  // img veranderd naar images
        Log::info("4 - Tenslote wordt wordt het output beeld gemaakt: " . '"/images/' . $path . '"');

        // -- create image -- to where?
        return $this->server->outputImage('/images/' . $path, $data);
    }
}