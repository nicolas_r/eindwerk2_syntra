<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class BestellingOntvangenEvent
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $bestelbonData;
    public $restaurantData;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($bestelbonData, $restaurantData)
    {
        $this->bestelbonData = $bestelbonData;
        $this->restaurantData = $restaurantData;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
