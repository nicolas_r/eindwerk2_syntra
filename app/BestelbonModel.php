<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Custom\CalculateOrderInvoice;

class BestelbonModel extends Model
{
    protected $table = 'bestelbons';

    protected $fillable = [
        'bestellingstatusen_id',
        'leveranciers_id',
        'klanten_id',
        'bereidingstijdstip',
        'extrawachttijd',
    ];

    protected $hidden = [
        'klanten_id',
        'leveranciers_id',
        'leveringsadressen_id',
        'followup_url_hash'
    ];

    public function gerechten()
    {
        return $this->belongsToMany('App\GerechtModel', 'bestelbons_gerechten', 'bestelbons_id', 'gerechten_id')
            ->withPivot('aantal', 'eenheidsprijs')
            ->withTimestamps();
    }

    public function leveringsadres()
    {
        return $this->belongsTo(LeveringsAdressenModel::class, 'leveringsadressen_id');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'klanten_id');
    }

    public function bestellingstatus()
    {
        return $this->belongsTo(BestellingStatusModel::class, 'bestellingstatusen_id');
    }

    public function restaurant()
    {
        return $this->belongsTo('App\RestaurantModel');
    }

    public function getTotaalBedragAttribute()
    {
        $invoiceCalculation = new CalculateOrderInvoice($this);
        return $invoiceCalculation->getOrderInvoice();
    }
}
