<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class BestelbonBevestiging extends Mailable
{
    use Queueable, SerializesModels;

    public $formdata;
    public $userdata;
    public $invoice;
    public $restaurantNaam;
    public $adresData;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($formData, $invoiceData, $userData, $restaurantNaam, $adresData)
    {
        $this->formdata = $formData;
        $this->userdata = $userData;
        $this->restaurantNaam = $restaurantNaam;
        $this->adresData = $adresData;
        $this->invoice = $invoiceData;
        //
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('emails.bevestiging-bestelling', [
            'invoicedata' => $this->invoice,
            'formdata' => $this->formdata,
            'userdata' => $this->userdata,
            'restaurantnaam' => $this->restaurantNaam,
            'adresdata' => $this->adresData
        ]);
    }
}
