<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RestaurantModel extends Model
{
    protected $table = 'restaurants';

    protected $hidden = [
        'users_id',
        'actief',
        'created_at',
        'updated_at'
    ];

    protected $fillable = [
        'naam_restaurant',
        'omschrijving',
        'email_restaurant',
        'tel_restaurant',
        'users_id',
        'actief',
        'straat',
        'nr',
        'extension',
        'postcode',
        'gemeente',
        'openingsuren'
    ];

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function gerechten()
    {
        return $this->hasMany('App\GerechtModel', 'restaurants_id', 'id');
    }

    public function orders()
    {
        return $this->hasMany('App\BestelbonModel', 'restaurant_id');
    }

    public function foto()
    {
        return $this->hasOne('App\FotosModel','id','fotos');
    }

}

