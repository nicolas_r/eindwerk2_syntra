<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GerechtModel extends Model
{
    protected $table = 'gerechten';

    protected $fillable = [
        'naam',
        'omschrijving',
        'prijs',
        'btw',
        'bereidingstijd',
        'restaurants_id',
        'verpakkingen_id',
        'fotos'
    ];

    public function restaurant()
    {
        return $this->belongsTo('App\RestaurantModel');
    }

    public function bestelbons()
    {
        return $this->belongsToMany('App\BestelbonModel','bestelbons_gerechten', 'gerechten_id', 'bestelbons_id')
            ->withPivot('aantal', 'eenheidsprijs')
            ->withTimestamps();
    }

    public function foto()
    {
        return $this->hasOne('App\FotosModel','id','fotos');
    }


}
