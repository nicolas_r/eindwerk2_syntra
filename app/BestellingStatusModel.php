<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BestellingStatusModel extends Model
{
    protected $table = 'bestellingstatusen';

    protected $hidden = [
        'created_at',
        'updated_at'
    ];

    public function bestelbons()
    {
        return $this->hasMany(BestelbonModel::class);
    }
}