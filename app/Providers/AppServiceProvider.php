<?php

namespace App\Providers;

use Illuminate\Support\Facades\Validator;
use Illuminate\Support\ServiceProvider;
use Illuminate\Contracts\Filesystem\Filesystem;
use League\Glide\ServerFactory;
use Illuminate\Support\Facades\Schema;


class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        /**
         * Custom validator voor telefoonnummers, zou voor veel landen ok moeten zijn...
         */
        Validator::extend('phone', function($attribute, $value, $parameters, $validator) {
            return preg_match('%^(?:(?:\(?(?:00|\+)([1-4]\d\d|[1-9]\d?)\)?)?[\-\.\ \\\/]?)?((?:\(?\d{1,}\)?[\-\.\ \\\/]?){0,})(?:[\-\.\ \\\/]?(?:#|ext\.?|extension|x)[\-\.\ \\\/]?(\d+))?$%i', $value) && strlen($value) >= 10;
        });

        Validator::replacer('phone', function($message, $attribute, $rule, $parameters) {
            return str_replace(':attribute',$attribute, ':attribute is invalid phone number');
        });

        Schema::defaultStringLength(191);

    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        if ($this->app->environment() !== 'production') {
            $this->app->register(\Barryvdh\LaravelIdeHelper\IdeHelperServiceProvider::class);
        }

        // !!!
        $this->registerGlide();  // NIET VERGETEN !!! NIET GEREGISTREERD = GEEN GLIDE...
    }

    /**
     * Registering Glide for image manipulation and thumbnails
     *
     * DO NOT FORGET TO REGISTER() ...
     */
    protected function registerGlide(){

        // binding to the Laravel IOC (inversion of control) container
        $this->app->singleton('League\Glide\Server', function($app) {

            // use the ioc container to get an instance of the filesystem
            $fileSystem = $app->make(Filesystem::class);

            // use the glide server factory to put it all together
            return ServerFactory::create([
                'source' => $fileSystem->getDriver(),  // file system for source images
                'cache' => $fileSystem->getDriver(),   // file system for cache for manipulated images
                'source_path_prefix' => '',
                'cache_path_prefix' => '/.cache',
                'watermarks' => $fileSystem->getDriver(),
                'watermarks_path_prefix' => '/images/watermarks',
                'presets' => [
                    'thumb' => [
                        'w' => 300,
                        'h' => 300,
                        'fit' => 'crop',
                    ]
                ]
            ]);
        });
    }
}
