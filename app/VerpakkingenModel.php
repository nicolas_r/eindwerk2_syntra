<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VerpakkingenModel extends Model
{
    protected $table = 'verpakkingen';
    protected $fillable = array('id', 'naam', 'lengte', 'breedte', 'hoogte', 'restaurant_id');
}
