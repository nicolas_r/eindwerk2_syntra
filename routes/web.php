<?php

Use App\User;
Use App\Role;
Use Illuminate\Http\Request;
Use Spatie\OpeningHours\OpeningHours;
Use Carbon\Carbon;



/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return view('layouts.app');
//});

// Main User Interface
Route::get('/', 'UIController@showRestaurants');
Route::get('{restoId}/menu', 'UIController@showGerechten');
Route::post('{restoId}/menu/toevoegen', 'UIController@addGerechtToBasket');
Route::get('/contact','UIController@contactForm')->middleware('auth');
Route::post('/contact/submit', 'UIController@submitContactForm');
//Route::post('{restoId}/menu/toevoegen', function(Request $request){
//    dd($request->toArray());
//});

Route::get('/winkelwagen', 'WinkelwagenController@inhoud')->middleware('auth');
Route::post('/winkelwagen/reset', 'WinkelwagenController@resetCart');
Route::post('/winkelwagen/bestellen', 'WinkelwagenController@plaatsOrder')->middleware('auth');
Route::get('/winkelwagen/remove/{rowId}', 'WinkelwagenController@remove');
Route::post('/winkelwagen/update', 'WinkelwagenController@update');
Route::get('/winkelwagen/back', 'WinkelwagenController@terugNaarGerechten');

/* API Calls statuses  */
Route::post('/api/checkstatus','BestellingenController@checkStatus');


/* API Calls Leveranciers APP */
Route::get('/api/overzicht', 'LeveranciersAppController@index')->middleware('cors');
Route::put('/api/overzicht/claim/{orderId}', 'LeveranciersAppController@claim')->middleware('cors');
Route::put('/api/overzicht/leveringsstatus/{orderId}', 'LeveranciersAppController@changeStatus')->middleware('cors');
Route::post('/api/overzicht/traveltime/{orderId}', 'LeveranciersAppController@travelTime')->middleware('cors');
Route::get('/api/overzicht/order/{orderId}', 'LeveranciersAppController@bestellingGereed')->middleware('cors'); // niet gebruikt

// toevoegen aan cart gebeurt reeds in UIController
//Route::post('/bestelmand/bestel', function(){
//    // Stuur e-mail met bestelling naar ontvanger
//    // Voeg data toe aan tabel bestellingen
//    //
//});

Route::get('/followup/{bestellings_id}/{hash}', 'BestellingenController@followup');

Route::get('/profile', function () {
    return view('pages.profiel');
});

Auth::routes();

Route::get('/home', 'HomeController@index');

// Pagina met lijst van alle users
Route::group(['prefix' => 'admin', 'middleware' => 'role:admin'], function(){
    Route::get('users', 'UserController@index');                  // Working!
    Route::get('user/add', 'UserController@create');
    Route::post('user/store', 'UserController@store');
    Route::get('user/profile/{id}', 'UserController@show');       // Working!
    Route::get('user/{id}', 'UserController@edit');               // - see show?
    Route::post('/user/update/{id}', 'UserController@update');    // Working!
    Route::post('/user/delete/', 'UserController@delete');        // Working!

    Route::get('api/user/read/{id}', 'UserController@showUserAjax');
    // api-'tje maken voor activer/deactiveren gebruiker?
    Route::post('api/user/toggleactivation', 'UserController@toggleActivationAjax');
});

// Restaurantbeheer
//Route::group(['prefix' => 'restaurant', 'middleware' => 'role:restaurant'], function(){
//    Route::get('beheer', 'RestaurantController@beheer');
//});

Route::group(['middleware' => 'role:restaurant'], function(){
    // API routes
    Route::get('/api/verpakkingen/{id?}', 'VerpakkingenController@index');
    Route::post('/api/verpakkingen', 'VerpakkingenController@store');
    Route::post('/api/verpakkingen/{id}', 'VerpakkingenController@update');
    Route::delete('/api/verpakkingen/{id}', 'VerpakkingenController@destroy');

    // Update wordt gebruikt, show momenteel niet gebruikt
    Route::resource('api/restaurant', 'RestaurantApiController', ['only' => [
        'update', 'show'
    ]]);

    //-- Restaurants REST
    Route::resource('restaurant', 'RestaurantController');

    //-- Alle methods in RestaurantBeheerController moeten eerst door mijn middleware 'CheckRestaurantAccess' gechecked
    //-- Admins mogen alle restaurants beheren. Non-admin restaurant houders, enkel hun eigen restaurants...
    Route::group(['middleware' => 'restaurant.access', 'prefix' => 'restaurant'], function(){
        Route::get('{restaurantId}/beheer', 'RestaurantBeheerController@home');
        Route::get('{restaurantId}/openingsuren', 'OpeningsUrenController@openingsuren');
        Route::post('{restaurantId}/openingsuren/addtime', 'OpeningsUrenController@addTime'); // add time
        Route::get('{restaurantId}/openingsuren/createTime', 'OpeningsUrenController@createTime');  // create time and save in database for this restaurant
        Route::get('{restaurantId}/openingsuren/reset', 'OpeningsUrenController@reset');  // create time and save in database for this restaurant

        //-- Gerechten - 'Nested' resource controller - REST
        Route::resource('{restaurantId}/gerechten', 'GerechtController');

        //-- Dropzone Gerechten fotos
        Route::post('{restaurantId}/gerechten/upload','GerechtController@uploadAction');

        //-- Foto gerecht verwijderen
        Route::post('{restaurantId}/gerechten/{gerechtId}/fotos/{fotoId}/delete', 'GerechtController@fotos_delete');

        //-- Beheer fotos restaurant
        Route::get('{restaurantId}/fotos', 'RestaurantController@fotos_index');
        Route::post('{restaurantId}/fotos/upload', 'RestaurantController@fotos_upload');
        Route::post('{restaurantId}/fotos/save', 'RestaurantController@fotos_bewaar_keuze');
        Route::post('{restaurantId}/fotos/{fotoId}/delete', 'RestaurantController@fotos_delete');


        // voorlopige versie, als testje
        Route::get('{restaurantId}/verpakkingen', function ($restaurantId) {
            return view('pages.restaurant.verpakking', [ 'restaurantId' => $restaurantId]);
        });

        //-- Bestellingen REST
//        Route::resource('{restaurantId}/bestellingen', 'BestellingenController');

        // Beheer bestellingen
        Route::group(['prefix' => 'bestellingen'], function(){
            Route::get('/{restaurantId}', 'BestellingenController@index')->middleware('auth');
            //Route::get('/{restaurantId}/filter/{filterId?}', 'BestellingenController@index');
            Route::get('/{restaurantId}/{bestellingId}', 'BestellingenController@detail')->middleware('auth');
            Route::post('/{restaurantId}/{bestellingId?}/wijzigstatus', 'BestellingenController@changeBestellingStatus');
            Route::post('/{restaurantId}/{bestellingId}/annuleren', 'BestellingenController@annuleerBestelling');
        });
    });
});

//Route::get('checksession', function(Request $request){
//    dump("check session");
//    dump($request->session());
//});

//Route::get('resetsession', function(Request $request){
//    if($request->session()->get('fotoUploadData'))
//        $request->session()->put('fotoUploadData', []);
//    //dump($request->session()->flush());  // Probleem: flush logt je ook volledig UIT, (user wordt ook geflushed)
//
//    dump($request->session());
//});

//Route::get('createsessionrubbish', function(Request $request){
//    $randomkey = "key" . rand(1,100);
//    $request->session()->put($randomkey, 'rubbish');
//    dump("Randomkey " . $randomkey . " added");
//});

Route::get('user/profile/{id}', 'UserController@show'); //-- Edit personal profile data
Route::post('user/profile/{id}', 'UserController@update'); //-- Update personal profile data

// Obsolete, used AJAX call instead //Route::post('user/profile/{userId}/deleteaddress', 'UserController@deleteAddress'); //-- Verwijder leveringsadres
Route::delete('user/profile/{userId}/deleteaddress/{id}', 'UserController@deleteAddressAjax'); //-- Verwijder leveringsadres via ajax-call

// Beheer user-data
Route::get('/admin/user/{id}', function(\App\User $user, $id) {
    $result = $user->where('id', $id)->first();

    if($result) {
        return "User " . $id . " found!";
    }

    return "User " . $id . " NOT FOUND!";
})->middleware('role:admin');

//-------------------------------------------------------------------------------------------------------------------
// Testjes:
// en of nl
Route::get('{locale}/verpakkingen', function ($locale) {
    App::setLocale($locale);
    return view('pages.restaurant.verpakking');
});

Route::get('/assignroles', function(\App\User $user, \App\Role $role) {

    // Werkende code MANY TO MANY TABLE
    // Vergeet niet eerst een ->first() te doen, vooraleer acties te nemen. Anders krijg je fouten...

    //-- Voeg roles toe aan users:
    //  $user->first()->roles()->attach(133);
    //  $user->first()->roles()->attach([1, 2, 3]);
    //  dump($user->with('roles')->first());
    //  dump($user->first()->roles->toArray());
    //  return $user->first()->roles;
    //  return $user->first()->roles->first()->name;

    // $user->first()->assignRole(4);  // Via helper functie in modal

    /* // Add role (name) to user...
    $role2 = $role->where('name', '=', 'restaurant')->first();
    $user = $user->first();
    $user->assignRole($role2);
    return $user; */

    //-- Geef alle users uit 1 role
    //return $role->with('users')->where('id', 2)->first();

    //-- Verwijder roles van users:
    //$user = $user->first();
    //$user->removeRole(4);


    //-- Check role:
    //$user = $user->first();
    //dump($user->name);
    //  if ($user->hasRole('admin')) return 'U bent administrator!';  // Via helper functie in modal
    //  if ($user->hasRole('restaurant')) return 'U bent restauranthouder!';  // Via helper functie in modal
    // if ($user->hasRole('leverancier')) return 'U bent leverancier!';  // Via helper functie in modal


    // Testcode



    //return
});

// Toegang beperken voor gebruikers met een onafdoende role... (enkel toegankelijk voor admins




/***********************************************************************
 * GLIDE - createImage maakt het beeld
 * In AppServiceProvider register()  niet vergeten
 * BEIDE ROUTES MOETEN ERIN ZITTEN VOOR API CALLS (input route & output route)!!
 */
Route::get('/img/{h1}/{h2}/{path}', function(\League\Glide\Server $server, \Illuminate\Http\Request $request, $h1, $h2, $path, \App\Lib\GlideHandler $glideHandler ){
    $path = $h1 . '/' . $h2 . '/' . $path;

    Log::info('2 - Router: de URL uit Blade wordt aangeroepen via GET: "' . $path . '"');
    Log::info('    GlideHandler.php createImage wordt nu aangeroepen en creëert de effectieve foto');
    Log::info('    Doorgegeven $request bevat de requestUri die ook de resterende URL querystring bevat: ');
    Log::info('    "' . $request->getRequestUri() . '"');

    //dump($request->getRequestUri());

    return  $glideHandler->createImage($path, $request->all());
});

// Werkt! --> gebruikte ik voor  api calls, voor blade gebruik $glideHandler code in controller...
//Route::get('/images/{path1}/{path2}/{name}', function(\App\Lib\GlideHandler $glideHandler, $path1, $path2, $name){
//    //dump($name);
//    $path = $path1 . '/' . $path2;
//    $imageUrl = $glideHandler->createImageUrl($name, ['w' => 200 ], $path);
//
//    Log::info('5 glide-createImageUrl Get route: "' . $imageUrl . '"');
//
//    //return $imageUrl;
//    return "<img src='" . $imageUrl . "'>";
//});

///************************************************************************
// * GLIDE
// *
// * In AppServiceProvider register() - BEIDE ROUTES MOETEN ERIN ZITTEN (input route & output route)!!
// */
//Route::get('/img/{h1}/{h2}/{path}', function(\League\Glide\Server $server, \Illuminate\Http\Request $request, $h1, $h2, $path, \App\Lib\GlideHandler $glideHandler ){
//
//    $path = $h1 . '/' . $h2 . '/' . $path;
//
//    return  $glideHandler->createImage($path, $request->all());
//
//});
//
//Route::get('/test/{path1}/{path2}/{name}', function(\App\Lib\GlideHandler $glideHandler, $path1, $path2, $name){
//
//    //dump($path1 . '/' . $path2 . '/' . $name);
//    $imageUrl = $glideHandler->createImageUrl('/' . $path1 . '/' . $path2 . '/' . $name, ['w' => 200]);
//
//    return Response::json([
//        'data' => $imageUrl
//    ], 200);
//
//    //return $imageUrl;
//    //return "<img src='" . $imageUrl . "'>";
//});

//Route::get('/images', function(\App\Lib\GlideHandler $glideHandler){
//
//    //dd($hash);
//
//    return view('images', ['handler' => $glideHandler]);
//});

// Als je de CORS module van BarryVdh gebruit, mag header() in de controller-code, eruit...
//Route::group(array('prefix' => 'cross'))


// VerifyCsrfToken.php voor api calls zonder API calls zonder CRSF tokens...
// handig als je op pagina op een externe server draait...