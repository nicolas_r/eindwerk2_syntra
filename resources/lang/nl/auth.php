<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'Het ingegeven wachtwoord komt niet overeen met uw profiel.',
    'throttle' => 'Teveel login pogingen gemaakt.  Probeer opnieuw na :seconds seconden.',

];
