<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Bevat gemeenschappelijke woorden voor controls
    |--------------------------------------------------------------------------
    |
    */


    'bewaarveranderingen' => 'Bewaar veranderingen',
    'annuleer' => 'Annuleer',
    'bewaar' => 'Bewaar',
    'bevestigverwijderen' => 'Bevestig verwijderen',
    'ja' => 'Ja',
    'neen' => 'Neen'
];