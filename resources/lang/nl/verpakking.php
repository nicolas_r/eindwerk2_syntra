<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Bevat alle verpakkingen-gerelateerde informatie
    |--------------------------------------------------------------------------
    |
    */

    'verpakkingen' => 'Packages',
    'naam' => 'Naam',
    'lengte' => 'Lengte',
    'breedte' => 'Breedte',
    'hoogte' => 'Hoogte',
    'voegtoe' => 'Voeg verpakking toe',
    'bevestigdefinitiefverwijderen' => 'Wenst u de volgende verpakking, *definitief* te verwijderen?'
];