<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => 'Wachtwoorden moeten minimum 6 karakters bevatten en moeten beide gelijk zijn.',
    'reset' => 'uw wachtwoord werd gereset!',
    'sent' => 'We hebben uw wachtwoord doorgemailed met een reset-link!',
    'token' => 'Dit wachtwoord reset token is ongeldig.',
    'user' => 'Er werd geen gebruiker met dit e-mail adres teruggevonden.',
];
