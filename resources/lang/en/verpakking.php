<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Bevat alle verpakkingen-gerelateerde informatie
    |--------------------------------------------------------------------------
    |
    */

    'verpakkingen' => 'Packages',
    'naam' => 'Name',
    'lengte' => 'Length',
    'breedte' => 'Width',
    'hoogte' => 'Height',
    'voegtoe' => 'Add packaging',
    'bevestigdefinitiefverwijderen' => 'Do you wish to delete the packing *permanently*?'

];