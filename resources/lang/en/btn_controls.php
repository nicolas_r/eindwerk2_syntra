<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Bevat gemeenschappelijke woorden voor controls
    |--------------------------------------------------------------------------
    |
    */

    'bewaarveranderingen' => 'Save Changes',
    'annuleer' => 'Cancel',
    'bewaar' => 'Save',
    'bevestigverwijderen' => 'Confirm Delete',
    'ja' => 'Yes',
    'neen' => 'No'

];