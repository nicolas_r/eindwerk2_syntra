@component('mail::message')
# Introduction

<b>U hebt een bericht gekregen van volgende klant:</b>

{{ $mailData['email'] }}

<b>Omschrijving klacht:</b>

{{ $mailData['question']}}

<b>Bericht:</b>

{{ $mailData['message'] }}

@if($mailData['question_type'] == 2)
@component('mail::button', ['url' => env('APP_URL', 'https://food4u.local') . '/restaurant/bestellingen/' . $mailData['restaurant_id'] . '/' . $mailData['bestelbon_nr'] ])
Beheer bestelling
@endcomponent
@else
@component('mail::button', ['url' => env('APP_URL', 'https://food4u.local') ])
Food4U
@endcomponent
@endif

Dank,<br>
{{ config('app.name') }}
@endcomponent
