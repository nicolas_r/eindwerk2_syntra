@component('mail::message')
# Beste {{ $formdata->name }}

Dank voor uw registratie.  Je account is nu geactiveerd.
Vanaf nu kan je lekkere gerechtjes bestellen, aan democratische prijzen!

@component('mail::button', ['url' => env('APP_URL', 'https://food4u.local')])
Bestel nu gerust je eerste maaltijd!
@endcomponent

Indien u zich wenst aan te melden als restauranthouder of leverancier kunt u met ons contact opnemen via het volgende web-formulier:

@component('mail::button', ['url' => env('APP_URL', 'https://food4u.local') . '/contact'])
    Contact
@endcomponent

Bedankt voor je vertrouwen en eet smakelijk!<br>
{{ config('app.name') }}
@endcomponent
