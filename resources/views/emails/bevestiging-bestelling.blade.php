@component('mail::message')
# Beste {{ $userdata->name }}

De volgende bestelling bij restaurant "{{ $restaurantnaam }}" is goed ontvangen en wordt nu bereid:

{{--Uw bestelling werd geplaatst op: {{ $formdata->bestellingtijdstip }}--}}
@component('mail::table')

{{--{{ dd($invoicedata) }}--}}

| Gerechtnaam                          | Eenheidsprijs                 | Aantal besteld | Subtotaal      | BTW |
|:-------------------------------------|:-----------------------------:|:--------------:|:--------------:|:--------------:
@foreach($invoicedata['order_items'] as $item)
| {{ $item['gerechtnaam'] }}                 | {{ $item['eenheidsprijs'] }} €       | {{ $item['aantal'] }} | {{ $item['subtotaal'] }} € | {{$item['subtotaal_btw']}} € |
@endforeach
@endcomponent

@component('mail::table')
|           |    |
|:----------|:---|
|BTW: | {{ $invoicedata['totaal']['btw'] }} € |
|Subtotaal: |{{ $invoicedata['totaal']['excl_btw'] }} € |
|<b>Totaal te betalen bij levering:</b> | <b>{{ $invoicedata['totaal']['algemeen'] }} €</b> |
@endcomponent

<b>Leveringsadres:</b><br><br>
&emsp;{{ $adresData->straat }} {{ $adresData->nr }} {{ $adresData->extension }}<br>
&emsp;{{ $adresData->postcode }} {{ $adresData->stad }}

Om de verdere vooruitgang van de bestelling on-line te kunnen volgen, kan je dit doen via onderstaande link

@component('mail::button', ['url' => env('APP_URL', 'https://food4u.local') . '/followup/' . $formdata->id . '/' . $formdata->followup_url_hash ])
Opvolging bestelling
@endcomponent

Bedankt voor het vertrouwen in Food4U,<br>
{{ config('app.name') }}
@endcomponent
