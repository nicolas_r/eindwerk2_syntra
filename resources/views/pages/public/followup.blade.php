@extends('layouts.app')

@section('css')
    <style>
        #payment_order {
            width: 50%;
        }

        #totals {
            font-weight: bold;
        }

    </style>


@endsection

@section('content')


    {{--Follwup HTML--}}

    {{--Hash gebruiken voor api call (samen met id) -> bij opvraging status via API--}}
    {{--{{ $bestelbondata->followup_url_hash }} {{ $bestelbondata->id }}--}}

    {{--Content order erin steken--}}

    {{--STatus ook weergeven...--}}

    <div class="container">

        <h2>Dank voor uw bestelling met - order nummer: {{$bestelbondata['order_nr']}}</h2>

        <div id="items_order" class="table-responsive">
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th>
                            Gerechtnaam
                        </th>
                        <th>
                            Eenheidsprijs
                        </th>
                        <th>
                            Aantal besteld
                        </th>
                        <th>
                            Subtotaal
                        </th>
                        <th>
                            BTW
                        </th>
                    </tr>
                </thead>
                <tbody>
                @foreach($bestelbondata['order_items'] as $item)
                    <tr>
                        <td>
                            {{ $item['gerechtnaam'] }}
                            {{--{{ $gerecht->naam }}--}}
                        </td>
                        <td>
                            {{ $item['eenheidsprijs'] }} €
                        </td>
                        <td>
                            {{ $item['aantal'] }} stuks
                        </td>
                        <td>
                            {{ $item['subtotaal'] }} €
                        </td>
                        <td>
                            {{ $item['subtotaal_btw'] }} € ({{ $item['btw_basis'] }} %)
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>

        <div id="payment_order" class="table-responsive">
            <table class="table">
                <tbody>
                    <tr>
                        <th>
                            BTW:
                        </th>
                        <td>
                            {{$bestelbondata['totaal']['btw']}} €
                        </td>
                    </tr>
                    <tr>
                        <th>
                            Subtotaal
                        </th>
                        <td>
                            {{$bestelbondata['totaal']['excl_btw']}} €
                        </td>
                    </tr>
                    <tr id="totals">
                        <th>
                            Totaal te betalen bij levering:
                        </th>
                        <td>
                            {{$bestelbondata['totaal']['algemeen']}} €
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>

        <div>
            <h2>Vooruitgang van uw bestelling:</h2>
        </div>

        <div class="row">
            <div class="col-md-6">
                <h3>Status bestelling: <span id="status">{{ $status }}</span></h3>
            </div>
            <div class="col-md-6">
                <h3>Geschatte leveringstijd: <span id="leveringstijd">Onbekend</span></h3>
            </div>
        </div>
        <div class="progress">
            <div class="progress-bar" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width:10%">
                0%
            </div>
        </div>

        <br>
    </div>


    @endsection

@section('js')
<script>
    // API call :
    // door te sturen naar server (post):
    // URL hash (daar record mee opzoeken met API om terug te sturen als "response" samen met de bestellingstatus)

    var statusMonitor = (function(){
        var _order = {
            id: null,
            urlHash: null
        };

        var init = function(){
            _getUrlHash();
            var check = _getServerData();
            if(check){
                _startMonitoring();
            } else {
                alert("Kan server niet bereiken.  Probeer de pagina te herladen!");
            }
        };

        var _getUrlHash = function(){
            var url = window.location.href;
            var urlArray = url.split("/");
            _order.id = urlArray[4];
            _order.urlHash = urlArray[5];
        };

        var _getServerData = function(){
            axios.post('/api/checkstatus', _order).then(function(response){

                var serverData = {
                    bestelbon_id: null,
                    hash: null,
                    status: 'onbekend',
                    percent_ready: 0
                };

                // Bij error, wordt default data gebruikt
                if(response.data.status !== "error") {
                    serverData = response.data.result;
                }

                console.log(serverData);

                _updateForm(serverData);

            }).catch(function(response){
                console.log("something went wrong");
                console.log(response);
                //alert("Something went wrong on the server!");
            });

            return true;
        };

        var _startMonitoring = function(){
            setInterval(_getServerData, 5000);
        };

        var _updateForm = function(serverData){
            // status message
            var statusText = document.querySelector('#status');
            statusText.innerHTML = serverData.status;

            // progress bar
            var progress = document.querySelector('.progress-bar');
            progress.style.width = serverData.percent_ready + '%';
            progress.innerHTML = serverData.percent_ready + '%';

            // levering tijdstip
            var tijdstip = document.querySelector('#leveringstijd');
            tijdstip.innerHTML = serverData.leveringstijd;
        };

        return {
            init: init
        }
    })();

    statusMonitor.init();

</script>
@endsection

