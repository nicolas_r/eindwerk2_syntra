@extends('layouts.app')

@section('css')
    <style>

        .navbar {
            position: relative;
            min-height: 250px;
            margin-bottom: 22px;
            border: 1px solid transparent;
            background-image: url(/assets/images/pizza.jpg);
        }

        .dropdown-toggle, .navbar-right, .navbar-brand, .navbar-toggle {
            background-color: rgba(0,0,0,.75);

        }

        .navbar-default .navbar-nav > li > a {
            color: #fff;
        }

        .navbar-default .navbar-brand {
            color: #fff;
        }

        .restolijst {
            list-style-type: none;
            border-style: outset;
            margin-bottom: 5px;
            margin-left: 0px;
            margin-right: 0px;
            //height: 170px;

        }

        .restologo {
            /*vertical-align: middle;*/
            height: 230px;
            padding-top: 10px;
            padding-bottom: 10px;
        }

        .restofigure {
            /*float: left;*/
            margin-left:auto;
            margin-right:auto;
        }

        ul {
            padding-left: 0px;
        }

        .test {
            text-align: center;
        }

        figure
        {
            display:table;
            //border: 1px solid red;
        }

        .col-md-3 {
            /*background-color: #2aabd2;*/
        }

        .col-md-9 {
            /*background-color: #2ab27b;*/
        }

        .test1 {
            height: 230px;

        }

        /*.test1 {*/
            /*background-color: #2aabd2;*/
        /*}*/

        /*.test2 {*/
            /*background-color: #2ca02c;*/

        /*}*/

        div.parent {
            display: inline-block;
            width: 100%;
        }


    </style>
    {{--<link href="/public/css/vendor/bootstrap/css/bootstrap.css" rel="stylesheet">--}}
@endsection

@section('content')

{{--<div class=""--}}
<div class="container">
    {{--<div class="row">--}}
        {{--<div id="test1" class="test1 col-md-3"></div>--}}
        {{--<div id="test2" class="test2 col-md-9"></div>--}}
    {{--</div>--}}
    <h1>Overzicht restaurants:</h1>
    <div class="restaurantlijst">
        <ul>
            @foreach($restaurants as $restaurant)
                <li class="restolijst row">
                    <a href="/{{ $restaurant->id }}/menu">
                        <div class="test1 col-md-3">
                            <figure class="restofigure">
                                <img class="restologo" src="{{ $restaurant->logo }}">
                            </figure>
                        </div>
                        <div class="test2 col-md-9">
                            <h2 class="test">{{ $restaurant->naam_restaurant }}</h2>
                            <h4 class="test">{{ $restaurant->omschrijving }}</h4>
                            <div class="test">Momenteel: {{ $restaurant->status }}</div>
                        </div>
                    </a>
                </li>
            @endforeach
        </ul>
    </div>
</div>

@endsection

@section('js')

@endsection