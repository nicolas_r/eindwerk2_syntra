@extends('layouts.app')

@section('css')
    <style>
        #naam1 {
            display: none;
        }
    </style>
@endsection

@section('content')
<div class="container">

    @include('partials.errors')

    <a href="/"><< Overzicht restaurants</a>
    <h2>Contactformulier</h2>

    <form action="/contact/submit" method="post">
        {{csrf_field()}}
        <input type="hidden" name="naam1" id="naam1">
        <input type="hidden" name="email" value="{{Auth::user()->email}}">

        <div class="form-group">
            <label for="sel1">Keuze vraag:</label>
            <select class="form-control" id="sel1" name="question_type">
                <option value="">...</option>
                @foreach($keuze_vragen as $key => $vraag)
                    <option value="{{$key}}" {{ old('question_type') == $key ? 'selected' : '' }}>{{$vraag}}</option>
                @endforeach
            </select>
        </div>

        <div class="form-group" id="bestelbon_field">
            <label for="sel2">Selecteer bestelling:</label>
            <select class="form-control" id="sel2" name="bestelbon_nr">
                <option value="">...</option>
                @foreach($bestellingen as $bestelling)
                    <option value="{{$bestelling->id}}" {{ old('bestelbon_nr') == $bestelling->id ? 'selected' : '' }}>Bon nr {{$bestelling->id}} - {{$bestelling['restaurant']->naam_restaurant}}</option>
                @endforeach
            </select>
        </div>

        <div class="form-group">
            <label for="comment">Uw vraag/boodschap:</label>
            <textarea class="form-control" rows="5" id="comment" name="message">{{old('message')}}</textarea>
        </div>
        {{--Je kan een verborgen input veld maken met het geregistreede e-mail adres als verzender?--}}
        <input class="btn btn-primary" type="submit">

    </form>

    {{--met dropdown in verschillende categorieën:--}}
     {{--* Ik heb een vraag met betrekking tot een bestelling (bestelbon nummer meegeven) -> doormailen naar juiste restaurant...--}}
     {{--* Ik wil restaurant houder worden (gewoon tekst formulier)--}}
     {{--* Ik wil leverancier worden (gewoon tekst formulier)--}}

</div>
@endsection

@section('js')
    <script>
        var bestelbon = document.querySelector('#bestelbon_field');
        bestelbon.style.display = 'none';

        var questionType = document.querySelector('#sel1');

        if(questionType.value == 2 || questionType.value == 3 )
        {
            bestelbon.style.display = 'block';
        } else {
            bestelbon.style.display = 'none';
        }

        questionType.addEventListener("change", function(){
            console.log(questionType.value);

            if(questionType.value == 2 || questionType.value == 3 )
            {
                bestelbon.style.display = 'block';
            } else {
                bestelbon.style.display = 'none';
            }
        });

    </script>
@endsection

