@extends('layouts.app')

@section('css')
    <style>
        body {
            background-color: #
        }

        .navbar {
            position: relative;
            min-height: 250px;
            margin-bottom: 22px;
            border: 1px solid transparent;
            background-image: url(/assets/images/pizza.jpg);
        }

        .dropdown-toggle, .navbar-right, .navbar-brand, .navbar-toggle {
            background-color: rgba(0,0,0,.75);

        }

        .navbar-default .navbar-nav > li > a {
            color: #fff;
        }

        .navbar-default .navbar-brand {
            color: #fff;
        }

        .restolijst {
            list-style-type: none;
            border-style: outset;
            margin-bottom: 5px;
            height: 170px;

        }

        .restologo {
            vertical-align: middle;
            height: 150px;
        }

        .restofigure, .restocontent {
            float: left;
        }

        #checkout_tab, #manualAddress_div {
            display: none;
        }

        #app {
            padding-bottom: 50px;
        }

    </style>


    {{--<link href="/public/css/vendor/bootstrap/css/bootstrap.css" rel="stylesheet">--}}
@endsection

@section('content')

    {{--<div class=""--}}
    <div class="container">
        <a href="/winkelwagen/back"><< Terug naar overzicht gerechten</a>
        <h1>Winkelwagen:</h1>

        @include('partials.errors');

        <div class="table-responsive">
            <table class="table">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Naam Gerecht</th>
                        <th>Prijs</th>
                        <th>Aantal</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                @foreach(Cart::content() as $content)
                    <tr>
                        <td>{{ $content->id }}</td>
                        <td>{{ $content->name }}</td>
                        <td>{{ $content->price }} €</td>
                        <td>
                            <form method="post" action="/winkelwagen/update">
                                {{ csrf_field() }}

                                <input type="number" pattern="\d+" name="aantal" step="1" min="1" max="100" value="{{ $content->qty }}">
                                <input type="hidden" name="rowId" value="{{$content->rowId}}">
                                <input type="submit" class="btn" value="update">

                            </form>

                        </td>
                        <td>
                            <a class="btn btn-danger" href="/winkelwagen/remove/{{$content->rowId}}">
                                Verwijderen
                            </a>
                        </td>
                    </tr>
                @endforeach

                </tbody>
            </table>
        </div>

        <h2>Subtotaal te betalen: {{ Cart::subtotal() }} €</h2>
        <h2>Totaal te betalen: {{ Cart::total() }} €</h2>
        @if(Cart::count())
            <button class="btn" id="checkout_btn">Checkout</button>
        @else
            <button class="btn" id="checkout_btn" disabled>Checkout</button>
        @endif
        <a href="/winkelwagen/back" class="btn btn-default">Terug</a>
        @if(Cart::count() == 0)
            <input class="btn pull-right" type="submit" value="Winkelwagen leegmaken" form="winkelwagen-reset" disabled>
        @else
            <input class="btn pull-right" type="submit" value="Winkelwagen leegmaken" form="winkelwagen-reset">
        @endif

        <form id="winkelwagen-reset" method="post" action="/winkelwagen/reset">
            {{ csrf_field() }}
        </form>
        <div id="checkout_tab">
            <hr>

            <h2>Leveringsadres</h2>
            <div class="row">
                <div class="col-md-6">

                    @isset($leveringsadressen)
                        @foreach($leveringsadressen as $leveringsadres)
                            <div class="radio">
                                <label>
                                    <input type="radio" class="address_existing" name="address_option" form="bestel_formulier" value="{{$leveringsadres->id}}">
                                    {{ $leveringsadres->straat }} {{$leveringsadres->nr}} {{$leveringsadres->extension}}<br>{{$leveringsadres->postcode}} {{$leveringsadres->stad}}
                                </label>
                            </div>
                        @endforeach
                    @endisset

                    <div class="radio">
                        <label>
                            <input type="radio" name="other_address" form="bestel_formulier" id="other_address" value="true">
                            Ander leveringsadres (aan te vullen)
                        </label>
                    </div>
                </div>

                <div class="col-md-6">
                    <div id="manualAddress_div">
                        <div class="form-group">
                            <label class="col-md-2" for="adres">Straat:</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" id="adres" name="straat" value="{{ $defaultLeveringsAdres['straat'] }}" form="bestel_formulier">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-2" for="huisnr">Nr:</label>
                            <div class="col-md-4">
                                <input type="text" class="form-control" id="huisnr" name="nr" value="{{ $defaultLeveringsAdres['nr'] }}" form="bestel_formulier">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-1" for="extension">Bus:</label>
                            <div class="col-md-4">
                                <input type="text" class="form-control" id="extension" name="extension" value="{{ $defaultLeveringsAdres['extension'] }}" form="bestel_formulier">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-2" for="postcode">Postcode:</label>
                            <div class="col-md-2">
                                <input type="text" class="form-control" id="postcode" name="postcode" value="{{ $defaultLeveringsAdres['postcode'] }}" form="bestel_formulier">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-2" for="gemeente">Stad:</label>
                            <div class="col-md-5">
                                <input type="text" class="form-control" id="gemeente" name="stad" value="{{ $defaultLeveringsAdres['stad'] }}" form="bestel_formulier">
                            </div>
                        </div>
                        <div class="form-group">
                            {{--<label class="col-md-2" for="gemeente">Land:</label>--}}
                            {{--<div class="col-md-5">--}}
                                <input type="hidden" class="form-control" id="land" name="land" value="Belgie" form="bestel_formulier">
                            {{--</div>--}}
                        </div>
                        {{--We moeten ervan uitgaan: we bewaren ALTIJD het (eerder gebruikte) leveringsadres...--}}
                        {{--<div class="form-group">--}}
                            {{--<label class="checkbox-inline col-md-10 col-md-offset-2">--}}
                                {{--<input type="checkbox" id="inlineCheckbox1" name="adresbewaren" value="true" form="bestel_formulier"> Adres bewaren?--}}
                            {{--</label>--}}
                        {{--</div>--}}
                    </div>
                </div>
            </div>
            <form action="/winkelwagen/bestellen" method="post" id="bestel_formulier">
                {{ csrf_field() }}
                @if(Cart::count() == 0)
                    <input type="submit" class="btn" value="Bestelling Versturen!" disabled>
                @else
                    <input type="submit" class="btn" value="Bestelling Versturen!">
                @endif
            </form>
        </div>
    </div>

@endsection

@section('js')
<script>
    var checkoutBtn = document.querySelector('#checkout_btn');
    checkoutBtn.addEventListener('click', checkout);

    var other_address = document.querySelector('#other_address');
    other_address.addEventListener('change', manualAddress);

    // todo : check of selectors bestaan (die moeten allemaal worden afgelopen en uitgevinkt)

    var element =  document.getElementById('elementId');
    if (typeof(element) != 'undefined' && element != null)
    {
        // exists.
    }

    var address_existing = document.querySelectorAll('.address_existing');
    //console.log(address_existing);

    if (typeof(address_existing) != 'undefined' && address_existing != null)
    {
        // exists.
        for (i = 0; i < address_existing.length; i++) {
            console.log(address_existing);
            address_existing[i].addEventListener('change', existingAddress);
        }
    }

    function checkout() {
        var checkoutDiv = document.querySelector('#checkout_tab');
        checkoutDiv.style.display = "block";
    }

    function manualAddress() {
        var manualAddress = document.querySelector('#manualAddress_div');

        if(other_address.checked){
            // check of selectors bestaan (die moeten allemaal worden afgelopen en uitgevinkt)
            if (typeof(address_existing) != 'undefined' && address_existing != null)
            {
                // exists.
                for (i = 0; i < address_existing.length; i++) {
                    address_existing[i].checked = null;
                }
            }
            manualAddress.style.display = "block";
        }
    }

    function existingAddress() {
        other_address.checked = false;
        var manualAddress = document.querySelector('#manualAddress_div');
        manualAddress.style.display = "none";
    }

    console.log(address_existing === null);

    if (address_existing.length === 0){
        var manualAddress2 = document.querySelector('#manualAddress_div');
        manualAddress2.style.display = "block";
        other_address.checked = true;
    }

</script>

@endsection