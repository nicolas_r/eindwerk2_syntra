@extends('layouts.app')

@section('css')
    <style>

        .restolijst {
            list-style-type: none;
            border-style: outset;
            margin-bottom: 5px;
            margin-left: 0px;
            margin-right: 0px;
        //height: 170px;

        }

        .restologo {
            /*vertical-align: middle;*/
            height: 230px;
            padding-top: 10px;
            padding-bottom: 10px;
        }

        .restofigure {
            /*float: left;*/
            margin-left:auto;
            margin-right:auto;
            padding-right: 10px;
        }

        ul {
            padding-left: 0px;
        }

        .restofigure, .restocontent {
            float: left;
        }

        #gerecht_id, #vliegenvanger {
            display: none;
        }

        figure
        {
            display:table;
        //border: 1px solid red;
        }

    </style>
    {{--<link href="/public/css/vendor/bootstrap/css/bootstrap.css" rel="stylesheet">--}}
@endsection

@section('content')

{{--@include('partials.errors')--}}
{{--{{ dump($errors->count()) }}--}}

<div class="container">
    <a href="/"><< Overzicht restaurants</a>
    <h1>Kies Gerecht:</h1>
    {{--{{ $restaurant->naam_restaurant }}--}}

    @include('partials.errors')

    <div class="gerechtenlijst">
        <ul>
            @foreach($menus as $menu)
                <li class="restolijst row">
                    <div class="col-md-3">
                        <figure class="restofigure">
                            <img class="restologo" src="{{ $menu['foto'] }}">
                        </figure>
                    </div>
                    <div class="col-md-9">
                        <h4>{{ $menu['naam'] }}</h4>
                        <p>{{ $menu['omschrijving'] }}</p>
                        <p>Prijs: {{ $menu['prijs'] }} €<br>
                            Bereidingstijd: {{ $menu['bereidingstijd'] }} minuten</p>
                        <form method="post" action="menu/toevoegen" autocomplete="off">
                            {{ csrf_field() }}
                            <input type="input" id="vliegenvanger" name="vliegenvanger">
                            <input type="input" id="gerecht_id" name="gerecht_id" value="{{ $menu['id'] }}">
                            <label>Aantal: <input type="number" pattern="\d+" name="aantal" step="1" min="1" max="100" value="1"></label>

                            @if(($errors->count() > 0) && ($errors->get($menu['id'])))
                                Fout!
                            @endif

                            <input type="submit" value="Toevoegen aan bestelling">
                        </form>
                    </div>
                </li>
            @endforeach
        </ul>
    </div>
</div>

@endsection

@section('js')

@endsection
