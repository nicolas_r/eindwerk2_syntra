@extends('layouts.app')

@section('css')
    {{--<link href="{{ asset('css/bootstrap-toggle.css') }}" rel="stylesheet">--}}
    <style>
        input[type="checkbox"]:checked + span {
            text-decoration: line-through;
        }
    </style>

@endsection

@section('content')

    <div class="container">

        <div class="row">
            <div class="col-md-12">
                @include('partials.errors', ['errors' => $errors])     {{--Aparte partial voor validatie--}}
            </div>
        </div>

        @if(Auth::user()->hasRole('admin'))
        <form method="post" action="/admin/user/update/{{ $userData['id'] }}">
        @else
        <form method="post" action="/user/profile/{{ $userData['id'] }}">
        @endif

            {{ csrf_field() }}
            <div class="form-group">
                <label class="col-md-3"for="usr">Login:</label>
                <div class="col-md-9">
                    <input type="text" class="form-control" id="usr" name="name" value="{{ old('name', $userData['name']) }}" readonly>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-3" for="email">E-mail:</label>
                <div class="col-md-9">
                    <input type="text" class="form-control" id="email" name="email" value="{{ old('email', $userData['email']) }}" readonly>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-3" for="firstname">First name:</label>
                <div class="col-md-9">
                    <input type="text" class="form-control" id="firstname" name="firstname" value="{{ old('firstname', $userData['firstname']) }}">
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-3" for="lastname">Last name:</label>
                <div class="col-md-9">
                    <input type="text" class="form-control" id="lastname" name="lastname" value="{{ old('lastname', $userData['lastname']) }}">
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-3" for="tel">Telephone:</label>
                <div class="col-md-9">
                    <input type="text" class="form-control" id="tel" name="tel" value="{{ old('tel', $userData['tel']) }}">
                </div>
            </div>

            <div class="form-group">
                <label class="col-md-3" for="tel">Primair Adres</label>
                <div class="col-md-9">
                    <input type="text" class="form-control" id="adres" name="straat" value="{{ old('straat', $userData['straat']) }}">
                </div>
            </div>

            <div class="form-group">
                <label class="col-md-3" for="tel">Nr</label>
                <div class="col-md-9">
                    <input type="text" class="form-control" id="nr" name="nr" value="{{ old('nr', $userData['nr']) }}">
                </div>
            </div>

            <div class="form-group">
                <label class="col-md-3" for="tel">Extensie</label>
                <div class="col-md-9">
                    <input type="text" class="form-control" id="extension" name="extension" value="{{ old('extension', $userData['extension']) }}">
                </div>
            </div>

            <div class="form-group">
                <label class="col-md-3" for="tel">Postcode</label>
                <div class="col-md-9">
                    <input type="text" class="form-control" id="postcode" name="postcode" value="{{ old('postcode', $userData['postcode']) }}">
                </div>
            </div>

            <div class="form-group">
                <label class="col-md-3" for="tel">Gemeente</label>
                <div class="col-md-9">
                    <input type="text" class="form-control" id="gemeente" name="gemeente" value="{{ old('gemeente', $userData['gemeente']) }}">
                </div>
            </div>

            {{--Gebruik User via middleware om gebruiker te blocken--}}
            {{--<div class="form-group">--}}
                {{--<label class="col-md-3" for="active">Active:</label>--}}
                {{--<div class="col-md-9">--}}
                    {{--<input type="checkbox" checked data-toggle="toggle">--}}
                    {{--<input type="text" class="form-control" id="active" name="active" value="{{$userData['active']}}">--}}
                {{--</div>--}}
            {{--</div>--}}

            {{--Only show checkboxes if you are admin--}}
            @if(Auth::user()->hasRole('admin'))
            <div class="form-group">
                <label class="col-md-3" for="activeRoles">Current Roles:</label>
                <div class="col-md-2">
                    <div class="checkbox">
                        <label for="role_user">
                            <input id="role_user" value="1" name="user" type="checkbox"
                            @if(old('user', $userData->roles->where('name', 'user')->first()))
                                checked
                            @endif
                            {{--Iedereen heeft standaard 'user' privileges, kan niet worden veranderd... POST daarintegen moet daarintegen nog steeds user = 1 bevatten...--}}
                            @if($userData->id == Auth::user()->id)
                                checked disabled><input type="hidden" name="user" value="1"
                            @endif
                            >
                            user (active)
                        </label>
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="checkbox">
                        <label for="role_restaurant">
                            <input id="role_restaurant" value="1" name="restaurant" type="checkbox"
                            @if(old('restaurant', $userData->roles->where('name', 'restaurant')->first()))
                                 checked
                            @endif
                            >
                            restaurant
                        </label>
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="checkbox">
                        <label for="role_leverancier">
                            <input id="role_leverancier" value="1" name="leverancier" type="checkbox"
                            @if(old('leverancier', $userData->roles->where('name', 'leverancier')->first()))
                                checked
                            @endif
                            >
                            leverancier
                        </label>
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="checkbox">
                        <label for="role_admin">
                            <input id="role_admin" value="1" type="checkbox" name="admin"
                            @if(old('admin', $userData->roles->where('name', 'admin')->first()))
                                checked
                            @endif
                            {{--Indien admin ingelogd, kan hij zijn eigen admin account niet disablen. POST daarintegen moet daarintegen admin = 1 bevatten...--}}
                            @if($userData->id == Auth::user()->id)
                                checked disabled><input type="hidden" name="admin" value="1"
                            @endif
                            >
                            administrator
                        </label>
                    </div>
                </div>

                <div class="col-md-1"></div>
            </div>
            @endif

            <div class="form-group">
                <div class="col-md-3"></div>
                <div class="col-md-9">
                    <button type="submit" class="btn btn-default">Update Profiel</button>
                    <button type="button" class="btn btn btn-default" data-toggle="modal" data-target="#levAdressenModal">
                        Leveringsadressen Beheren
                    </button>
                    @if(Auth::user()->hasRole('admin'))
                        <a href={{ URL::previous() }} class="btn btn-default">Back</a>
                        {{--<a href="/admin/users" class="btn btn-default">Annuleer</a>--}}
                    @else
                        <a href="/" class="btn btn-default">Annuleer</a>
                    @endif

                </div>
            </div>
        </form>

    </div>

    <!-- Modal -->
    <div class="modal fade" id="levAdressenModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Leveringsadressen overzicht</h4>
                </div>
                {{--<form action="/user/profile/{{$userData['id']}}/deleteaddress" method="post">--}}
                <form>
                    {{csrf_field()}}
                    <div class="modal-body">
                        @if($userData['leveringsadressen']->isEmpty())
                            <p>Geen leveringsadressen gevonden!</p>
                        @else

                        <fieldset>
                            <legend>Selecteer de adressen die je wenst te verwijderen:</legend>
                        @foreach($userData['leveringsadressen'] as $leveringsAdres)
                            <label>
                                <input type="checkbox" id="{{$leveringsAdres['id']}}" class="levAdressen" value="1" name="{{$leveringsAdres['id']}}">
                                <span>
                                {{--<a data-id="{{$leveringsAdres['id']}}" class="btn btn-danger btn-xs btn-delete" data-toggle="modal" data-target="#delete-form">--}}
                                    {{--<span class="glyphicon glyphicon-trash"></span>--}}
                                {{--</a>--}}
                                {{$leveringsAdres['straat']}} {{$leveringsAdres['nr']}} {{$leveringsAdres['extension']}} -
                                {{$leveringsAdres['postcode']}} {{$leveringsAdres['stad']}}</span></label><br>

                        @endforeach
                        </fieldset>
                        @endif
                    </div>
                    <div class="modal-footer">
                        {{--<input type="submit" value="Save" id="submitBtn" class="btn btn-default" ></input>--}}
                        <a value="Save" id="submitBtn" class="btn btn-default" data-dismiss="modal">Delete & close</a>
                        <a value="Cancel" id="cancelBtn" type="button" class="btn btn-default" data-dismiss="modal">Cancel</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('js')
    {{--<script src="{{ asset('/js/bootstrap-toggle.js') }}"></script>--}}

<script>

// API calls om leveringsadressen in modal weer te kunnen geven.  Probeersel, gewoon omdat het kan...
var checkApp = (function(){
    var _apiUrl =  '/user/profile/' + {{$userData['id']}} + '/deleteaddress';

    var _init = function(){
        //_levAdressen = document.querySelectorAll('.levAdressen');
        var _submitBtn = document.querySelector("#submitBtn");
        _submitBtn.addEventListener("click", _deleteSelected);
        var _cancelBtn = document.querySelector("#cancelBtn");
        _cancelBtn.addEventListener("click", _undoSelectedAll);
    };

    //-- Delete one element
    var _deleteAdres = function(id) {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
            url: _apiUrl + "/" + id,
            type: "DELETE"
        }).done(function(data){
            console.log(data);

        }).fail(function(data){
            console.log(data.responseText);

        })
    };

    var _undoSelectedAll = function(){
        var _levAdressen = document.querySelectorAll('.levAdressen');
        for(var i = 0; i < _levAdressen.length; i++){
            if(!_levAdressen[i].disabled){
                _levAdressen[i].checked = false;
            }
        }
    };

    //-- Deletes selected checkboxes
    var _deleteSelected = function(){
        var _levAdressen = document.querySelectorAll('.levAdressen');
        for(var i = 0; i < _levAdressen.length; i++) {
            //console.log(_levAdressen[i].getAttribute('id'));
            //console.log(_levAdressen[i].checked);
            if(_levAdressen[i].checked && !_levAdressen[i].disabled){
                _executeDelete(_levAdressen[i].getAttribute('id'), _levAdressen, i);
            }
        }
    };

    var _executeDelete = function(url, levAdressen, index){
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
            url: _apiUrl + "/" + levAdressen[index].getAttribute('id'),
            type: "DELETE"
        }).done(function(data){
            console.log(data);
            levAdressen[index].disabled = true;
        }).fail(function(data){
            console.log(data.responseText);
            alert("Niet verwijderd. Dit adres nl. wordt momenteel gebruikt!");
            levAdressen[index].disabled = false;
            levAdressen[index].checked = false;
        });
    };

    return {
        init: _init
    }

})();

checkApp.init();

</script>

@endsection
