@extends('layouts.app')

@section('css')

    <link rel="stylesheet" href="/css/bootstrap-toggle.css">
    <style>
        h2 {
            display: inline;
        }
    </style>

@endsection

@section('content')

    <div class="container">

        <h2>Restaurant Beheer</h2>
        <a href="/restaurant/create" class="btn btn-primary pull-right">Voeg nieuw Restaurant toe</a>

        <table class="table table-striped table-responsive">
            <thead>
            <tr>
                <th>ID</th>
                <th>Naam Restaurant</th>
                <th>Omschrijving</th>
                <th>E-mail</th>
                <th>Telefoon</th>
                <th>Actief</th>
                <th>Adresgegevens</th>
                <th></th>
            </tr>
            </thead>
            <tbody>
            @foreach($restaurants as $restaurant)
                <tr>
                    <td>{{ $restaurant->id }}</td>
                    <td>{{ $restaurant->naam_restaurant }}</td>
                    <td>{{ $restaurant->omschrijving }}</td>
                    <td>{{ $restaurant->email_restaurant }}</td>
                    <td>{{ $restaurant->tel_restaurant }}</td>
                    <td> <input type="checkbox" class="toggletest" id="toggle{{ $restaurant->id }}" {{ $restaurant->actief ? 'checked' : '' }} data-on="online" data-off="offline" data-toggle="toggle" data-onstyle="success"></td>
                    <td>{{ $restaurant->straat }} {{ $restaurant->nr }} {{ $restaurant->postcode }} {{ $restaurant->gemeente }}</td>
                    <td>
                        <a id="resto-detail-{{ $restaurant->id }}" class="btn btn-warning btn-xs btn-detail" href="/restaurant/{{ $restaurant->id }}/beheer">
                            <span class="glyphicon glyphicon-zoom-in"></span>
                        </a>
                    </td>

                </tr>
            @endforeach
            </tbody>
        </table>

        {{ $restaurants->links() }}

        {{--<div class="row">--}}
            {{--<ul>--}}
                {{--<li>--}}
                    {{--<div class="col-sm-6 col-md-4">--}}
                        {{--<div class="thumbnail">--}}
                            {{--<img src="..." alt="...">--}}
                            {{--<div class="caption">--}}
                                {{--<h3>Restaurant 1 Naam</h3>--}}
                                {{--<p>...</p>--}}
                                {{--<p><a href="#" class="btn btn-primary" role="button">Beheer</a> <a href="#" class="btn btn-default" role="button">Verwijder</a></p>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</li>--}}
                {{--<li>--}}
                    {{--<div class="col-sm-6 col-md-4">--}}
                        {{--<div class="thumbnail">--}}
                            {{--<img src="..." alt="...">--}}
                            {{--<div class="caption">--}}
                                {{--<h3>Restaurant 2 Naam</h3>--}}
                                {{--<p>...</p>--}}
                                {{--<p><a href="#" class="btn btn-primary" role="button">Button</a> <a href="#" class="btn btn-default" role="button">Button</a></p>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</li>--}}
                {{--<li>--}}
                    {{--<div class="col-sm-6 col-md-4">--}}
                        {{--<div class="thumbnail">--}}
                            {{--<img src="..." alt="...">--}}
                            {{--<div class="caption">--}}
                                {{--<h3>Restaurant 3 Naam</h3>--}}
                                {{--<p>...</p>--}}
                                {{--<p><a href="#" class="btn btn-primary" role="button">Button</a> <a href="#" class="btn btn-default" role="button">Button</a></p>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</li>--}}
                {{--<li>--}}
                    {{--<div class="col-sm-6 col-md-4">--}}
                        {{--<div class="thumbnail">--}}
                            {{--<div class="caption">--}}
                                {{--<h3 id=newRestaurant class="glyphicon glyphicon-plus"></h3>--}}
                                {{--<p>...</p>--}}
                                {{--<p><a href="#" class="btn btn-primary" role="button"></a> <a href="#" class="btn btn-default" role="button"></a></p>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</li>--}}
            {{--</ul>--}}
        {{--</div>--}}

        {{--<span class="glyphicon glyphicon-search" aria-hidden="true"></span>--}}

        {{--DIV's for test functions--}}
        {{--<input id="toggle-event" type="checkbox" data-toggle="toggle" data-on="Enabled" data-off="Disabled" data-size="mini">--}}
        {{--<div id="console-event">BLABLA</div>--}}
        {{--<button class="btn btn-success" onclick="actiefToggle.toggleOn('toggle-event')">On by API</button>--}}
        {{--<button class="btn btn-danger" onclick="actiefToggle.toggleOff('toggle-event')">Off by API</button>--}}
        {{--<button class="btn btn-success" onclick="actiefToggle.toggleOnByInput('toggle-event')">On by Input</button>--}}
        {{--<button class="btn btn-danger" onclick="actiefToggle.toggleOffByInput('toggle-event')">Off by Input</button>--}}

    </div>

@endsection

@section('js')

    <script>

        // jQuery bootstrap-toggle swiches eens proberen aanroepen via 'revealing module pattern' voor de fun...
        // node-module enkel invoegen als require in bootstrap.js (NIET webpack.mix.js of elders!)
        // daarna compileren met npm run dev!
        var actiefToggle = (function(){

            const DEBUG = false;
            const APIDEBUG = false;
            DEBUG ? console.log("DEBUG ENABLED") : console.log("DEBUG DISABLED");
            DEBUG ? console.log("Initializing script!") : '';

            /**
             * Check the toggle-button and return the value (true or false)
             * @private
            */
            function _checkValue2(id){
                //console.log(id);
                var result = $('#' + id).bootstrapToggle();
                return result[0].checked;
            }

            function toggleOn(id) {
                $('#' + id).bootstrapToggle('on')
            }

            function toggleOff(id) {
                $('#' + id).bootstrapToggle('off')
            }

            function toggleOnByInput(id) {
                $('#' + id).prop('checked', true).change()
            }

            function toggleOffByInput(id) {
                $('#' + id).prop('checked', false).change()
            }

            /**
             * Closure op elke toggle-switch
             * @param toggleSwitch
             * @private
            */
            var _attachEventListener = function(toggleSwitch) {
                var id = toggleSwitch.getAttribute('id');
                DEBUG ? console.log("Setting closure for id: " + id) : '';
                var initialValue = _checkValue2(id);

                $('#' + id).change(function() {

                    DEBUG ? console.log(_checkValue2(id)) : '';

                    var restaurantId = id.replace('toggle','');
                    var data = {
                        actief: _checkValue2(id)  //-- true of false
                    };

                    axios.put('/api/restaurant/' + restaurantId, data).then(function(response){
                        APIDEBUG ? console.log(response) : '';
                        APIDEBUG ? console.log(response.data.status) : '';
                        APIDEBUG ? console.log(response.data.result.actief): '';

                        // Bij error, zet toggle-switch terug op 'vorige' stand...
                        if(response.data.status == "error") {
                            //- Indien fout, zet knop terug op vorige stand
                            DEBUG ? console.log("Fout! Zet knop terug! Terugzetten op vorige stand:") : '';

                            //-- Vermijd oneindige event-loop in teval van response-fout of server-fout
                            if(initialValue == _checkValue2(id))
                                return;
                            initialValue ? toggleOnByInput(id) : toggleOffByInput(id);
                        }

                    }).catch(function(response){

                        //-- Vermijd oneindige event-loop in teval van response-fout of server-fout
                        if(initialValue == _checkValue2(id))
                            return;
                        initialValue ? toggleOnByInput(id) : toggleOffByInput(id);
                        alert("Something went wrong on the server!");
                    })
                })
            };

            /**
             * Put event listeners on toggle-switches
             */
            var addEventListeners = function(){
                var toggleSwitches = document.querySelectorAll('.toggletest');
                for(var i = 0; i < toggleSwitches.length; i++) {
                    DEBUG ? console.log("Adding event listenener for: ") : '';
                    DEBUG ? console.log(toggleSwitches[i]) : '';
                    toggleSwitches[i].addEventListener('click', _attachEventListener(toggleSwitches[i]))
                }
            };

            return {
                //-- Public methods
                toggleOn: toggleOn,
                toggleOff: toggleOff,
                toggleOnByInput: toggleOnByInput,
                toggleOffByInput: toggleOffByInput,
                addEventListeners: addEventListeners
            }
        })();

        actiefToggle.addEventListeners();

    </script>

@endsection

