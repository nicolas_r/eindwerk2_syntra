{{--Oplijsten gerechten, gerechten toevoegen, gerechten verwijderen, elk gerecht van 1 of meerdere foto's voorzien...--}}
@extends('layouts.app')

@section('css')
    <link rel="stylesheet" href="/css/vendor/dropzone/dropzone.css">
    <link rel="stylesheet" href="/css/vendor/dropzone/basic.css">
@endsection

@section('content')


    @if(isset($fotoData))
        {{--{{ dump($fotoData) }}--}}
    @endif

    <div class="container">
        <form method="post" action="./fotos/save" id="selectFoto">
            {{ csrf_field() }}
            <input type="submit"class="btn btn-primary pull-right" value="Keuze opslaan en terugkeren">
        </form>

        <h1>Welkom bij "{{ $restaurantNaam }}" !</h1>

        Hier kan je een foto's voegen of een foto voor uw restaurant selecteren.

        @include('partials.errors')

        <div class="row">
            <form action="/restaurant/{{$restaurantId}}/fotos/upload" class="dropzone">

                <div class="form-group">
                    <label class="col-md-3"for="usr"></label>
                    <div class="col-md-9 fallback">
                        <input name="file" type="file" multiple />
                        <input name="restaurantId" type="hidden" value="{{$restaurantId}}">
                    </div>
                </div>

                <input name="restaurantId" type="hidden" value="{{$restaurantId}}">
                {{--CSRF kan ook via de header van dropzone worden meegegeven, als je niet via een form of hidden field de csrf meegeeft (xsrf token--}}
                {{ csrf_field() }}
            </form><br>
        </div>
        <div class="row">
        @isset($fotoData)
            @foreach($fotoData as $foto)
                <div class="col-md-4">
                    <div class="thumbnail">
                        <a href="#">

                            {{--<img src="{{ '/' . $fotoData[0]['path'] . '/' . $fotoData[0]['filename'] }}">--}}
                            {{ dump($foto['path']) }}
                            <img src="{{ $foto['path'] }}" alt="Test" style="width:100%">
                            {{--<img src="/images/test.jpg?w=200&amp;s=32b5b572d011b82f50bf31c4e2ad5d12" alt="Test" style="width:100%">--}}
                            <div class="caption">
                                {{--<p>TEST...</p>--}}
                                <div>
                                    <form action="./fotos/{{$foto['id']}}/delete" method="post">
                                        {{ csrf_field() }}
                                        <input type="submit" class="btn btn-danger pull-right" value="Delete">
                                    </form>
                                </div>
                                <label for="gerecht_img"><input form="selectFoto" type="radio" name="fotos" value="{{ $foto['id'] }}"> Stel in als hoofdafbeelding</label>
                            </div>
                        </a>
                    </div>
                </div>
            @endforeach
        @endisset
        </div>

    </div>

@endsection

@section('js')
    <script src="/js/vendor/dropzone.js"></script>
@endsection