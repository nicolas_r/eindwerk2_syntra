{{-- Backend portaalsite restaurant (toegangeklijk met restaurant rechten...)--}}
{{-- Created by PhpStorm.--}}
{{-- User: nicol--}}
{{-- Date: 24/04/2017--}}
{{-- Time: 22:57--}}

@extends('layouts.app')

@section('css')

    <link href="/css/font-awesome.css" rel="stylesheet" type="text/css" />

    <style>
        .btn-sq-lg {
            width: 150px !important;
            height: 150px !important;
        }

        .btn-sq {
            width: 100px !important;
            height: 100px !important;
            font-size: 10px;
        }

        .btn-sq-sm {
            width: 50px !important;
            height: 50px !important;
            font-size: 10px;
        }

        .btn-sq-xs {
            width: 25px !important;
            height: 25px !important;
            padding:2px;
        }

        ul
        {
            margin: 0;
            padding: 0;
            list-style-type: none;
            text-align: center;
        }

        ul li
        {
            display: inline;
        }

        .btn
        {
            margin-bottom: 4px;
        }

    </style>

@endsection

@section('content')

    <div class="container">

        <h2>Backend portaalsite restaurantbeheer</h2><br>

        <div class="row">
            <div class="col-lg-12">
                <ul>
                    <li>
                        <a href="/restaurant/{{ $restaurantId }}/edit" class="btn btn-sq-lg btn-danger">
                            <i class="fa fa-user fa-5x"></i><br/>
                            Details<br>Restaurant
                        </a>
                    </li>
                    <li>
                        <a href="/restaurant/{{ $restaurantId }}/fotos" class="btn btn-sq-lg btn-danger">
                            <i class="fa fa-picture-o user fa-5x"></i><br/>
                            Fotos<br>Restaurant
                        </a>
                    </li>
                    <li>
                        <a href="./openingsuren" class="btn btn-sq-lg btn-primary">
                            <i class="fa fa-calendar fa-5x"></i><br/>
                            Openingsuren<br>Restaurant
                        </a>
                    </li>
                    <li>
                        <a href="./gerechten" class="btn btn-sq-lg btn-success">
                            <i class="fa fa-cutlery fa-5x"></i><br/>
                            Gerechten<br>Retaurant
                        </a>
                    </li>
                    <li>
                        <a href="../bestellingen/{{$restaurantId}}" class="btn btn-sq-lg btn-info">
                            <i class="fa fa-book fa-5x"></i><br/>
                            Beheer Bestellingen<br>Restaurant
                        </a>
                    </li>
                    <li>
                        <a href="/restaurant/{{ $restaurantId }}/verpakkingen" class="btn btn-sq-lg btn-warning">
                            <i class="fa fa-archive fa-5x"></i><br/>
                            Beheer<br>Verpakkingen
                        </a>
                    </li>
                    <li>
                        <a href="/restaurant" class="btn btn-sq-lg btn-danger">
                            <i class="fa fa-table fa-5x"></i><br/>
                            Terug naar<br>Overzicht
                        </a>
                    </li>
                </ul>
            </div>
        </div>

    </div>

@endsection

@section('js')

@endsection