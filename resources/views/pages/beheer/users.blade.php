@extends('layouts.app')

@section('css')

@endsection

@section('content')
    <div class="container">
        Beheer Users + set privileges, enkel mogelijk als admin!

        @include('partials.data_updated');

        <table class="table table-striped table-responsive">
            <thead>
            <tr>
                <th>ID</th>
                <th>Naam</th>
                <th>E-mail</th>
                <th>Aanmaak datum</th>
                <th>Actief</th>
            </tr>

            </thead>
            <tbody>
            @foreach($users as $user)
                <tr>
                    <td>{{ $user->id }}</td>
                    <td>{{ $user->name }}</td>
                    <td>{{ $user->email }} </td>
                    <td>{{ $user->created_at }}</td>
                    <td>{{ $user->active }}</td>
                    <td>
                        <a class="btn btn-warning btn-xs btn-detail" href="/admin/user/profile/{{ $user->id }}">
                            <span class="glyphicon glyphicon-zoom-in"></span>
                        </a>

                        {{--Je eigen account waarmee je bent ingelogd, kan je niet verwijderen, dus géén delete icon--}}
                        @if($user->id !== Auth::user()->id)

                            <a data-id="{{ $user->id }}" class="btn btn-danger btn-xs btn-delete" data-toggle="modal" data-target="#delete-form">
                                <span class="glyphicon glyphicon-trash"></span>
                            </a>
                        @endif
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>

        {{ $users->links() }}

    </div>

    <! -- show modal -->
    <div class="modal fade" id="delete-form" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">x</span>
                    </button>
                    <h4 class="modal-title" id="myModalLabel">Bent u zeker dat u deze gebruiker wenst te verwijderen?</h4>
                </div>
                <form method="post" action="/admin/user/delete">
                    <input id="id" name="id" type="hidden" value="" />
                    {{ csrf_field() }}

{{--Contents ophalen via een AJAX-call? Zie niet goed in, hoe ik deze kan ophalen uit de bestaande $users array...--}}
                    <div class="modal-body">
                        <p id="ajax_id">Unknown ID</p>
                        <p id="ajax_name">Unknown Name</p>
                        <p id="ajax_email">Unknown E-mail</p>
                        <p id="ajax_created_at">Unknown date</p>
                    </div>

                    <div class="modal-footer">
                        <input id="delete-form-ok" class="btn btn-default" type="submit" value="Ja">
                        <button class="btn btn-default" data-dismiss="modal" aria-label="Close">Neen</button>
                    </div>
                </form>
            </div>
        </div>
    </div>


@endsection

@section('js')
<script>
    $(function() {
        // JQuery: Pak al deze elementen met button klasse btn-delete, voer functie uit zodra je erop klikt
        $('.btn-delete').click(function(e){
            e.preventDefault();                 // Ik prevent het 'default gedrag', NIETS laden dus bij klikken delete!
            var id = $(this).attr('data-id');   // Vang het ID attribuut van de knop op
            $('#id').attr('value', id);          // Verander de value van de form op het id nummer

            var modalData = {};

            document.querySelector('#ajax_id').innerHTML = "";
            document.querySelector('#ajax_name').innerHTML = "";
            document.querySelector('#ajax_email').innerHTML = "";
            document.querySelector('#ajax_created_at').innerHTML = "";
            $.get("api/user/read/" + id).done(function(data){
                console.log(data);
                modalData = data;

                document.querySelector('#ajax_id').innerHTML = modalData.id;
                document.querySelector('#ajax_name').innerHTML = modalData.name;
                document.querySelector('#ajax_email').innerHTML = modalData.email;
                document.querySelector('#ajax_created_at').innerHTML = modalData.created_at;

            }).fail(function(){
                console.log("Error loading user json data....");
            });
        });

        $('#delete-form-ok').click(function(e){
            $('#delete-form').modal('toggle');
        })
    });
</script>

@endsection