@extends('layouts.app')

@section('css')

    <link rel="stylesheet" href="/css/bootstrap-toggle.css">

@endsection

@section('content')

<div class="container">

    @isset($restaurant->id)
        <a href="/restaurant/{{ $restaurant->id }}/beheer"><< Terug naar overzicht</a>
        <h2>Restaurantgegevens wijzigen</h2>
    @else
        <a href="/restaurant/"><< Terug naar overzicht</a>
        <h2>Restaurant toevoegen</h2>
    @endisset

    <div class="row">
        <div class="col-md-12">
            @include('partials.errors', ['errors' => $errors])     <!--Aparte partial voor validatie-->
        </div>
    </div>

    @isset($restaurant['id'])
        <form method="post" action="/restaurant/{{ $restaurant['id'] }}">
            {{--Spoof methods PUT (delete and replace record content) / PATCH (replace parts) / DELETE--}}
            {{ method_field('PATCH') }}
            {{--<input type="hidden" name="_method" value="PATCH">--}}
    @else
        <form method="post" action="/restaurant" }}">
    @endisset

        {{ csrf_field() }}
        @isset($restaurant['id'])
        <div class="form-group">
            <label class="col-md-3"for="usr">Restaurant ID:</label>
            <div class="col-md-9">
                <input type="text" class="form-control" id="restaurant-id" name="id" value="{{ old('id', isset($restaurant['id']) ? $restaurant['id'] : '') }}" readonly>
            </div>
        </div>
        @endisset

        <div class="form-group">
            <label class="col-md-3"for="usr">Naam</label>
            <div class="col-md-9">
                <input type="text" class="form-control" id="restaurant-naam" name="naam_restaurant" value="{{ old('naam_restaurant', isset($restaurant['naam_restaurant']) ? $restaurant['naam_restaurant'] : '') }}">
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-3"for="usr">Omschrijving</label>
            <div class="col-md-9">
                <textarea class="form-control" name="omschrijving">{{old('omschrijving', isset($restaurant['omschrijving']) ? $restaurant['omschrijving'] : '')}}</textarea>
                {{--<input type="" class="form-control" id="restaurant-omschrijving" name="omschrijving" value="{{ old('omschrijving', isset($restaurant['omschrijving']) ? $restaurant['omschrijving'] : '') }}" readonly>--}}
            </div>
        </div>

        @isset($restaurant->id)
        <div class="form-group">
            <label class="col-md-3"for="usr">Actief</label>
            <div class="col-md-9">
                {{--Als checkbox ungechecked is, heeft het anders geen waarde in $request (wordt niet gepost). Oplossing: hidden dummy-input...--}}
                <input type="hidden" name="actief" value="0">
                <input type="checkbox" name="actief" class="toggletest" id="toggle{{ $restaurant->id }}"
                       @if(old('actief', $restaurant->actief))
                           checked
                       @endif
                       value="1" data-on="online" data-off="offline" data-toggle="toggle" data-onstyle="success">
            </div>
        </div>
        @endisset
        <div class="form-group">
            <label class="col-md-3"for="usr">E-mail Restaurant</label>
            <div class="col-md-9">
                <input type="text" class="form-control" id="restaurant-email" name="email_restaurant" value="{{ old('email_restaurant', isset($restaurant['email_restaurant']) ? $restaurant['email_restaurant'] : '') }}">
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-3"for="usr">Telefoon Restaurant</label>
            <div class="col-md-9">
                <input type="text" class="form-control" id="restaurant-tel" name="tel_restaurant" value="{{ old('tel_restaurant', isset($restaurant['tel_restaurant']) ? $restaurant['tel_restaurant'] : '') }}">
            </div>
        </div>

        {{--<br><br>--}}
        {{--Indien admin, toon eigenaar restaurant:--}}
        @isset($restaurant->users_id)
        @if(Auth::user()->hasRole('admin'))
        <div class="form-group">
            <label class="col-md-3"for="usr">Owner:
                <a class="btn btn-warning btn-xs btn-detail" href="/admin/user/profile/{{ $restaurant->users_id }}">
                    <span class="glyphicon glyphicon-zoom-in"></span>
                </a>
            </label>
            <div class="col-md-9">
                    <input type="text" class="form-control" id="restaurant-tel" name="tel_restaurant" value="{{ $owner }}" disabled>
            </div>
        </div>
        @endif

        @else
            <div class="form-group">
                <label class="col-md-3" for="land">Eigenaar:</label>
                <div class="col-md-9">
                    <select class="col-md-9 form-control" required="" autocomplete="on" id="owner" name="users_id">
                        <option value="null">...</option>
                        @foreach($owners as $owner)
                            <option value="{{$owner->id}}" {{ $owner->id == old('users_id') ? 'selected' : '' }}>{{ $owner->lastname }} {{ $owner->firstname }} - ({{ $owner->email }})</option>
                        @endforeach
                    </select>
                </div>
            </div>
        @endisset

        <div class="form-group">
            <label class="col-md-3"for="usr">Straat</label>
            <div class="col-md-9">
                <input type="text" class="form-control" id="restaurant-straat" name="straat" value="{{ old('straat', isset($restaurant['straat'])) ? $restaurant['straat'] : '' }}">
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-3"for="usr">Nr</label>
            <div class="col-md-9">
                <input type="text" class="form-control" id="restaurant-nr" name="nr" value="{{ old('nr', isset($restaurant['nr']) ? $restaurant['nr'] : '') }}">
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-3"for="usr">Bus</label>
            <div class="col-md-9">
                <input type="text" class="form-control" id="restaurant-bus" name="extension" value="{{ old('extension', isset($restaurant['extension']) ? $restaurant['extension'] : '') }}">
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-3"for="usr">Postcode</label>
            <div class="col-md-9">
                <input type="text" class="form-control" id="restaurant-pc" name="postcode" value="{{ old('postcode', isset($restaurant['postcode']) ? $restaurant['postcode'] : '') }}">
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-3"for="usr">Gemeente</label>
            <div class="col-md-9">
                <input type="text" class="form-control" id="restaurant-city" name="gemeente" value="{{ old('gemeente', isset($restaurant['gemeente']) ? $restaurant['gemeente'] : '') }}">
            </div>
        </div>

        <div class="form-group">
            <div class="col-md-3"></div>
            <div class="col-md-9">
                @isset($restaurant->id)

                    <button type="submit" class="btn btn-default">Update restaurantgegevens</button>
                    <a href="/restaurant/{{ $restaurant->id }}/beheer" class="btn btn-default">Annuleer</a>

                @else
                    <button type="submit" class="btn btn-default">Activeer nieuw Restaurant!</button>
                    <a href="/restaurant/" class="btn btn-default">Annuleer</a>

                @endisset
            </div>
        </div>
    </form>
</div>

@endsection

@section('js')

<script>

</script>

@endsection