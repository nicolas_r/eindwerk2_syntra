{{--Bestellingen bestaan uit een lijst bestelbons (met bestelde content...)--}}

@extends('layouts.app')

@section('css')

@endsection

@section('content')

    <div class="container">
        <a href="/restaurant/{{$currentRestaurant}}/beheer"><< Terug naar portaalsite restaurant</a>
        <div class="row">
            <div class="col-md-8">
                <h2>"{{$restaurant_naam}}"</h2>
                <h2>Beheer bestellingen</h2><br>
            </div>
            <div class="col-md-4">
                <form method="get" action="">
                    {{--{{csrf_field()}}--}}
                    <label for="filterstatus">Lijst filteren:</label>
                    <select class="form-control" id="filterstatus" name="filterstatus" onchange="this.form.submit()">
                        <option value="0">Alles (ongefilterd)</option>
                        @foreach($statuses as $status)
                            <option value="{{$status->id}}" {{$filterId == $status->id ? 'selected' : '' }}>{{$status->status}}</option>
                        @endforeach
                    </select>
                </form>
            </div>
        </div>

        <div class="table-responsive">
            <table class="table">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Details Bestelling</th>
                    <th>Naam</th>
                    <th>Besteltijd</th>
                    <th>Tijdstip levering</th>
                    <th>Status bestelling</th>
                    <th>Extra wachttijd</th>
                </tr>
                </thead>
                <tbody>
                    @foreach($bestelbondata as $data)
                        <tr>
                            <td>{{$data->id}}</td>
                            <td><a href="./{{$currentRestaurant}}/{{$data->id}}" class="btn btn-primary">Details</a></td>
                            <td>{{$data->user->name}}</td>
                            <td>{{$data->bestellingtijdstip}}</td>
                            <td></td>
                            <td>
                                <form method="post" action="./{{$currentRestaurant}}/{{$data->id}}/wijzigstatus">
                                    {{csrf_field()}}
                                    <input type="hidden" name="overzicht" value="true">
                                    <select class="form-control" id="status" name="status" onchange="this.form.submit()">
                                        @foreach($statuses as $status)
                                        <option value="{{$status->id}}" {{ $status->id == $data->bestellingstatusen_id ? 'selected' : ''}}>{{$status->status}}</option>
                                        @endforeach
                                    </select>
                                </form>
                            </td>
                            <td>{{$data->extrawachttijd}} minuten</td>
                        </tr>
                    @endforeach
                </tbody>
            </table>

            {{ $bestelbondata->links() }}
        </div>

    </div>

@endsection

@section('js')

@endsection