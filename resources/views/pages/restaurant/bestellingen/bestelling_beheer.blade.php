{{--Bestellingen bestaan uit een lijst bestelbons (met bestelde content...)--}}

@extends('layouts.app')

@section('css')

@endsection

@section('content')

    <div class="container">
        <a href="/restaurant/bestellingen/{{$currentRestaurant->id}}"><< Terug naar overzicht gerechten</a>
        <div class="row">
            <h2>"{{$currentRestaurant->naam_restaurant}}"</h2>
            <h2>Detail Bestelling nr {{$invoiceData['order_nr']}}</h2><br>
        </div>

        <div class="table-responsive">
            <table class="table">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Gerecht</th>
                    <th>Aantal</th>
                    <th>Eenheidsprijs</th>
                    <th>Subtotaal</th>
                    <th>Delete</th>
                </tr>
                </thead>
                <tbody>
                    @foreach($invoiceData['order_items'] as $data)
                    <tr>
                        <td>{{$data['artikelnr']}}</td>
                        <td>{{$data['gerechtnaam']}}</td>
                        <td>{{$data['aantal']}}</td>
                        <td>{{$data['eenheidsprijs'] + $data['eenheidsprijs'] * $data['btw_basis'] / 100}}</td>
                        <td>{{$data['subtotaal'] + $data['subtotaal_btw']}}</td>
                        <td>delete</td>
                    </tr>
                    @endforeach

                </tbody>
            </table>
        </div>

        <h3>Totaal bedrag te betalen (incl BTW): {{$invoiceData['totaal']['algemeen']}} €</h3>
        <hr>
        <div class="row">
            <div class="col-md-6">
                <h3>Leveringsadres:</h3>
                {{$leveringsadres->straat}} {{$leveringsadres->nr}} {{$leveringsadres->extension}}<br>
                {{$leveringsadres->postcode}} {{$leveringsadres->stad}}
            </div>
            <div class="col-md-6"><br>
                Telefoon klant: {{$gebruikersData->tel}}<br>
                E-mail klant: <a href="mailto:{{$gebruikersData->email}}">{{$gebruikersData->email}}</a>
            </div>
        </div>
        <hr>
        <div class="row">
            <div class="col-md-3">
                <form method="post" action="./{{$bestellingId}}/wijzigstatus">
                    {{csrf_field()}}
                    <label for="filterstatus">Bestelling status:</label>
                    <select class="form-control" id="status" name="status" onchange="this.form.submit()">
                        @foreach($statuses as $status)
                            <option value="{{$status->id}}" {{$bestellingStatus == $status->id ? 'selected' : '' }}>{{$status->status}}</option>
                        @endforeach
                    </select>
                </form>
            </div>
            <div class="col-md-6">
                Besteltijd: {{ $bestellingTijdstip }}<br>
                Geschatte bereidingstijd: #<br>
                Gewenste levertijd:
            </div>
            <div class="col-md-3">
                <a href="/restaurant/bestellingen/{{$currentRestaurant->id}}" class="btn btn-primary">Overzicht bestellingen</a>
            </div>
        </div>
    </div>

@endsection

@section('js')

@endsection