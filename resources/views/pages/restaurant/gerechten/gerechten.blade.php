{{--Oplijsten gerechten, gerechten toevoegen, gerechten verwijderen, elk gerecht van 1 of meerdere foto's voorzien...--}}
@extends('layouts.app')

@section('css')

@endsection

@section('content')

    <div class="container">
        <a href="/restaurant/{{ $restaurantId }}/beheer"><< Terug naar overzicht</a>
        <br>OVERZICHT GERECHTEN! --

        <ul>
            <li>Menu kaart gerechten overzicht (tabel naam gerecht, omschrijving, prijs, bereidingstijd, soort verpakking (id)</li>
            <li>Menu categorieŽn (drank, voorgerecht, hoofdgerecht, desserts) (kan voor elk restaurant id anders zijn (!! tabel bestaat nog niet !!)</li>

        </ul>

        <a href="./gerechten/create" class="btn btn-default">Voeg Gerecht toe!</a>

        {{--$table->increments('id');--}}
        {{--$table->string('naam', 255);--}}
        {{--$table->text('omschrijving')->nullable();--}}
        {{--$table->double('prijs', 6, 2);--}}
        {{--$table->smallInteger('bereidingstijd')->default('0');--}}
        {{--$table->unsignedInteger('restaurants_id');--}}
        {{--$table->unsignedInteger('verpakkingen_id');        --}}

        <table class="table table-striped table-responsive">
            <thead>
            <tr>
                <th>ID</th>
                <th>Naam Gerecht</th>
                <th>Omschrijving</th>
                <th>Prijs</th>
                <th>Bereidingstijd</th>
                <th>Foto</th>
            </tr>
            </thead>
            <tbody>
            @foreach($gerechten as $gerecht)
                <tr>
                    <td>{{ $gerecht->id }}</td>
                    <td id="table-gerecht_{{$gerecht->id}}_naam">{{ $gerecht->naam }}</td>
                    <td id="table-gerecht_{{$gerecht->id}}_omschrijving">{{ $gerecht->omschrijving }}</td>
                    <td>{{ $gerecht->prijs }}</td>
                    <td>{{ $gerecht->bereidingstijd}}</td>
                    <td>
                        <a id="resto-detail-{{ $gerecht->id }}" class="btn btn-warning btn-xs btn-detail" href="/restaurant/{{ $restaurantId }}/gerechten/{{ $gerecht->id }}/edit">
                            <span class="glyphicon glyphicon-zoom-in"></span>
                        </a>
                    </td>
                    <td>
                        <a data-id="{{ $gerecht->id }}" class="delete_button" class="btn btn-danger btn-xs btn-delete" data-toggle="modal" data-target="#delete-form">
                            <span class="glyphicon glyphicon-trash"></span>
                        </a>
                    </td>

                </tr>
            @endforeach

            <! -- show modal -->
            <div class="modal fade" id="delete-form" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">x</span>
                            </button>
                            <h4 class="modal-title" id="myModalLabel">Bent u zeker dat u dit item wenst te verwijderen?</h4>
                        </div>

                                {{--Boostrap bug in modal, form elements are kicked-out, adding form="" to resolve! --}}
                                {{--<form method="post" id="verwijder_song" action="/beheer/oostendezingt/"></form>--}}
                                {{--<input id="delete-form-ok" class="btn btn-default" type="submit" value="Ja" form="verwijder_song">--}}
                                {{--<input type="hidden" name="_method" value="DELETE" form="verwijder_song">--}}
                                {{--<input type="hidden" name="_token" value="{{ csrf_token()}}" form="verwijder_song">--}}
                                {{--<button class="btn btn-default" data-dismiss="modal" aria-label="Close">Neen--}}
                                {{--</button>--}}

                        <form method="post" id="verwijder_gerecht" action="/restaurant/{{ $restaurantId }}/gerechten/">
                            <input id="id" name="id" type="hidden" value="" />
                            {{ method_field('DELETE') }}
                            {{ csrf_field() }}

                            <div class="modal-body">
                                <h4>Wenst u het volgende gerechtje definitief verwijderen van de kaart?</h4>
                                <p id="gerecht_naam"></p>
                                <p id="gerecht_omschrijving"></p>
                            </div>

                            <div class="modal-footer">
                                <input id="delete-form-ok" class="btn btn-default" type="submit" value="Ja">
                                <button class="btn btn-default" data-dismiss="modal" aria-label="Close">Neen</button>
                            </div>
                        </form>

                    </div>
                </div>
            </div>

            </tbody>
        </table>

    </div>

@endsection

@section('js')
<script>
    (function() {

        console.log("initializing script");

        var _setDeleteButton = function(gerecht_id){

            var verwijderGerechtModalForm = document.querySelector('#verwijder_gerecht');
            var defaultAttribute = verwijderGerechtModalForm.getAttribute('action');

            var gerecht_naam = document.querySelector('#gerecht_naam');
            var gerecht_omschrijving = document.querySelector('#gerecht_omschrijving');

            function setDeleteButton(){

                gerecht_naam.innerHTML = document.querySelector('#table-gerecht_' + gerecht_id + '_naam').innerHTML;
                gerecht_omschrijving.innerHTML = document.querySelector('#table-gerecht_' + gerecht_id + '_omschrijving').innerHTML;

                verwijderGerechtModalForm.setAttribute('action', defaultAttribute + gerecht_id);
                console.log("delete: " + verwijderGerechtModalForm.getAttribute('action'));
            }

            return setDeleteButton;
        };

        var restaurantIds = document.querySelectorAll('.delete_button');

        [].forEach.call(restaurantIds, function(restaurantId) {
            delete_id = restaurantId.getAttribute('data-id');
            restaurantId.addEventListener('click', _setDeleteButton(delete_id));
            //console.log("adding event listener for _setDeleteButton : " + delete_id);
        });

    })();

</script>
@endsection