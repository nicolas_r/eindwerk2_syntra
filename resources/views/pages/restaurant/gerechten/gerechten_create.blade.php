{{--Oplijsten gerechten, gerechten toevoegen, gerechten verwijderen, elk gerecht van 1 of meerdere foto's voorzien...--}}
@extends('layouts.app')

@section('css')
    <link rel="stylesheet" href="/css/vendor/dropzone/dropzone.css">
    <link rel="stylesheet" href="/css/vendor/dropzone/basic.css">
@endsection

@section('content')


    @if(isset($fotoData))
    {{--{{ dump($fotoData) }}--}}
    @endif

    <div class="container">

        <h1>Welkom bij "{{ $restaurantNaam }}" !</h1>

        Uiteraard kan je bij elk nieuw restaurant, nieuwe lekkere gerechtjes toevoegen of wijzigen!

@include('partials.errors')
        @if(isset($gerechtData['id']))
            <form method="post" action="../{{ $gerechtData['id'] }}" id="nieuwGerecht">
        @else
            <form method="post" action="./" id="nieuwGerecht">
        @endif

        @if(isset($gerechtData))
            {{--Data exists: edit--}}
            {{ method_field('PATCH') }}
        @endif {{--Empty form: create--}}
            {{ csrf_field() }}
            <div class="form-group">
                <label class="col-md-3"for="usr">Naam Gerecht:</label>
                <div class="col-md-9">
                    <input type="text" class="form-control" id="naam" name="naam" value="{{ old('naam', isset($gerechtData['naam']) ? $gerechtData['naam'] : '') }}">
                </div>
            </div>

            <div class="form-group">
                <label class="col-md-3"for="usr">Omschrijving Gerecht:</label>
                <div class="col-md-9">
                    <input type="text" class="form-control" id="omschrijving" name="omschrijving" value="{{ old('omschrijving', isset($gerechtData['omschrijving']) ? $gerechtData['omschrijving'] : '') }}">
                </div>
            </div>

            <div class="form-group">
                <label class="col-md-3"for="usr">Netto Prijs:</label>
                <div class="col-md-9">
                    <input type="text" class="form-control" id="prijs" name="prijs" value="{{ old('prijs', isset($gerechtData['prijs']) ? $gerechtData['prijs'] : '') }}">
                </div>
            </div>

            <div class="form-group">
                <label class="col-md-3"for="usr">BTW (in %):</label>
                <div class="col-md-9">
                    <input type="number" class="form-control" id="btw" name="btw" value="{{ old('btw', isset($gerechtData['btw']) ? $gerechtData['btw'] : '') }}">
                </div>
            </div>

            <div class="form-group">
                <label class="col-md-3"for="usr">Bereidingstijd:</label>
                <div class="col-md-9">
                    <input type="text" class="form-control" id="bereidingstijd" name="bereidingstijd" value="{{ old('bereidingstijd', isset($gerechtData['bereidingstijd']) ? $gerechtData['bereidingstijd'] : '') }}">
                </div>
            </div>


            <div class="form-group">
                <label class="col-md-3"for="usr">Verpakkingstype:</label>
                <div class="col-md-9">
                    @if(count($verpakkingen))
                        <select name="verpakkingen_id" class="form-control">
                            <option value=""></option>
                            @foreach($verpakkingen as $verpakking)

                                <option {{ $verpakking['id'] == old('verpakkingen_id', (isset($gerechtData)) ? $gerechtData->verpakkingen_id : '') ? 'selected' : '' }} value="{{ $verpakking['id'] }}">{{ $verpakking['naam'] }} @if($verpakking['lengte']) (L x B x H) - {{ $verpakking['lengte'] }} x {{ $verpakking['breedte'] }} x {{ $verpakking['hoogte'] }} @endif</option>
                                {{--<option {{ $verpakking['id'] == old('verpakkingen_id', ($gerechtData) ? $gerechtData->verpakkingen_id : '') ? 'selected' : '' }} value="{{ $verpakking['id'] }}">{{ $verpakking['naam'] }}</option>--}}

                            @endforeach
                        </select>
                    @else
                        Geen verpakkingen gevonden!?
                    @endif
                    <a href='/restaurant/{{ $restaurantId }}/verpakkingen' target="_blank">Beheer verpakkingen</a>
                    {{--<input type="text" class="form-control" id="naam" name="naam" value="{{ old('naam', isset($gerechtData['naam']) ? $gerechtData['naam'] : '') }}" readonly>--}}
                </div>
            </div>

        </form>

        @if(isset($gerechtData))
            {{--Data exists: edit--}}
            <button type="submit" class="btn btn-default" form="nieuwGerecht">Save changes!</button>
        @else
            {{--New Data: create--}}
            <button type="submit" class="btn btn-default" form="nieuwGerecht">Maak nieuw gerecht!</button>
        @endif

        <a href="/restaurant/{{$restaurantId}}/gerechten" class="btn btn-default">Annuleren</a><br><br>

        <div>
            <div class="row">

                @isset($fotoData)
                    @foreach($fotoData as $foto)
                    <div class="col-md-4">
                        <div class="thumbnail">
                            <a href="#">

                                {{--<img src="{{ '/' . $fotoData[0]['path'] . '/' . $fotoData[0]['filename'] }}">--}}
                                {{ dump($foto['path']) }}
                                <img src="{{ $foto['path'] }}" alt="Test" style="width:100%">
                                {{--<img src="/images/test.jpg?w=200&amp;s=32b5b572d011b82f50bf31c4e2ad5d12" alt="Test" style="width:100%">--}}
                                <div class="caption">
                                    {{--<p>TEST...</p>--}}
                                    <div>
                                        <form action="./fotos/{{$foto['id']}}/delete" method="post">
                                            {{ csrf_field() }}
                                            <input type="submit" class="btn btn-danger pull-right" value="Delete">
                                        </form>
                                    </div>
                                    <label for="gerecht_img"><input form="nieuwGerecht" type="radio" name="fotos" value="{{ $foto['id'] }}"> Stel in als hoofdafbeelding</label>
                                </div>
                            </a>
                        </div>
                    </div>
                    @endforeach
                @endisset
            </div>
        </div>

        <form action="/restaurant/{{$restaurantId}}/gerechten/upload" class="dropzone">

            <div class="form-group">
                <label class="col-md-3"for="usr"></label>
                <div class="col-md-9 fallback">
                    <input name="file" type="file" multiple />
                    <input name="restaurantId" type="hidden" value="{{$restaurantId}}">
                    <input name="gerechtId" type="hidden" value="{{isset($gerechtData['id']) ? $gerechtData['id'] : ''}}">
                </div>
            </div>

            <input name="restaurantId" type="hidden" value="{{$restaurantId}}">
            <input name="gerechtId" type="hidden" value="{{isset($gerechtData['id']) ? $gerechtData['id'] : ''}}">
            {{--CSRF kan ook via de header van dropzone worden meegegeven, als je niet via een form of hidden field de csrf meegeeft (xsrf token--}}
            {{ csrf_field() }}
        </form><br>
    </div>

@endsection

@section('js')
    <script src="/js/vendor/dropzone.js"></script>
@endsection