@extends('layouts.app')

@section('content')
    <div class="container" ng-app="getVerpakkingen">

        <a href="/restaurant/{{ $restaurantId }}/beheer"><< Terug naar overzicht</a>

        {{--Angular Application test!--}}
        {{--Verification number: @{{ 4 + 4 }}--}}

        <h2>@lang('verpakking.verpakkingen')</h2>
        <div ng-controller="VerpakkingenController">
            <table class="table table-striped">
                <thead>
                <tr>
                    <th>ID</th>
                    <th>@lang('verpakking.naam')</th>
                    <th>@lang('verpakking.lengte')</th>
                    <th>@lang('verpakking.breedte')</th>
                    <th>@lang('verpakking.hoogte')</th>
                    {{--<th>Gewicht</th>                                --}}
                    <th>
                        <button id="btn-add" class="btn btn-success btn-xs" ng-click="toggle('add', 0)">
                            @lang('verpakking.voegtoe')
                        </button>
                    </th>
                </tr>

                </thead>
                <tbody>
                <tr ng-repeat="verpakking in verpakkingen">
                    <td>@{{ verpakking.id }}</td>
                    <td>@{{ verpakking.naam }}</td>
                    <td>@{{ verpakking.lengte }}</td>
                    <td>@{{ verpakking.breedte }}</td>
                    <td>@{{ verpakking.hoogte }}</td>
                    {{--<td>@{{ verpakking.gewicht }}</td>--}}
                    <td>
                        <button class="btn btn-warning btn-xs btn-detail" ng-click="toggle('edit', verpakking.id)">
                            <span class="glyphicon glyphicon-edit"></span>
                        </button>
                        <button class="btn btn-danger btn-xs btn-delete" ng-click="verifyDeleteConfirmation(verpakking.id)">
                            <span class="glyphicon glyphicon-trash"></span>
                        </button>
                    </td>
                </tr>
                </tbody>
            </table>
            <! -- show modal -->
            <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
                 aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">x</span>
                            </button>
                            <h4 class="modal-title" id="myModalLabel">@{{ form_title }}</h4>
                        </div>
                        <div class="modal-body">
                            <form name="frmVerpakking" class="form-horizontal" novalidate="">
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">@lang('verpakking.naam')</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" id="naam" name="naam"
                                               placeholder="Naam verpakking" value="@{{ verpakking.naam }}"
                                               ng-model="verpakking.naam" ng-required="true">
                                        <span ng-show="frmVerpakking.naam.$invalid && frmVerpakking.naam.$touched">U hebt geen naam opgegeven voor de toe te voegen verpakking?</span>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-3 control-label">@lang('verpakking.lengte')</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" id="lengte" name="lengte"
                                               placeholder="Lengte verpakking" value="@{{ verpakking.lengte }}"
                                               ng-model="verpakking.lengte" ng-required="true">
                                        <span ng-show="frmVerpakking.lengte.$invalid && frmVerpakking.lengte.$touched">U hebt geen lengte opgegeven voor de toe te voegen verpakking?</span>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-3 control-label">@lang('verpakking.width')</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" id="breedte" name="breedte"
                                               placeholder="Breedte verpakking" value="@{{ verpakking.breedte }}"
                                               ng-model="verpakking.breedte" ng-required="true">
                                        <span ng-show="frmVerpakking.breedte.$invalid && frmVerpakking.breedte.$touched">U hebt geen breedte opgegeven voor de toe te voegen verpakking?</span>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-3 control-label">@lang('verpakking.height')</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" id="hoogte" name="hoogte"
                                               placeholder="Hoogte verpakking" value="@{{ verpakking.hoogte }}"
                                               ng-model="verpakking.hoogte" ng-required="true">
                                        <span ng-show="frmVerpakking.hoogte.$invalid && frmVerpakking.hoogte.$touched">U hebt geen hoogte opgegeven voor de toe te voegen verpakking?</span>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-primary" id="btn-save" ng-click="save(modalstate, id)"
                                    ng-disabled="frmVerpakking.$invalid">@lang('btn_controls.bewaarveranderingen')
                            </button>
                        </div>
                    </div>
                </div>
            </div>

            <! -- show modal -->
            <div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
                 aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">x</span>
                            </button>
                            <h4 class="modal-title" id="myModalLabel">@lang('btn_controls.bevestigverwijderen')</h4>
                        </div>
                        <div class="modal-body">
                            <p>@lang('verpakking.bevestigdefinitiefverwijderen')</p>
                            <p>@lang('verpakking.naam'): @{{ verpakking.naam }}</p>
                            <p>@lang('verpakking.hoogte'): @{{ verpakking.hoogte }}</p>
                            <p>@lang('verpakking.breedte'): @{{ verpakking.breedte }}</p>
                            <p>@lang('verpakking.lengte'): @{{ verpakking.lengte }}</p>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-primary" ng-click="confirmDelete(verpakking.id)">@lang('btn_controls.ja')
                            </button>
                            <button type="button" class="btn btn-default" ng-click="cancelDelete()">@lang('btn_controls.neen')</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
    <script src="{{ asset('js/angular.js') }}"></script>

    <!-- Custom Scripts-->
    <script src="{{ asset('js/app/app.js') }}"></script>
    <script src="{{ asset('js/app/controllers/VerpakkingenController.js') }}"></script>
@endsection