{{--Kalender-Backend waarin je openingsuren / sluitingsuren / sluitingsdagen kunt vastleggen...--}}
@extends('layouts.app')

@section('css')
<style>
    .dropdown-toggle {
        height: auto;
        max-height: 100px;
        overflow-x: hidden;
    }


</style>

@if(false)

@endif

@endsection

@section('content')

    <div class="container">

        <a href="/restaurant/{{$currentRestaurant}}/beheer"><< Terug naar portaalsite restaurant</a>

        @include('partials.errors')

        <h2>OPENINGSUREN</h2><br>

        <p>Uren invullen, toevoegen en zou op scherm moeten komen</p>
        <p>Bij knop bewaren, wordt hetgeen op het scherm is afgebeeld, bewaard in desbetreffende record van het restaurant...</p>

        <form method="post" action="openingsuren/addtime" id="voegTijd">
            {{ csrf_field() }}
            <div class="form-group">
                <label class="col-xs-3" for="weeklijst">Weekdag:</label>
                <div class="col-xs-9">
                    <select class="form-control" id="weeklijst" name="weekdag">
                        <option>---</option>
                        <option value="monday" {{ old('weekdag') == 'monday' ? 'selected' : '' }}>Maandag</option>
                        <option value="tuesday" {{ old('weekdag') == 'tuesday' ? 'selected' : '' }}>Dinsdag</option>
                        <option value="wednesday" {{ old('weekdag') == 'wednesday' ? 'selected' : '' }}>Woensdag</option>
                        <option value="thursday" {{ old('weekdag') == 'thursday' ? 'selected' : '' }}>Donderdag</option>
                        <option value="friday" {{ old('weekdag') == 'friday' ? 'selected' : '' }}>Vrijdag</option>
                        <option value="saturday" {{ old('weekdag') == 'saturday' ? 'selected' : '' }}>Zaterdag</option>
                        <option value="sunday" {{ old('weekdag') == 'sunday' ? 'selected' : '' }}>Zondag</option>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label class="col-xs-3" for="weeklijst">Openingsuur:</label>
                <div class="col-xs-9">
                    <input class="form-control" name="openingsuur" type="time" value="{{old('openingsuur')}}">
                </div>
            </div>
            <div class="form-group">
                <label class="col-xs-3" for="weeklijst">Sluitingsuur:</label>
                <div class="col-xs-9">
                    <input class="form-control" name="sluitingsuur" type="time" value="{{old('sluitingsuur')}}">
                </div>
            </div>
        </form>

        <button class="btn" type="submit" value="Submit" form="voegTijd">Voeg tijd toe</button>

        <br><br>

        @if(session('input_openingsuren'))
            {{--<div class="alert alert-success">--}}
                {{--{{ dump(session('input_openingsuren')) }}--}}
            {{--</div>--}}
            <div class="col-md-6">
                Nieuwe openingsuren
                <div>
                    <ul>
                        @foreach(session('input_openingsuren') as $key_dag => $dag)
                            <li>{{ $key_dag }}</li>
                                <ul>
                                    @foreach($dag as $key_uren => $uren)
                                        <li>{{$uren}}</li>
                                    @endforeach
                                </ul>
                        @endforeach
                    </ul>
                </div>
                <a class="btn btn-warning" href="openingsuren/createTime">Bewaar ingevoerde tijden</a>
                <a class="btn btn-danger" href="openingsuren/reset">Reset</a>

            </div>

        @else
            <div class="col-md-6">
            </div>
        @endif

        @isset($huidigeopeningstijden)
            <div class="col-md-6">
                Huidige openingsuren:
                <ul>
                    @foreach($huidigeopeningstijden as $key_dag => $dag)
                        <li>{{ $key_dag }}</li>
                        <ul>
                            @foreach($dag as $key_uren => $uren)
                                <li>{{$uren}}</li>
                            @endforeach
                        </ul>
                    @endforeach
                </ul>
                {{--{{ dump($huidigeopeningstijden) }}--}}
            </div>
        @endisset


        {{--Data pushen naar sessie per submit (array wordt verder aangevuld)--}}
        {{----}}
        {{--Bij Saven kan je de data uit de sessie (definitief) storen naar database.--}}
        {{--Alleja sort of... ;)--}}

        {{----}}
        {{--@isset($openingsuren)--}}
        {{--{{ dump($openingsuren) }}--}}
        {{--@endisset--}}

    </div>


@endsection

@section('js')

@endsection