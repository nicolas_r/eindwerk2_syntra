<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>

    <!-- Store CSRF Token in HTML meta tag -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Default Styles -->
    <link href="{{ asset(mix('/css/app.css')) }}" rel="stylesheet">
    @yield('css')

    <!-- Default Scripts -->
    <script>
        window.Laravel = {!! json_encode([
            'csrfToken' => csrf_token(),
        ]) !!};
    </script>
</head>
<body>

@yield('content')

<!-- Scripts -->
<script src="{{ asset(mix('js/app.js')) }}"></script>
@yield('js')

</body>
</html>