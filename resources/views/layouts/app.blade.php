<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Styles -->
    <link href="{{ asset(mix('css/app.css')) }}" rel="stylesheet">
    @yield('css')

    <!-- Scripts -->
    <script>
        window.Laravel = {!! json_encode([
            'csrfToken' => csrf_token(),
        ]) !!};
    </script>
    <style>
        .navbar {
            position: relative;
            min-height: 250px;
            margin-bottom: 22px;
            border: 1px solid transparent;
            background-image: url(/assets/images/pizza.jpg);
        }

        .dropdown-toggle, .navbar-right, .navbar-brand, .navbar-toggle {
            background-color: rgba(0,0,0,.75);

        }

        .navbar-default .navbar-nav > li > a {
            color: #fff;
        }

        .navbar-default .navbar-brand {
            color: #fff;
        }

        #navbar-container {
            position: relative;
        }

        .winkelwagentje-knop {
            position: absolute;
            text-align: left;
            font-size: 30px;
            right: 10%;
            top: 300%;
        }

        .contactform-knop {
            position: absolute;
            text-align: left;
            font-size: 30px;
            left: 10%;
            top: 300%;

            box-sizing: content-box;
            width: 55px;
            text-align: center;
        }

        .glyphicon-shopping-cart {
            font-size: 30px;
            vertical-align:middle;
        }

        .glyphicon-envelope {
            /*transform: translate(0,20%);*/
            font-size: 30px;
            vertical-align:middle;
        }

    </style>
</head>
<body>
    <div id="app">
        <nav class="navbar navbar-default navbar-static-top">

            <div class="container" id="navbar-container">
                <a class="btn btn-primary winkelwagentje-knop" href="/winkelwagen">
                    <span class="glyphicon glyphicon-shopping-cart"></span> {{ Cart::count() }}
                </a>

                <a class="btn btn-primary contactform-knop" href="/contact">
                    <span class="glyphicon glyphicon-envelope"></span>
                </a>


                <div class="navbar-header">


                    <!-- Collapsed Hamburger -->
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                        <span class="sr-only">Toggle Navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>

                    <!-- Branding Image -->
                    <a class="navbar-brand" href="{{ url('/') }}">
                        {{ config('app.name', 'Laravel') }}
                    </a>
                </div>

                <div class="collapse navbar-collapse" id="app-navbar-collapse">
                    <!-- Left Side Of Navbar -->
                    <ul class="nav navbar-nav">
                        &nbsp;
                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="nav navbar-nav navbar-right">
                        <!-- Authentication Links -->
                        @if (Auth::guest())
                            <li><a href="{{ route('login') }}">Login</a></li>
                            <li><a href="{{ route('register') }}">Registreer</a></li>


                        @else

                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <ul class="dropdown-menu" role="menu">
                                    <li>
                                        <a href="/">Overzicht restaurants</a>
                                    </li>
                                    <li>
                                        <a href="/winkelwagen">Winkelwagen</a>
                                    </li>
                                    @if(Auth::user()->hasRole('admin'))
                                    <li>
                                        <a href="/admin/users">Beheer Gebruikers (role: admin)</a>
                                    </li>
                                    @endif
                                    @if(Auth::user()->hasRole('restaurant'))
                                    <li>
                                        <a href="/restaurant/">Beheer Restaurants (role: restaurant)</a>
                                    </li>
                                    @endif
                                    @if(Auth::user()->hasRole('leverancier'))
                                    <li>
                                        <a href="#">Beheer Leveranciers (role: leverancier)</a>
                                    </li>
                                    @endif
                                    <hr>
                                    <li>
                                        <a href="/user/profile/{{Auth::user()->id}}">Profielgegevens</a>
                                    </li>
                                    <li>
                                        <a href="{{ route('logout') }}"
                                            onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                            Logout
                                        </a>

                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                                    </li>
                                    <li>

                                    </li>
                                </ul>
                            </li>
                        @endif
                    </ul>
                </div>
            </div>
        </nav>

        @yield('content')
    </div>

    <!-- Scripts -->
    <script src="{{ asset(mix('js/app.js')) }}"></script>
    @yield('js')
</body>
</html>
