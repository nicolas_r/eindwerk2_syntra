<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFotosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fotos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('path');
            $table->string('filename');
            $table->string('extensie', 5)->nullable();
            $table->unsignedInteger('gerechten_id')->nullable();   // Nullable, omdat gerecht foto niet altijd
            $table->unsignedInteger('restaurants_id')->nullable(); // restaurant foto is (of omgekeerd)
            $table->boolean('temporary')->default(false);
            $table->integer('uploaded_by')->unsigned();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fotos');
    }
}
