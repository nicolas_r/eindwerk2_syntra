<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLeveringsadressenTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('leveringsadressen', function (Blueprint $table) {
            $table->increments('id');
            $table->string('straat');
            $table->string('nr');
            $table->string('extension')->nullable();
            $table->string('postcode');
            $table->string('stad');
            $table->string('land');
            $table->float('lat', 12, 8)->nullable();
            $table->float('lng', 12, 8)->nullable();
            $table->string('accuracy')->nullable();            // Altijd handig voor diagnostische purpose, stel dat er geen GPS coordinaten in zitten...
            $table->string('formatted_address')->nullable();   // Altijd handig voor diagnostische purpose, stel dat er geen GPS coordinaten in zitten...
            $table->integer('user_id')->unsigned();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('leveringsadressen');
    }
}
