<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVerpakkingenTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('verpakkingen', function (Blueprint $table) {
            $table->increments('id');
            $table->string('naam');
            $table->unsignedTinyInteger('lengte')->nullable();
            $table->unsignedTinyInteger('breedte')->nullable();
            $table->unsignedTinyInteger('hoogte')->nullable();
            $table->integer('restaurant_id')->unsigned();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('verpakkingen');
    }
}
