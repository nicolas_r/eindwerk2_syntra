<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRestaurantsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('restaurants', function (Blueprint $table) {
            $table->increments('id');
            $table->string('naam_restaurant');
            $table->text('omschrijving')->nullable();
            $table->string('email_restaurant');
            $table->string('tel_restaurant', 30)->nullable();
            $table->unsignedInteger('users_id');
            $table->boolean('actief')->default(true);
            $table->string('straat')->nullable();
            $table->mediumInteger('nr')->unsigned()->nullable();
            $table->string('extension')->nullable();
            $table->string('postcode')->nullable();
            $table->string('gemeente')->nullable();
            $table->string('openingsuren', 4096)->nullable();
            $table->unsignedInteger('fotos')->nullable(); // Linken naar een échte foto tabel...
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('restaurants');
    }
}
