<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGerechtenTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('gerechten', function (Blueprint $table) {
            $table->increments('id');
            $table->string('naam', 255);
            $table->string('omschrijving')->nullable();
            $table->double('prijs', 6, 2);
            $table->double('btw', 3,1);
            $table->smallInteger('bereidingstijd')->default('0');
            $table->unsignedInteger('restaurants_id');
            $table->unsignedInteger('verpakkingen_id')->nullable();
            $table->unsignedInteger('fotos')->nullable(); // Linken naar een échte foto tabel...
//            $table->text('fotos', 16383)->nullable(); // Alle foto links + info rechtstreeks in json formaat opslaan...
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('gerechten');
    }
}
