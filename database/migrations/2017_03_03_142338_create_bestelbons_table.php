<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBestelbonsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bestelbons', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('bestellingstatusen_id');
            $table->unsignedInteger('leveranciers_id')->nullable();
            $table->unsignedInteger('klanten_id');
            $table->unsignedInteger('restaurant_id');
            $table->unsignedInteger('leveringsadressen_id');
            $table->dateTime('bestellingtijdstip');                // Tijdstip waarop bestelling is gemaakt (timestamps waren even goed, maar bon...)
            $table->dateTime('ready4pickuptijdstip')->nullable();  // Geschatte afhalingstijd voor leverancier (= bestellingstijdstip + extrawachttijd + langste bereidingstijd gerecht)
            $table->dateTime('pickuptijdstip')->nullable();        // Effectieve afhaling door leverancier
            $table->unsignedSmallInteger('travel_time')->nullable();        // Reistijd doorgegeven door leverancier-API
            $table->unsignedSmallInteger('remaining_time')->nullable();
            $table->dateTime('geschatteleveringtijdstip')->nullable();       // Effectieve afhaling door leverancier
            $table->dateTime('effectieveleveringtijdstip')->nullable();       // Effectieve afhaling door leverancier
            $table->smallInteger('extrawachttijd')->default('0');
            $table->string('followup_url_hash')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bestelbons');
    }
}
