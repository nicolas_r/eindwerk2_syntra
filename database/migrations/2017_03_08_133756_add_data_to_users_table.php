<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDataToUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('firstname')->nullable();
            $table->string('lastname')->nullable();
            $table->boolean('active')->default(true);
            $table->string('straat')->nullable();
            $table->mediumInteger('nr')->unsigned()->nullable();
            $table->string('extension')->nullable();
            $table->string('postcode')->nullable();
            $table->string('gemeente')->nullable();
            $table->string('tel')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('firstname');
            $table->dropColumn('lastname');
            $table->dropColumn('active');
            $table->dropColumn('straat');
            $table->dropColumn('nr');
            $table->dropColumn('extension');
            $table->dropColumn('postcode');
            $table->dropColumn('gemeente');
            $table->dropColumn('tel');
        });
    }
}
