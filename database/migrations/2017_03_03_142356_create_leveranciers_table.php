<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLeveranciersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('leveranciers', function (Blueprint $table) {
            $table->increments('id');
            // $table->string('naam');
            // $table->string('tel', 20);
            $table->unsignedInteger('adressen_id');
            $table->boolean('actief')->default(true);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('leveranciers');
    }
}
