<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBestellingenTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bestelbons_gerechten', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('gerechten_id')->unsigned();
            $table->unsignedInteger('bestelbons_id')->unsigned();
            $table->unsignedTinyInteger('aantal');
            $table->double('eenheidsprijs', 6, 2);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bestelbons_gerechten');
    }
}
