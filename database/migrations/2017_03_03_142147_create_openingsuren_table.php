<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOpeningsurenTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('openingsuren', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('restaurants_id');
            $table->dateTime('openingstijd');
            $table->dateTime('sluitingstijd');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('openingsuren');
    }
}
