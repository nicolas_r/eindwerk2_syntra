<?php

use Illuminate\Database\Seeder;

class rolesSeeder extends Seeder
{
    /**
     * Inserting Dummy Roles
     *
     * @return void
     */
    public function run()
    {
        // clear the database
        DB::table('roles')->truncate();

        // create fixed data
        $data = array(
            array('name' => 'admin'),
            array('name' => 'user'),          // Todo: Bij elke nieuwe account, standaard 'user' role toekennen?
            array('name' => 'restaurant'),
            array('name' => 'leverancier')
        );

        \App\Role::insert($data);

        $this->command->info('Roles inserted!');
    }
}
