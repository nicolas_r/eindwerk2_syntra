<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class usersSeeder extends Seeder
{
    /**
     * Inserting Dummy Users
     *
     * @return void
     */
    public function run()
    {
        // clear the database
        DB::table('users')->truncate();

        // create some own users / account data
        $data = array(
            array(
                'name' => 'Nicolas Ramoudt',
                'email' => 'nicolas.ramoudt@gmail.com',
                'password' => '$2y$10$M6DiKlqQi7itxTnmyaCdhe0id7cL/aCk6sA8TNja.Z8AmP6GmpIIy',
                'firstname' => 'Nicolas',
                'lastname' => 'Ramoudt',
                'straat' => 'Kreekstraat',
                'nr' => '1',
                'extension' => '103',
                'postcode' => '8400',
                'gemeente' => 'Oostende',
                'tel' => '0473 82 38 85',
            ),
            array(
                'name' => 'Pietje Pek',
                'email' => 'pietje.pek@gmail.com',
                'password' => '$2y$10$M6DiKlqQi7itxTnmyaCdhe0id7cL/aCk6sA8TNja.Z8AmP6GmpIIy',
                'firstname' => 'Nicolas',
                'lastname' => 'Ramoudt',
                'straat' => 'Kreekstraat',
                'nr' => '1',
                'extension' => '103',
                'postcode' => '8400',
                'gemeente' => 'Oostende',
                'tel' => '0473 82 38 85',
            ),

        );

        $users = \App\User::insert($data);

        $data2 = array(

                'name' => 'Pandaman',
                'email' => 'pandaman@gmail.com',
                'password' => '$2y$10$M6DiKlqQi7itxTnmyaCdhe0id7cL/aCk6sA8TNja.Z8AmP6GmpIIy',
        );

        $users = \App\User::insert($data2);

        // create some additional FAKE user data
        $faker = Faker\Factory::create();
        foreach(range(1, 50) as $index){
            DB::table('users')->insert([
                'name' => $faker->name,
                'email' => $faker->email,
                'password' => $faker->password,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
                'firstname' => $faker->firstName,
                'lastname' => $faker->lastName,
                'active' => $faker->numberBetween(0, 1),
                'straat' => $faker->streetName,
                'nr' => $faker->numberBetween(0, 500),
                'extension' => $faker->streetSuffix,
                'postcode' => $faker->postcode,
                'gemeente' => $faker->city,
                'tel' => $faker->phoneNumber
            ]);
        }

        $this->command->info('Users inserted!');
    }
}
