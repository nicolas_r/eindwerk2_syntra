<?php

use Illuminate\Database\Seeder;

class verpakkingSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // clear the database
        DB::table('verpakkingen')->truncate();

        // create some own users / account data
        $data = array(
            array(
                'naam' => 'Geen',
                'lengte' => '0',
                'breedte' => '0',
                'hoogte' => '0',
                'restaurant_id' => '1'
            ),
            array(
                'naam' => 'Grote doos',
                'lengte' => '20',
                'breedte' => '20',
                'hoogte' => '5',
                'restaurant_id' => '1'
            ),
            array(
                'naam' => 'Kleine doos',
                'lengte' => '10',
                'breedte' => '10',
                'hoogte' => '2',
                'restaurant_id' => '1'
            ),
            array(
                'naam' => 'Pizzadoos',
                'lengte' => '30',
                'breedte' => '30',
                'hoogte' => '2',
                'restaurant_id' => '1'
            )
        );

        $verpakkingen = \App\VerpakkingenModel::insert($data);

        $this->command->info('Verpakkingen inserted!');
    }
}
