<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Eloquent::unguard();

        //-- Maken seeder-files, vergeet geen "composer dump-autoload" te doen,
        //   om de seed-files aan de autoloader toe te voegen!

        // $this->call(UsersTableSeeder::class);
        $this->call('rolesSeeder');
        $this->call('usersSeeder');        // Add immediately roles while creating users...
        $this->call('rolesToUsers');
        $this->call('levAdresSeeder');
        $this->call('restaurantSeeder');
        $this->call('gerechtSeeder');
        $this->call('verpakkingSeeder');
        $this->call('BestellingStatusSeeder');
        $this->call('BestelbonSeeder');
        $this->command->info('All done!'); // show information in the command line after everything is run
    }
}
