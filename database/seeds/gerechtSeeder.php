<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class gerechtSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

//        {{--$table->increments('id');--}}
//        {{--$table->string('naam', 255);--}}
//        {{--$table->text('omschrijving')->nullable();--}}
//        {{--$table->double('prijs', 6, 2);--}}
//        {{--$table->smallInteger('bereidingstijd')->default('0');--}}
//        {{--$table->unsignedInteger('restaurants_id');--}}
//        {{--$table->unsignedInteger('verpakkingen_id');        --}}
        $data = array(
                    array(
                        'id' => 1,
                        'naam' => 'Wortelsoep',
                        'omschrijving' => 'Lekkere peentjessoep met een scheutje porto!',
                        'prijs' => '12',
                        'btw' => '6',
                        'bereidingstijd' => '15',
                        'restaurants_id' => '1',
                        'verpakkingen_id' => '1'
                    ),
                    array(
                        'id' => 2,
                        'naam' => 'Gebraden kip',
                        'omschrijving' => 'De Koekoek kan er niet aan tippen!',
                        'prijs' => '8',
                        'btw' => '6',
                        'bereidingstijd' => '30',
                        'restaurants_id' => '1',
                        'verpakkingen_id' => '1'
                    ),
                    array(
                        'id' => 3,
                        'naam' => 'Pietje Pek\'s Gebraden kip',
                        'omschrijving' => 'De Koekoek kan er niet aan tippen!',
                        'prijs' => '8',
                        'btw' => '6',
                        'bereidingstijd' => '30',
                        'restaurants_id' => '2',
                        'verpakkingen_id' => '1'
                    ),
                    array(
                        'id' => 4,
                        'naam' => 'Pietje Pek\'s Ribbetjes',
                        'omschrijving' => 'Met lekkere kruidjes',
                        'prijs' => '8',
                        'btw' => '6',
                        'bereidingstijd' => '30',
                        'restaurants_id' => '2',
                        'verpakkingen_id' => '1'
                    ),
                );

        \App\GerechtModel::insert($data);

        $faker = Faker\Factory::create();
        foreach(range(1, 45) as $index){
            DB::table('gerechten')->insert([
                'naam' => $faker->sentence($nbWords = 3, $variableNbWords = true),
                'omschrijving' => $faker->sentence($nbWords = 10, $variableNbWords = true),
                'prijs' => $faker->numberBetween(1,70),
                'btw' => '6',
                'bereidingstijd' => $faker->numberBetween(10,45),
                'restaurants_id' => $faker->numberBetween(1,15),
                'verpakkingen_id' => '1',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ]);
        }

        $this->command->info('GerechtenIngevoerd!');
    }
}
