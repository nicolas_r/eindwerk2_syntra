<?php

use Illuminate\Database\Seeder;

class BestellingStatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = array(
            array(
                'id' => 1,
                'status' => 'Open',
                'progress_procent' => 5    // vaste waarde
            ),
            array(
                'id' => 2,
                'status' => 'Bereiding',
                'progress_procent' => 25   // vaste waarde
            ),
            array(
                'id' => 3,
                'status' => 'Bestelling Gereed',
                'progress_procent' => 50   // 50% wordt berekend
            ),
            array(
                'id' => 4,
                'status' => 'Bestelling wordt afgehaald',
                'progress_procent' => 50   // 50% wordt berekend
            ),
            array(
                'id' => 5,
                'status' => 'Onderweg',
                'progress_procent' => 50   // 50% wordt berekend
            ),
            array(
                'id' => 6,
                'status' => 'Geleverd',
                'progress_procent' => 100  // vaste waarde
            ),
            array(
                'id' => 99,
                'status' => 'GEANNULEERD',
                'progress_procent' => 0    // vaste waarde
            )
        );

        \App\BestelingStatusModel::insert($data);

        $this->command->info('Bestelstatus Seeds Ingevoerd!');
    }
}
