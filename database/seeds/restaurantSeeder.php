<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class restaurantSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // clear the database
        DB::table('restaurants')->truncate();

        // create some restaurants
        $data = array(
            array(
                'naam_restaurant' => 'Nicolas\'Zeezotje',
                'omschrijving' => 'Visspecialiteiten!',
                'email_restaurant' => 'zeezotje@telenet.be',
                'tel_restaurant' => '+3256235522',
                'users_id' => 1,
                'actief' => true,
                'straat' => 'Langestraat',
                'nr' => '55',
                'postcode' => '8400',
                'gemeente' => 'Oostende',
                'openingsuren' => '{"monday":["08:00-18:00"],"tuesday":["10:00-20:00"],"wednesday":["10:00-22:00"],"thursday":["12:00-22:00"],"friday":["12:00-23:00"],"saturday":["08:00-14:00","18:00-20:00"],"sunday":["10:00-14:00"]}',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ),
            array(
                'naam_restaurant' => 'Pietje Pek\'s Specialiteitjes',
                'omschrijving' => 'Vleesgerechjes!',
                'email_restaurant' => 'pietjesdinges@telenet.be',
                'tel_restaurant' => '+3256235522',
                'users_id' => 2,
                'actief' => true,
                'straat' => 'Langestraat',
                'nr' => '55',
                'postcode' => '8400',
                'gemeente' => 'Oostende',
                'openingsuren' => '{"monday":["08:00-18:00"],"tuesday":["10:00-20:00"],"wednesday":["10:00-22:00"],"thursday":["12:00-22:00"],"friday":["12:00-23:00"],"saturday":["08:00-14:00","18:00-20:00"],"sunday":["10:00-14:00"]}',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            )
        );

        \App\RestaurantModel::insert($data);

        $faker = Faker\Factory::create();

        foreach(range(1, 150) as $index){
            DB::table('restaurants')->insert([
                'naam_restaurant' => $faker->company,
                'omschrijving' => $faker->sentence($nbWords = 6, $variableNbWords = true),
                'email_restaurant' => $faker->companyEmail,
                'tel_restaurant' => $faker->phoneNumber,
                'users_id' => $faker->numberBetween(1, 10),
                'actief' => true,
                'straat' => $faker->streetAddress,
                'nr' => $faker->numberBetween(0, 500),
                'postcode' => $faker->postcode,
                'gemeente' => $faker->city,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ]);
        }

        $this->command->info('Restaurant inserted!');
    }
}
