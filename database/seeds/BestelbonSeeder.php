<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class BestelbonSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $data = array(
            array(
                'bestellingstatusen_id' => '1',
                'klanten_id' => '1',
                'bestellingtijdstip' => Carbon::now(),
                'restaurant_id' => 2,
                'extrawachttijd' => 20,
                'leveringsadressen_id' => 2,
                'followup_url_hash' => hash('sha1', (string)1 . '_' . (string)time())
            ),
            array(
                'bestellingstatusen_id' => '1',
                'klanten_id' => '2',
                'bestellingtijdstip' => Carbon::now(),
                'restaurant_id' => 2,
                'extrawachttijd' => 50,
                'leveringsadressen_id' => 2,
                'followup_url_hash' => hash('sha1', (string)2 . '_' . (string)time())
            )
        );

        \App\BestelbonModel::insert($data);

        $this->command->info('Bestelbons Seeds Ingevoerd!');

    }
}
