<?php

use Illuminate\Database\Seeder;

class rolesToUsers extends Seeder
{
    /**
     * Assign Dummy Roles to Users
     *
     * @return void
     */
    public function run()
    {
        // clear the database
        DB::table('role_user')->delete();
        \App\User::first()->roles()->attach([1, 2, 3, 4]);
        \App\User::where('name', '=', 'Pietje Pek')->first()->roles()->attach([2, 3, 4]);
        \App\User::where('name', '=', 'Pandaman')->first()->roles()->attach([2, 3]);
        $this->command->info('Roles assigned!');
    }
}
