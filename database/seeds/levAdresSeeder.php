<?php

use Illuminate\Database\Seeder;

class levAdresSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = array(
            array(
                'straat' => 'Kreekstraat',
                'nr' => '1',
                'extension' => '103',
                'postcode' => '8400',
                'stad' => 'Oostende',
                'land' => 'Belgie',
                'lat' => '51.2180022',
                'lng' => '2.93357179',
                'user_id' => '2'
            ),
            array(
                'straat' => 'Bronstraat',
                'nr' => '29',
                'extension' => '',
                'postcode' => '8400',
                'stad' => 'Oostende',
                'land' => 'Belgie',
                'lat' => '51.2134351',
                'lng' => '2.91264739',
                'user_id' => '2'
            ),
            array(
                'straat' => 'Noordstraat',
                'nr' => '14',
                'extension' => '',
                'postcode' => '8400',
                'stad' => 'Oostende',
                'land' => 'Belgie',
                'lat' => '51.2165178',
                'lng' => '2.94001009',
                'user_id' => '2'
            ),
            array(
                'straat' => 'Kreekstraat',
                'nr' => '1',
                'extension' => '103',
                'postcode' => '8400',
                'stad' => 'Oostende',
                'land' => 'Belgie',
                'lat' => '51.2180022',
                'lng' => '2.93357179',
                'user_id' => '1'
            ),
            array(
                'straat' => 'Kreekstraat',
                'nr' => '1',
                'extension' => '103',
                'postcode' => '8400',
                'stad' => 'Oostende',
                'land' => 'Belgie',
                'lat' => '51.2180022',
                'lng' => '2.93357179',
                'user_id' => '2'
            ),
            array(
                'straat' => 'Bronstraat',
                'nr' => '29',
                'extension' => '',
                'postcode' => '8400',
                'stad' => 'Oostende',
                'land' => 'Belgie',
                'lat' => '51.2134351',
                'lng' => '2.91264739',
                'user_id' => '2'
            ),
            array(
                'straat' => 'Noordstraat',
                'nr' => '14',
                'extension' => '',
                'postcode' => '8400',
                'stad' => 'Oostende',
                'land' => 'Belgie',
                'lat' => '51.2165178',
                'lng' => '2.94001009',
                'user_id' => '2'
            ),
            array(
                'straat' => 'Kreekstraat',
                'nr' => '1',
                'extension' => '103',
                'postcode' => '8400',
                'stad' => 'Oostende',
                'land' => 'Belgie',
                'lat' => '51.2180022',
                'lng' => '2.93357179',
                'user_id' => '1'
            )
        );

        \App\LeveringsAdressenModel::insert($data);
        $this->command->info('LeveringsAdressen Ingevoerd!');

    }
}
