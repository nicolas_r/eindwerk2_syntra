app.controller('VerpakkingenController', function($scope, $http, API_URL){
    $http.get(API_URL + "verpakkingen")
        .then(function(response){
            console.log(response);
            $scope.verpakkingen = response.data;
        }, function(response){
            console.log(response);
            alert('Kon de gegevens ophalen.  Fout gebeurd bij ophaling gegevens!');
        });

    // Toon modal form
    $scope.toggle = function(modalstate, id) {
        $scope.modalstate = modalstate;
        switch(modalstate) {
            case 'add':
                $scope.form_title = "Voeg nieuwe verpakking toe";
                break;
            case 'edit':
                $scope.form_title = "Verpakking details";
                $scope.id = id;
                $http.get(API_URL + 'verpakkingen/' + id).then(function(response){
                    console.log(response);
                    $scope.verpakking = response.data;
                });
                break;
            default:
                break;
        }
        console.log(id);
        $('#myModal').modal('show');
    };

    // bewaar nieuwe verpakking en update bestaande verpakking
    $scope.save = function(modalstate, id){
        var url = API_URL + "verpakkingen";
        if(modalstate === 'edit') {
            // console.log("The modalstate variable is:");
            // console.log(modalstate);
            url += "/" + id;
            // console.log(url);
        }
        $http({
            method: 'POST',
            url: url,
            data: $.param($scope.verpakking),
            headers: {'Content-type': 'application/x-www-form-urlencoded'}
        }).then(function(response){    //-- Indien gelukt ?
            // console.log(response);
            location.reload();
        }, function(response){         //-- Indien gefaald!
            // console.log(response);
            alert('Error has occurred, check logs!');
        });
    };

    // delete verpakkingen

    $scope.verifyDeleteConfirmation = function(id){
        $http.get(API_URL + 'verpakkingen/' + id).then(function(response){
            console.log(response);
            $scope.verpakking = response.data;
        });
        $('#deleteModal').modal('show');
    };

    $scope.cancelDelete = function(){
        $('#deleteModal').modal('hide');
        //-- Maak data leeg bij verbergen
        //(anders blijft die tijdelijk even tevoorschijn komen bij herladen)
        $scope.verpakking = {};
    };

    $scope.confirmDelete = function(id) {
            $http({
                method: 'DELETE',
                url: API_URL + 'verpakkingen/' + id
            }).then(function(data){     //-- Indien gelukt ?
                //console.log(data);
                location.reload();
            }, function(data){          //-- Indien gefaald!
                //console.log(data);
                alert('Kan niet verwijderen');
            })
    }
});